<?php ini_set('display_errors','On');

//Evita presentar contenidos sin el login debido
include "../../../security/secure.php";
include "../../../core/class/db.class.php";
include "../../../../business/function/plGeneral.fnc.php";
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include "../../class/PhpThumbFactory.class.php";
include "../../class/ClassFile.class.php";


 
$ccontacto= new Dbcontacto();
$datos_li['where'] = "order by id desc ";
$info_list = $ccontacto->getList($datos_li);

?>
<?php header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=contactenos.xls");
header("Pragma: no-cache");
header("Expires: 0");?>
<table >
					<thead><tr>
                                                <th>Nombres</th>
                                                <th>Apellidos</th>
                                                <th>E-mail</th>
                                                <th>Telefono</th>
                                                <th>Entidad</th>
                                                <th>Asunto</th>
                                                <th>Mensaje</th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
                   foreach ($info_list as $item) {			
                   ?>
                <tr class="odd gradeX">
                    <td class="center" width="150px">
                      <?php echo $item['nombres']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['apellidos']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['email']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['telefono']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['entidad']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['asunto']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['mensaje']?>
                  </td>
                  
                </tr>
             	<?php }?>

            </tbody>
          </table>