<?php 
$cpuntos= new Dbpuntos();


?>

<style>
	.gmap3 {
	margin: 0px auto;
	border: 1px dashed #C0C0C0;
	width: 100%;
	height: 350px;
	}
</style>



<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el puntos?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
        $datos_ord = $cpuntos->getByPk($_GET["id_del"]);
  	$datos_eli['where']=" AND orden > ".$datos_ord['orden'];
        $cpuntos->update_masi("orden = (orden-1)",$datos_eli['where']);
        $cpuntos->deleteById($_GET["id_del"]); 
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $texto = $_POST['texto'];
        $correo = $_POST['correo'];
        $direccion = $_POST['direccion'];
        $latitud = $_POST['latitud'];
        $longitud = $_POST['longitud'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
        $li_inf = $cpuntos->getList($datos_pr);
        $cpuntos->setorden($li_inf[0]['max_orden'] + 1);
        $cpuntos->setnombre($nombre);
        $cpuntos->setcorreo($correo);
        $cpuntos->setdireccion($direccion);
        $cpuntos->setlatitud($latitud);
        $cpuntos->setlongitud($longitud);
        $cpuntos->settexto($texto);
        $cpuntos->save();
	$id = $cpuntos->getMaxId();
  } else {
  	$nombre = $_POST['nombre'];
        $texto = $_POST['texto'];
        $correo = $_POST['correo'];
        $direccion = $_POST['direccion'];
        $latitud = $_POST['latitud'];
        $longitud = $_POST['longitud'];
        $cpuntos->setnombre($nombre);
        $cpuntos->setcorreo($correo);
        $cpuntos->setdireccion($direccion);
        $cpuntos->setlatitud($latitud);
        $cpuntos->setlongitud($longitud);
        $cpuntos->settexto($texto);
        $cpuntos->setid($id);
	$cpuntos->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cpuntos->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	$cates_orden = $cpuntos->getList($datos_ord2);
	$cpuntos_2  = new Dbpuntos();
	$cpuntos_2->setid($cates_orden[0]['id']);
	$cpuntos_2->setorden($cates_orden[0]['orden'] + 1);
	$cpuntos_2->save();
	$cpuntos_3  = new Dbpuntos();
	$cpuntos_3->setid($id_ord);
	$cpuntos_3->setorden($datos_ord2['orden']);
	$cpuntos_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cpuntos->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	$grados_orde = $cpuntos->getList($datos_ord2);
	$cpuntos_2  = new Dbpuntos();
	$cpuntos_2->setid($grados_orde[0]['id']);
	$cpuntos_2->setorden($grados_orde[0]['orden'] - 1);
	$cpuntos_2->save();
	$cpuntos_3  = new Dbpuntos();
	$cpuntos_3->setid($id_ord);
	$cpuntos_3->setorden($datos_ord2['orden']);
	$cpuntos_3->save();
}

// Consultamos la img actual del banner
$datos_inst = $cpuntos->getByPk($id);
$datos_li['where'] = "order by orden ";
$info_list = $cpuntos->getList($datos_li);
?>
<script type="text/javascript" src="../../../../gmap3v5.1.1/gmap3v5.1.1/jquery/jquery-1.4.4.min.js"></script>        
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="../../../../gmap3v5.1.1/gmap3v5.1.1/gmap3.js"></script> 
    <script>
        
        $(document).ready(function () {
            <?php if ($id > 0){?>
                mapa_direccion('<?php if ($datos_inst['latitud'] == ''){
                    echo $datos_inst['direccion'];                    
                    }?>');
            <?php }?>
        });  
        
        function buscar_dir(){
            var direccion = $("#direccion").val();
            mapa_direccion(direccion);
        }
        
        function mapa_direccion(direccion){
	  $("#mapa").html('<div id="test" class="gmap3"></div>');
          if (direccion != ''){
	  $("#test").gmap3({
			marker:{
				
			  values:[
			  		
            		{address:direccion, data:"", options:{
					draggable: true,
					icon: "../../../../assets/img/logomapa.png"}}
    			],
					events: {
                        dragend: function (marker, event, context) {
							$("#latitud").val(event.latLng.lat());
							$("#longitud").val(event.latLng.lng());									
                        }
                    }
			}
			,
			map:{
			  options:{
				zoom: 16
			  }
			}
            
		  });
          }else{
            <?php if ($datos_inst['latitud'] != ''){?>
            $("#test").gmap3({
			marker:{
				
			  values:[
			  		
            		{latLng:[<?php echo $datos_inst['latitud']?>, <?php echo $datos_inst['longitud'];?>], data:"", options:{
					draggable: true,
					icon: "../../../../assets/img/logomapa.png"}}
    			],
					events: {
                        dragend: function (marker, event, context) {
							$("#latitud").val(event.latLng.lat());
							$("#longitud").val(event.latLng.lng());									
                        }
                    }
			}
			,
			map:{
			  options:{
				zoom: 16
			  }
			}
            
		  });
            <?php }?>
          }      
	}
    </script>
<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      Puntos
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando puntos" ?></h3>
        
        <p>&nbsp;</p>   

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_inst["nombre"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Direccion</label>
            <div>
                <input type="text" name="direccion" id="direccion" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_inst["direccion"]; ?>" />
              <input type="button" name="" value="buscar" onclick="buscar_dir()">
              <input type="hidden" name="latitud" id="latitud" value="<?php echo $datos_inst["latitud"]; ?>" >
              <input type="hidden" name="longitud" id="longitud" value="<?php echo $datos_inst["longitud"]; ?>" >
            </div>

          </div>
          
          
          
          <div style="margin-top: 36px;">
            <label>Mapa</label>
            <div id="mapa">
              
            </div>

          </div>
          
          
          <div style="margin-top: 36px;">
              <label>Texto</label>
            <div>
                <textarea name="texto" id="texto" style="width: 325px; margin-left: 200px; margin-top: -25px;"><?php echo $datos_inst["texto"]; ?></textarea>

            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Correo</label>
            <div>
              <input type="text" name="correo" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_inst["nombre"]; ?>" />

            </div>

          </div>

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=puntos&id=0">Agregar Nuevo Punto</a>
		   <table class="display" >
					<thead>
						
					  <tr>
					      <th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($info_list as $item) {
					?>
                <tr class="odd gradeX">   
                    <td class="center" width="150px">   
                                    <?php if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=puntos&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($info_list)){
								?>
								<a href="index.php?seccion=puntos&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>    
                                        </td>
                    <td><?= $item["nombre"] ?></td>
                    <td>
                     <a class="uibutton icon edit" href="index.php?seccion=puntos&id=<?= $item["id"] ?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=puntos&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script type="text/javascript" src="../../../js/tinymce/tinymce.min.js"></script>

</script>

<script>
  $(document).ready(function() {
                tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
});
                
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>