<?php 
$cinfoinstitucional= new Dbinstitucional();
?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
        $datos_ord = $cinfoinstitucional->getByPk($_GET["id_del"]);
  	$datos_eli['where']=" AND orden > ".$datos_ord['orden'];
        $cinfoinstitucional->update_masi("orden = (orden-1)",$datos_eli['where']);
        $cinfoinstitucional->deleteById($_GET["id_del"]); 
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $texto = $_POST['texto'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
        $retorno = ClassFile::UploadFile("imagen", "../../../../imagenes/info_inst", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$cinfoinstitucional->setimagen($retorno["NameFile"]);	
	}else{
		
	}
        $li_inf = $cinfoinstitucional->getList($datos_pr);
        $cinfoinstitucional->setorden($li_inf[0]['max_orden'] + 1);
        $cinfoinstitucional->setnombre($nombre);
        $cinfoinstitucional->settexto($texto);
        $cinfoinstitucional->save();
	$id = $cinfoinstitucional->getMaxId();
  } else {
        $retorno = ClassFile::UploadFile("imagen", "../../../../imagenes/info_inst", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$cinfoinstitucional->setimagen($retorno["NameFile"]);	
	}else{
		
	}
  	$nombre = $_POST['nombre'];
        $texto = $_POST['texto'];
        $cinfoinstitucional->setnombre($nombre);
        $cinfoinstitucional->settexto($texto);
        $cinfoinstitucional->setid($id);
	$cinfoinstitucional->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cinfoinstitucional->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	$cates_orden = $cinfoinstitucional->getList($datos_ord2);
	$cinfoinstitucional_2  = new Dbinfo_institucional();
	$cinfoinstitucional_2->setid($cates_orden[0]['id']);
	$cinfoinstitucional_2->setorden($cates_orden[0]['orden'] + 1);
	$cinfoinstitucional_2->save();
	$cinfoinstitucional_3  = new Dbinfo_institucional();
	$cinfoinstitucional_3->setid($id_ord);
	$cinfoinstitucional_3->setorden($datos_ord2['orden']);
	$cinfoinstitucional_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cinfoinstitucional->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	$grados_orde = $cinfoinstitucional->getList($datos_ord2);
	$cinfoinstitucional_2  = new Dbinfo_institucional();
	$cinfoinstitucional_2->setid($grados_orde[0]['id']);
	$cinfoinstitucional_2->setorden($grados_orde[0]['orden'] - 1);
	$cinfoinstitucional_2->save();
	$cinfoinstitucional_3  = new Dbinfo_institucional();
	$cinfoinstitucional_3->setid($id_ord);
	$cinfoinstitucional_3->setorden($datos_ord2['orden']);
	$cinfoinstitucional_3->save();
}

// Consultamos la img actual del banner
$datos_inst = $cinfoinstitucional->getByPk($id);
$datos_li['where'] = "order by orden ";
$info_list = $cinfoinstitucional->getList($datos_li);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      CAMPOS
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando campos" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_inst["nombre"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Imagen</label>
            <?php if ($datos_inst['imagen'] != ''){?>
            <img src="../../../../imagenes/info_inst/<?php echo $datos_inst['imagen']?>" width="200px"/>
                <?php }?>
            <div>
                <input type="file" name="imagen" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>
                <br>Dimensiones 713 x 285
            </div>

          </div>
          
          <div style="margin-top: 36px;">
              <label>Texto</label>
            <div>
                <textarea name="texto" id="texto" style="width: 325px; margin-left: 200px; margin-top: -25px;"><?php echo $datos_inst["texto"]; ?></textarea>

            </div>

          </div>
          
          
          

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=info_inst&id=0">Agregar Nueva información Institucional</a>
		   <table class="display" >
					<thead>
						
					  <tr>
					      <th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($info_list as $item) {
					?>
                <tr class="odd gradeX">   
                    <td class="center" width="150px">   
                                    <?php if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=info_inst&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($info_list)){
								?>
								<a href="index.php?seccion=info_inst&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>    
                                        </td>
                    <td><?= $item["nombre"] ?></td>
                    <td>
                     <a class="uibutton icon edit" href="index.php?seccion=info_inst&id=<?= $item["id"] ?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=info_inst&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script type="text/javascript" src="../../../js/tinymce/tinymce.min.js"></script>

</script>

<script>
  $(document).ready(function() {
                tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
});
                
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>