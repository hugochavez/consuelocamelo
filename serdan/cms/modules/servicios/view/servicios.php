<?php 
$ctexto = new Dbtexto();

if (isset($_POST['titulo'])){
    $ctexto->setvalor($_POST['titulo']);
    $ctexto->setid(1);
    $ctexto->save();
    $ctexto->setvalor($_POST['texto_encab']);
    $ctexto->setid(2);
    $ctexto->save();
}

$datos_titulo = $ctexto->getByPk(1);
$datos_texto = $ctexto->getByPk(2);



$cservicio= new Dbservicio();
?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
        $datos_ord = $cservicio->getByPk($_GET["id_del"]);
  	$datos_eli['where']=" AND orden > ".$datos_ord['orden'];
        $cservicio->update_masi("orden = (orden-1)",$datos_eli['where']);
        $cservicio->deleteById($_GET["id_del"]); 
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
        $retorno = ClassFile::UploadFile("imagen", "../../../../imagenes/servicios", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$cservicio->setimagen($retorno["NameFile"]);	
	}else{
		
	}
        $retorno2 = ClassFile::UploadFile("logo", "../../../../imagenes/logos_serv", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno2["Status"]=="Uploader"){
		$cservicio->setlogo($retorno2["NameFile"]);	
	}else{
		
	}
        $li_inf = $cservicio->getList($datos_pr);
        $cservicio->setorden($li_inf[0]['max_orden'] + 1);
        $cservicio->setnombre($nombre);
        $cservicio->save();
	$id = $cservicio->getMaxId();
  } else {
        $retorno = ClassFile::UploadFile("imagen", "../../../../imagenes/servicios", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$cservicio->setimagen($retorno["NameFile"]);	
	}else{
		
	}
        $retorno2 = ClassFile::UploadFile("logo", "../../../../imagenes/logos_serv", "producto_".rand(0,10000), "producto_".rand(0,10000));
	if($retorno2["Status"]=="Uploader"){
		$cservicio->setlogo($retorno2["NameFile"]);	
	}else{
		
	}
  	$nombre = $_POST['nombre'];
        $cservicio->setnombre($nombre);
        $cservicio->setid($id);
	$cservicio->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cservicio->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	$cates_orden = $cservicio->getList($datos_ord2);
	$cservicio_2  = new Dbservicio();
	$cservicio_2->setid($cates_orden[0]['id']);
	$cservicio_2->setorden($cates_orden[0]['orden'] + 1);
	$cservicio_2->save();
	$cservicio_3  = new Dbservicio();
	$cservicio_3->setid($id_ord);
	$cservicio_3->setorden($datos_ord2['orden']);
	$cservicio_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cservicio->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	$grados_orde = $cservicio->getList($datos_ord2);
	$cservicio_2  = new Dbservicio();
	$cservicio_2->setid($grados_orde[0]['id']);
	$cservicio_2->setorden($grados_orde[0]['orden'] - 1);
	$cservicio_2->save();
	$cservicio_3  = new Dbservicio();
	$cservicio_3->setid($id_ord);
	$cservicio_3->setorden($datos_ord2['orden']);
	$cservicio_3->save();
}

// Consultamos la img actual del banner
$datos_inst = $cservicio->getByPk($id);
$datos_li['where'] = "order by orden ";
$info_list = $cservicio->getList($datos_li);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      Servicio
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando servicios" ?></h3>
        
        <?php if ($id == 0){?>
        
        <form method="post" action="" name="formtexto" id="formtexto" enctype="multipart/form-data">
            
            

          <input type="hidden" value="4" name="id_texto" id="id_texto">

          <div style="margin-top: 36px;">
                      <label>Título</label>
            <div>
                <input type="text" name="titulo" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_titulo["valor"]; ?>" />

            </div>

          </div>		  
        
          
          <div style="margin-top: 36px;">
                      <label>Texto</label>
            <div>
                <textarea id="texto_encab" name="texto_encab" style="width: 325px; margin-left: 200px; margin-top: -25px;"><?php echo $datos_texto['valor']?></textarea>

            </div>

          </div>
          
          
          <div><a id="submitForm" onclick="$('#formtexto').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
        </form>  
        
        <?php }?>
        
        <?php if ($id > 0){?>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>
                Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_inst["nombre"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Logo</label>
            <?php if ($datos_inst['logo'] != ''){?>
            <img src="../../../../imagenes/logos_serv/<?php echo $datos_inst['logo']?>"  width="200px">
                <?php }?>
            <div>
                <input type="file" name="logo" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>
                <br>Dimensiones 220 x 37
            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Imagen</label>
            <?php if ($datos_inst['imagen'] != ''){?>
            <img src="../../../../imagenes/servicios/<?php echo $datos_inst['imagen']?>" width="200px"/>
                <?php }?>
            <div>
                <input type="file" name="imagen" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>
                <br>Dimensiones 611 x 543

            </div>

          </div>
          
          

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          
          <p>&nbsp;</p>
          <?php }?>
          
           
		   <table class="display" >
					<thead>
						
					  <tr>
					      <th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
                                              
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($info_list as $item) {
					?>
                <tr class="odd gradeX">   
                    <td class="center" width="150px">   
                                    <?php if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=servicios&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($info_list)){
								?>
								<a href="index.php?seccion=servicios&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>    
                                        </td>
                    <td><?= $item["nombre"] ?></td>
                    <td>
                     <a class="uibutton icon edit" href="index.php?seccion=detalles_servicios&servicio=<?= $item["id"] ?>">Detalles</a>   
                     <a class="uibutton icon edit" href="index.php?seccion=servicios&id=<?= $item["id"] ?>">Editar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>


<script type="text/javascript" src="../../../js/tinymce/tinymce.min.js"></script>

</script>

<script>
  $(document).ready(function() {
                tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
});
                
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>