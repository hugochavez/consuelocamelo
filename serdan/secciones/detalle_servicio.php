<?php include("head.php"); ?>
<?php include("menu.php"); 
$cservicio = new Dbservicio();
$id_deta = 0;
if (isset($_GET['id_deta'])){
    $id_deta = $_GET['id_deta'];
}
$id = $_GET['id'];
$lista_colores = $cservicio->get_colores();
$datos_servicio = $cservicio->getByPk($id);
$cdetalle_servicio = new Dbdetalle_servicio();
$datos_deta['servicio'] = $id;
$datos_deta['where'] = " ORDER BY orden asc ";
$lista_detalles = $cdetalle_servicio->getList($datos_deta);
?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(3);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>

<section>
	<div class="content content-wide <?php echo $lista_colores[($datos_servicio['color'])]['clase']?>">
    	<div class="title">
        	<h3>Portafolio de Servicios</h3>
        </div>
        <div class="clear"></div>
        
        <div class="row-fluid">
        	<div class="span12">
                <div class="box-body">
                	<div class="row-fluid">
                    	<div class="span6">
                            <img src="imagenes/logos_serv/<?php echo $datos_servicio['logo']?>" class="serv-tit">
                        	
                            <div class="wide-img">
                                <img src="imagenes/servicios/<?php echo $datos_servicio['imagen']?>" />
                            </div>
                        </div>
                        <div class="span6">
                        	<div class="accordion">
                                    <?php $ind=0;
                                    foreach ($lista_detalles as $item){
                                        $ind++;?>
                                <div class="acc-element <?php if (($ind == 1 && $id_deta == 0) || ($item['id'] == $id_deta)){
                                    echo "open";
                                }?>">
                                    <div class="acc-header">
                                        <p><?php echo $item['nombre']?></p>
                                        <span class="btn-colapse"></span>
                                    </div>
                                    <div class="acc-body">
                                        <p><?php echo $item['texto']?></p>
                                    </div>
                                </div>
                                    <?php }?>
                                
                            </div>
                            
                            <div class="align-right">
                                <a class="button button-big button-orange" href="index.php?seccion=portafolio">VOLVER</a>
                            </div>
                            
                        </div>
                	
					</div>
                </div>
            </div>
        </div>
        
    </div>
</section>


<?php include("footer.php"); ?>

