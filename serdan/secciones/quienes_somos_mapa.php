<?php include("head.php"); ?>
<?php include("menu.php"); 

$dats_puntos['where'] = " ORDER BY orden ASC ";
$cpuntos = new Dbpuntos();
$puntos = $cpuntos->getList($dats_puntos);

?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(2);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>



<section>
	<div class="content">
    	<div class="title">
        	<h3>Quiénes Somos</h3>
        </div>
        <div class="clear"></div>
        <div class="row-fluid">
        	<div class="sidebar span3">
                <?php include("sidebar.php"); ?>
            </div>
            <div class="box span9">
                <div class="box-body">
                	<h2 class="subtitle">En dónde estamos</h2>
                        <div id="mapa"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("footer.php"); ?>

<style>
	.gmap3 {
	margin: 0px auto;
	border: 1px dashed #C0C0C0;
	width: 100%;
	height: 350px;
	}
</style>
       
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="gmap3v5.1.1/gmap3v5.1.1/gmap3.js"></script>
    
    <script>
        
    $(document).ready(function () {
            cargar_mapa();
        });
        
    function  cargar_mapa(){
              mapa_direccion();
          }     
        
    function mapa_direccion(){
	  $("#mapa").html('<div id="test" class="gmap3"></div>');
	  $("#test").gmap3({
			marker:{
				
			  values:[
			  <?php $ind = 0;
                          foreach ($puntos as $item){
                              $ind++;?>
                                              
            		{<?php if ($item['latitud'] != ''){
                             echo "latLng:[".$item['latitud'].",".$item['longitud']."]"; 
                    }else{
                        echo "address:'".$item['direccion']."'";
                             }?>, data:"<?php echo $item['nombre']?>__<?php echo $item['texto']?>", options:{
					draggable: false,
					icon: "assets/img/logomapa.png"}}
                                
                                <?php if($ind < (count($puntos))){
                                    echo ",";
                                }
                                }?>
    			],
                        events: {
                        click: function (marker, event, context) {
							
							var n = context.data.split("__");
							var contentString = n[0]+"<br>"+n[1];
							$(this).gmap3({
                                                        infowindow: {
                                                            options: {
                                                                content: contentString,
                                                                position: event.latLng
                                                            }
                                                        }
                                                    });		
                        }
                    }
				
			},
			map:{
			  options:{
				zoom: 14
			  }
			}
		  });
	}
    </script>

