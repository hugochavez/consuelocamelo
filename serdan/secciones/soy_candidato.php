<?php include("head.php"); ?>
<?php include("menu.php"); 
$cimagen = new Dbimagen();
$ctexto = new Dbtexto();
$cperfil = new Dbperfil();
$datos_imagen1 = $cimagen->getByPk(4);
$datos_imagen2 = $cimagen->getByPk(5);
$datos_texto1 = $ctexto->getByPk(7);
$datos_texto2 = $ctexto->getByPk(8);
$data_perfi['where']= " ORDER BY orden asc ";
$lista_perfiles = $cperfil->getList($data_perfi);
?>


<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(5);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>
	
<section>
	<div class="content">
    	<div class="title">
        	<h3>Soy Candidato</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
                <div class="box-body">
                	<div class="row-fluid">
                    	<div class="span6">
                        	<img src="imagenes/generales/<?php echo $datos_imagen1['valor']?>" class="img-box" />
                        </div>
                        <div class="span6">
                        	<img src="imagenes/generales/<?php echo $datos_imagen2['valor']?>" class="img-box" />
                        </div>
                    </div>
                	<p></p>
                    <h2 class="subtitle"><?php echo $datos_texto1['valor']?></h2>
                	<p><?php echo $datos_texto2['valor']?></p>
                    <?php foreach ($lista_perfiles as $item){?>
                    <div class="box box-offer">
                    	<div class="box-header">
                        	<h2 class="subtitle"><small><?php echo $item['nombre']?></small></h2>
                        </div>
                        <div class="box-body">
                        	<div class="vertical-slide first-slide">
                                <ul>
                                    <li><p>A la espera del webservice</p></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <?php }?> 
                            
                </div>
            </div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

