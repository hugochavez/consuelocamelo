<?php include("head.php"); ?>
<?php include("menu.php");
$cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);


$dats_banner['tipo'] = 2;
$dats_banner['where'] = "order by orden";
$lista_banner2 = $cbanner->getList($dats_banner);

$dats_banner['tipo'] = 3;
$dats_banner['where'] = "order by orden";
$lista_banner3 = $cbanner->getList($dats_banner);

$cservicio = new Dbservicio();
$dats_serv['where'] = " ORDER BY orden";
$lista_servicios = $cservicio->getList($dats_serv);


?>
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(1);
if ($datos_secciones['banner'] == 1){
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>

<section>
	<div class="content">
    	<div class="title">
        	<h3>Nuestros Servicios y Marcas</h3>
        </div>
        <div class="services-nav">
            <?php foreach ($lista_servicios as $item){?>
        	<a href="index.php?seccion=detalle_servicio&id=<?php echo $item['id']?>">
            <div class="service-btn">
            	<img src="imagenes/logos_serv/<?php echo $item['logo']?>" />
            </div></a>
            <?php }?>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section>
	<div class="grey-bkg">
        <div class="content">
            <div class="high-block">
            
                <div class="high-slide">
                    <ul class="bjqs">
                        <?php foreach ($lista_banner2 as $item){?>
                        <li>
                            <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                            <div class="slide-info-block">
                                <div class="slide-info">
                                    <h3><?php echo $item['texto']?></h3>
                                </div>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                </div>
                
                <div class="high-img">
                	<img src="imagenes/banner/<?php echo $lista_banner3[0]['imagen']?>" />
                    <div class="high-txt">
                    	<p><?php echo $lista_banner3[0]['texto']?></p>
                    </div>
                </div>
                
                <div class="clear"></div>
                
            </div>
        </div>
    </div>
</section>

<?php include("footer.php"); ?>

