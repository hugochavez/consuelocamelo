<?php include("head.php"); ?>
<?php include("menu.php"); 
$ctexto = new Dbtexto();
$datos_texto = $ctexto->getByPk(10);
?>

<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(7);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>
	
<section>
	<div class="content">
    	<div class="title">
        	<h3>Contáctenos</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
                <div class="box-body">
                	<p><?php echo $datos_texto['valor']?></p>
                    <form class="form-small">
                    	<div class="row-fluid">
                    		<div class="span4">
                   				<fieldset>
                    				<label>Nombre *</label>
                    				<input type="text">
                    			</fieldset>
                    		</div>
                            <div class="span4">
                   				<fieldset>
                    				<label>Email *</label>
                    				<input type="email">
                    			</fieldset>
                    		</div>
                            <div class="span4">
                   				<fieldset>
                    				<label>Teléfono</label>
                    				<input type="text">
                    			</fieldset>
                    		</div>
                        </div>
                        <div class="row-fluid">
                        	<div class="span6">
                                <fieldset>
                                    <label>Ciudad</label>
                                    <select>
                                        <option value="">Seleccione</option>
                                        <option value="0">Opción</option>
                                        <option value="1">Opción</option>
                                        <option value="2">Opción</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="span6">
                                <fieldset>
                                    <label>Asunto</label>
                                    <select>
                                        <option value="">Seleccione</option>
                                        <option value="0">Opción</option>
                                        <option value="1">Opción</option>
                                        <option value="2">Opción</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row-fluid">
                        	<div class="span12">
                            	<label>Comentario *</label>
								<textarea></textarea>
                            </div>
                        </div>
                        <fieldset class="check-terms">
                            <input type="checkbox" /><p>Acepto los <a class="modal-large fancybox.iframe" href="index.php?seccion=terminos">Términos y Condiciones</a> y <a class="modal-large fancybox.iframe" href="index.php?seccion=politicas">Políticas de privacidad y confidencialidad de la información</a></a>
                        </fieldset>
                        <div class="align-right">
                        	<input type="submit" class="button button-big button-orange" value="ENVIAR" />
                        </div>
                    </form>
                            
                            
                </div>
            </div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

