<?php include("head.php"); ?>
<?php include("menu.php"); 
$ctexto = new Dbtexto();
$datos_titulo = $ctexto->getByPk(1);
$datos_texto = $ctexto->getByPk(2);
$cservicio = new Dbservicio();
$dats_serv['where'] = " ORDER BY orden ASC ";
$lista_servicios = $cservicio->getList($dats_serv);
?>

<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(3);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>
	
<section>
	<div class="content">
    	<div class="title">
        	<h3>Portafolio de Servicios</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
             
                <div class="box-body">
                	<h2 class="subtitle"><?php echo $datos_titulo['valor']?></h2>
                	<p><?php echo $datos_texto['valor'] ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
	<div class="content">
    	<div class="title">
        	<h3>Servicios</h3>
        </div>
        <div class="services-nav">
            <?php foreach ($lista_servicios as $item){?>
        	<a href="index.php?seccion=detalle_servicio&id=<?php echo $item['id']?>"><div class="service-btn">
            	<img src="imagenes/logos_serv/<?php echo $item['logo']?>" />
            </div></a>
            <?php }?>
            <div class="clear"></div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

