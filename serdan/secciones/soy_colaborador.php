<?php include("head.php"); ?>
<?php include("menu.php"); 
$ctexto = new Dbtexto();
$cimagen = new Dbimagen();
$datos_imagen = $cimagen->getByPk(6);
$datos_texto = $ctexto->getByPk(9);
?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(6);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>

<section>
	<div class="content">
    	<div class="title">
        	<h3>Soy Colaborador</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
                <div class="box-body">
                	<img src="imagenes/generales/<?php echo $datos_imagen['valor']?>" class="img-box" />
                	<p><?php echo $datos_texto['valor']?></p>
                    <div class="row-fluid">
                    	<div class="box span6 offset3">
                        	<div class="box-body">
                            	<h2 class="subtitle">LOGIN</h2>
                                    <form class="form-small">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <fieldset>
                                                    <label>Usuario</label>
                                                    <input type="text">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <fieldset>
                                                    <label>Password</label>
                                                    <input type="password">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <fieldset class="check-terms">
                                                    <p><a>Olvidó su contraseña?</a>
                                                </fieldset>
                                            </div>
                                        </div>
                                        
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="align-right">
                                                    <input type="submit" class="button button-big button-orange" value="ACCEDER" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                            </div>
                        </div>
                    </div>
                    
                            
                            
                </div>
            </div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

