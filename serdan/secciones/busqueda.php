<?php include("head.php"); ?>
<?php include("menu.php"); 

$buscar = $_POST['buscar'];
$dats_texto['where'] = " AND valor LIKE '%".$buscar."%' ";
$ctexto = new Dbtexto();
$textos = $ctexto->getList($dats_texto);
$dats_perfil['where'] = " AND nombre LIKE '%".$buscar."%' ";
$cperfil = new Dbperfil();
$perfiles = $cperfil->getList($dats_perfil);
$datos_servicio['campos_esp'] = "a.nombre,a.id,a.texto,ds.nombre as nomb_deta,ds.texto as textodeta,ds.id as id_ds";
$datos_servicio['join'] = " LEFT JOIN detalle_servicio ds on a.id = ds.servicio AND (ds.nombre LIKE '%".$buscar."%' OR ds.texto LIKE '%".$buscar."%')"; 
$datos_servicio['where'] = " AND (a.nombre LIKE '%".$buscar."%' OR a.texto LIKE '%".$buscar."%' OR ds.nombre LIKE '%".$buscar."%' OR ds.texto LIKE '%".$buscar."%')";
$cservicio = new Dbservicio();
$servicios = $cservicio->getList($datos_servicio);



?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(8);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>

<section>
	<div class="content">
    	<div class="title">
        	<h3>Resultados de la Búsqueda</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
                <div class="box-body">
                    <form action="index.php?seccion=busqueda">
                    	<div class="row-fluid" >
                        	
                    		<div class="span6 offset2">
                   				<fieldset>
                                                    <input type="text" placeholder="Introducza su búsqueda" name="buscar" value="<?php echo $buscar?>">
                    			</fieldset>
                    		</div>
                            <div class="span4">
                   				<fieldset>
                    				<input type="submit" class="button button-larger button-orange" value="BUSCAR" />
                    			</fieldset>
                    		</div>
                           
                        </div>
                    </form>
                    <?php foreach ($textos as $item){?>
                	<div class="result-block">
                    	<h2 class="subtitle"><?php echo str_replace('_',' ',$item['url'])?></h2>
                        <p><?php echo substr(strip_tags($item['valor']),0,300); ?></p>
                        <div class="align-right">
                            <a class="button button-orange" href="index.php?seccion=<?php echo $item['url']?>">VER MÁS</a>
                        </div>
                    </div>
                    <?php }?>
                    <?php foreach ($perfiles as $item){?>
                	<div class="result-block">
                    	<h2 class="subtitle">Soy candidato/<?php echo $item['nombre']?></h2>
                        <p><?php echo substr(strip_tags($item['texto']),0,300); ?></p>
                        <div class="align-right">
                            <a class="button button-orange" href="index.php?seccion=soy_candidato" >VER MÁS</a>
                        </div>
                    </div>
                    <?php }?>
                    <?php foreach ($servicios as $item){
                        $nombre = $item['nombre'];
                        $texto = $item['texto'];
                        $url = "&id=".$item['id'];
                        if ($item['nomb_deta'] != ''){
                            $nombre.= "/".$item['nomb_deta'];
                            $texto = $item['textodeta'];
                            $url.="&id_deta=".$item['id_ds'];
                        }
                        ?>
                    <div class="result-block">
                    	<h2 class="subtitle">Servicios/<?php echo $nombre?></h2>
                        <p><?php echo substr(strip_tags($texto),0,300); ?></p>
                        <div class="align-right">
                            <a class="button button-orange" href="index.php?seccion=detalle_servicio<?php echo $url?>" >VER MÁS</a>
                        </div>
                    </div>
                    <?php }?>
                    	
                    
                  
                </div>
            </div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

