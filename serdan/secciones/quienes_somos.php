<?php include("head.php"); ?>
<?php include("menu.php"); 
$cquienes = new Dbinstitucional();

?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(2);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>

<section>
	<div class="content">
    	<div class="title">
        	<h3>Quiénes Somos</h3>
        </div>
        <div class="clear"></div>
        <div class="row-fluid">
        	<div class="sidebar span3">
                <?php include("sidebar.php"); 
                $datos_quienes = $cquienes->getByPk($id);
                
                ?>
            </div>
            <div class="box span9">
                <div class="box-body">
                	<h2 class="subtitle"><?php echo $datos_quienes['nombre']?></h2>
                	<img src="imagenes/info_inst/<?php echo $datos_quienes['imagen']?>" class="img-box" />
                    <p><?php echo $datos_quienes['texto']?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("footer.php"); ?>

