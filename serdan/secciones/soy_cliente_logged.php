<?php include("head.php"); ?>
<?php include("menu.php"); ?>
	
<?php $csecciones= new Dbsecciones();
$datos_secciones = $csecciones->getByPk(4);
if ($datos_secciones['banner'] == 1){
    $cbanner = new Dbbanner();
$dats_banner['tipo'] = 1;
$dats_banner['where'] = "order by orden";
$lista_banner = $cbanner->getList($dats_banner);
?>	
<section>
	<div class="main-slide <?php if ($datos_secciones['estado'] != 1){ echo "cerrada"; } ?>">
        <ul class="bjqs">
        
        <?php foreach ($lista_banner as $item){?>
        	<li>
                <img src="imagenes/banner/<?php echo $item['imagen']?>" />
                <div class="slide-info-block">
                	<div class="slide-info">
                    	<div class="slide-txt">
                            <h4><?php echo $item['texto']?></h4>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="markers-block">
        
        </div>
    </div>
    <div class="slide-control">
    	<div class="btn-slide"></div>
    </div>
</section>
<?php }?>


<section>
	<div class="content">
    	<div class="title">
        	<h3>Soy Cliente</h3>
        </div>
        <div class="clear"></div>
    	<div class="row-fluid">
        	<div class="box span12">
                <div class="box-body">
                	<img src="assets/img/section_img.jpg" class="img-box" />
                    <p></p>
                    <h2 class="subtitle">Modelo de servicio</h2>
                    <div class="row-fluid">
                    	<div class="span6">
                        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies arcu quis ipsum placerat aliquam. Suspendisse eu pulvinar. Fusce at varius felis. Nam eros quam, consectetur vitae magna ac, ullamcorper auctor turpis. Sed ac hendrerit massa, non ultricies.</p>
                        </div>
                        <div class="span6">
                        	<img src="assets/img/section_img.jpg" class="img-box" />
                        </div>
                    </div>
                    <p></p>
                    <h2 class="subtitle">Zona transaccional clientes</h2>
                	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies arcu quis ipsum placerat aliquam. Suspendisse eu pulvinar. Fusce at varius felis. Nam eros quam, consectetur vitae magna ac, ullamcorper auctor turpis. Sed ac hendrerit massa, non ultricies.</p>
                    
                    <h2 class="subtitle">Enlaces de Interés</h2>
                    <ul class="link-list">
                    	<li>
                        	<a href ="http://es.wikipedia.org/wiki/Sitio_web" target="_blank">http://es.wikipedia.org/wiki/Sitio_web</a>
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod ante eget ante molestie, ut dictum sem tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i>
                        </li>
                        <li>
                        	<a href ="http://es.wikipedia.org/wiki/Sitio_web" target="_blank">http://es.wikipedia.org/wiki/Sitio_web</a>
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod ante eget ante molestie, ut dictum sem tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i>
                        </li>
                        <li>
                        	<a href ="http://es.wikipedia.org/wiki/Sitio_web" target="_blank">http://es.wikipedia.org/wiki/Sitio_web</a>
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod ante eget ante molestie, ut dictum sem tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i>
                        </li>
                        <li>
                        	<a href ="http://es.wikipedia.org/wiki/Sitio_web" target="_blank">http://es.wikipedia.org/wiki/Sitio_web</a>
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod ante eget ante molestie, ut dictum sem tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i>
                        </li>
                        <li>
                        	<a href ="http://es.wikipedia.org/wiki/Sitio_web" target="_blank">http://es.wikipedia.org/wiki/Sitio_web</a>
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod ante eget ante molestie, ut dictum sem tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i>
                        </li>
                    </ul>
                    <a href="soy_cliente.php?sec=3" class="button button-big button-orange">DESCONECTAR</a>
                    
                </div>
            </div>
        </div>
    </div>
</section>


<?php include("footer.php"); ?>

