<header>
	<div class="header-block">
    	<div class="header">
            <a href="index.php"><img src="assets/img/logo.png" class="logo" /></a>
            <div class="header-right">
                <ul class="main-nav">
                    <li class="<?php if ($_GET['seccion'] == 'quienes_somos' || $_GET['seccion'] == 'quienes_somos_mapa'){echo 'active';}?>"><a href="index.php?seccion=quienes_somos">Quiénes Somos</a></li>
                    <li class="<?php if ($_GET['seccion'] == 'portafolio'){echo 'active';}?>"><a href="index.php?seccion=portafolio">Portafolio de Servicios</a></li>
                    <li class="<?php if ($_GET['seccion'] == 'soy_cliente'){echo 'active';}?>"><a href="index.php?seccion=soy_cliente">Soy Cliente</a></li>
                    <li class="<?php if ($_GET['seccion'] == 'soy_candidato'){echo 'active';}?>"><a href="index.php?seccion=soy_candidato">Soy Candidato</a></li>
                    <li class="<?php if ($_GET['seccion'] == 'soy_colaborador'){echo 'active';}?>"><a href="index.php?seccion=soy_colaborador">Soy Colaborador</a></li> 
                    <li class="<?php if ($_GET['seccion'] == 'contactenos'){echo 'active';}?>"><a href="index.php?seccion=contactenos">Contáctenos</a></li>
                    <div class="clear"></div>
                </ul>
                <img src="assets/img/search-icon.png" class="btn-search"/>
                <div class="clear"></div>
                <form class="search-form" action="index.php?seccion=busqueda" method="post">
                    <input type="text" name="buscar" id="buscar" />
                    <input type="submit" value="Buscar" />
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</header>