$(window).resize(function(){
	
	var ww = $(window).width();
	var hh = $(window).height();
	
	/*  SLIDE  */
	$(".bjqs-slide img").each(function(){
		if(ww <= $(this).width()){
			var dif = $(this).width() - ww;
			dif = dif / 2;
			$(this).css({
				'margin-left': -dif	
			})
		}

	})
	
});
$(document).ready(function() {
	
	var ww = $(window).width();
	var hh = $(window).height();
	
	
	/* Buscador header */
	$(".btn-search").click(function(event){
		$(".search-form").fadeIn();
		$(".search-form input[type='text']").focus();	
		event.stopPropagation();
	})	
	$('html').click(function() {
	   $(".search-form").fadeOut();	
	});
	
	/*  SLIDE  */
	if($('.main-slide').length) {
		$('.main-slide').bjqs({
		  'animation' : 'fade',
		  'width' : '100%',
		  'height':'398',
		  'automatic' : false
		});
	}
	if($('.high-slide').length) {
		$('.high-slide').bjqs({
		  'animation' : 'fade',
		  'width' : '458',
		  'height':'138',
		  'automatic' : false
		});
	}
	$(".bjqs-slide img").each(function(){
		$(this).parent().show();
		if(ww <= $(this).width()){
			var dif = $(this).width() - ww;
			dif = dif / 2;
			$(this).css({
				'margin-left': -dif	
			})
		}
		if($(this).parent().index() != 0){
			$(this).parent().hide();
		}
	})
	$(".main-slide .bjqs-markers").appendTo($(".markers-block"));
	$(".btn-slide").click(function(){
		if($(this).hasClass("closed")){
			$(".main-slide").animate({
				'height' : '398'	
			})
			$(this).removeClass("closed");
		}else{
			$(".main-slide").animate({
				'height' : '0'	
			})
			$(this).addClass("closed");
		}
	})
	
	
	/*  SCROLLPANE  */
	$(".scroll-content").jScrollPane();
	
	/*  CHECKBOX  */
	if($('input[type="checkbox"]').length || $('input[type="radio"]').length){
		$('input').ezMark();
	}
	
	/*  FANCYBOX  */
	if($('.modal-large').length){
		$('.modal-large').fancybox({"width":1000});
	}
	if($('.modal-medium').length){
		$('.modal-medium').fancybox({"width":750});
	}
	if($('.modal-small').length){
		$('.modal-small').fancybox({"width":500});
	}
	
	/* ACORDEÓN */
	$(".acc-element").each(function(){
		if($(this).hasClass("open")){
		}else{
			$('.acc-body', this).hide();
		}
	})
	$(".acc-element .acc-header").click(function(){
		if($(this).parent().hasClass("open")){
			$(this).parent().removeClass("open");
			$('.acc-body',$(this).parent()).slideToggle();	
		}else{
			$('.open .acc-body',$(this).parent().parent()).slideToggle();
			$('.open',$(this).parent().parent()).removeClass("open");
			$(this).parent().addClass("open");	
			$('.acc-body',$(this).parent()).slideToggle();
		}
	})
	
	
	/* TABLAS y SELECTS*/
	$(".subheader").each(function(){
		$("th:last", this).addClass("last");
	})
	
	$("tr").each(function(){
		$("td:last", this).addClass("last");
	})
	
	$(".table-stripped").each(function(){
		$("tbody tr:odd", this).addClass("even");
		$("tbody tr:even", this).addClass("odd");
	})
	
	if($('.table-dinamicl').length){
		$('.table-dinamic').dataTable();
	}
	
	$(".dataTables_wrapper .dataTables_filter input").each(function(){
		$(this).parent().addClass("form-small");	
	})
	
	if($('select').length) {
		$("select").chosen();
	}
	$("select.no-marg").next('.chzn-container').addClass("no-marg");
	
	$(".dataTables_wrapper .chzn-container").each(function(){
		$(this).parent().addClass("form-small");	
	})
	
	
	
	$(".vertical-slide").each(function(){
		var slide = $("ul", this);
		var num_slide = $("li", this).size();
		var contador = 0;
		var pos = 0;
		intervalId = setInterval(function() {
			pos = pos - 26;
			$(slide).animate({
				'top' : pos
			})
		
			if(contador == num_slide){
				contador = -1;
				
				$(slide).animate({
					'top' : '26'
				}, 1)
				pos = 26;
			}
			contador = contador + 1;
		}, 3000);
	})
	


	
});

