	<footer>
    	<div class="footer-block">
        	<div class="footer">
            	<div class="footer-social">
                	<a class="fb-ico"></a>
                    <div class="footer-div"></div>
                    <a class="tw-ico"></a>
                    <div class="footer-div"></div>
                    <a class="in-ico"></a>
                    <div class="clear"></div>
                </div>
                <div class="footer-right">
                	<p>Línea de atención a Empleados: Nacional <b>018000127673</b> - Bogotá 358 1000</p>
                    <ul class="footer-nav">
                    	<li><a href="index.php">Inicio</a></li>
                        <div class="footer-div"></div>
                        <li><a href="index.php?seccion=quienes_somos">Quiénes Somos</a></li>
                        <div class="footer-div"></div>
                        <li><a href="index.php?seccion=portafolio">Portafolio de Servicios</a></li>
                        <div class="footer-div"></div>
                        <li><a href="index.php?seccion=soy_cliente">Soy Cliente</a></li>
                        <div class="footer-div"></div>
                        <li><a href="index.php?seccion=soy_candidato">Soy Candidato</a></li>
                        <div class="footer-div"></div>
                        <li><a href="index.php?seccion=soy_colaborador">Soy Colaborador</a></li>
                        <div class="footer-div"></div> 
                        <li><a href="index.php?seccion=contactenos">Contáctenos</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="postfooter-block">
        	<p class="trade">Copyright © 2014  <b>SERDAN</b>. Todos los derechos reservados.</p>
            <div class="footer-autor"><span id="ahorranito2"></span><a href="http://www.imaginamos.com" target="_blank">Diseño Web</a><div>:</div><a href="http://www.imaginamos.com" target="_blank">imagin<span>a</span>mos.com</a></div>
			<div class="clear"></div>
        </div>
    </footer>
    
    <!-- SLIDE -->
    <script src="assets/js/basic-jquery-slider.js"></script>
    <!---->
    
    <!-- SELECTS -->
	<script src="assets/js/chosen.jquery.js"></script>
    <!---->
    
    <!-- SCROLLPANE -->
    <script type="text/javascript" src="assets/js/jquery.jscrollpane.min.js"></script>      
    <script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>      
    <!---->
    
    <!-- DATATABLE -->
	<script src="assets/js/jquery.dataTables.js"></script>
    <!---->
    
    <!-- CHECK / RADIO STYLE -->
	<script type="text/javascript" language="Javascript" src="assets/js/ezmark/js/jquery.ezmark.js"></script>
    <!---->
    
    <!-- FANCYBOX -->
	<script src="assets/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="assets/js/fancybox/jquery.fancybox.js"></script>
    <!---->
    
    <!-- LLAMADOS PLUGINS -->
    <script src="assets/js/serdan.js"></script>
    <!---->
    
    </body>
</html>