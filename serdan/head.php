<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js ie10"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<meta content="width=1024, maximum-scale=2" name="viewport">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2013" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />
<title>SERDAN</title>


<script src="assets/js/lib/jquery-1.9.1.js"></script>

<!-- BOOTSTRAP -->
<link rel="stylesheet" href="assets/css/bootstrap.css">
<!---->

<!-- SLIDE -->
<link rel="stylesheet" href="assets/css/basic-jquery-slider.css">
<!---->

<!-- COLORBOX -->
<link href="assets/css/colorbox.css" rel="stylesheet" type="text/css" />
<!---->

<!-- SELECTS -->
<link href="assets/css/chosen.css" rel="stylesheet" type="text/css" />
<!---->

<!-- SCROLLPANE -->
<link href="assets/css/jquery.jscrollpane.css" rel="stylesheet" type="text/css" />
<!---->

<!-- DATATABLE -->
<link href="assets/css/demo_table.css" rel="stylesheet" type="text/css" />
<!---->

<!-- CHECK / RADIO STYLE -->
<link href="assets/js/ezmark/css/ezmark.css" rel="stylesheet" /> 
<!----> 

<!-- FANCYBOX -->
<link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.css">
<!---->

<link href="assets/css/serdan.css" rel="stylesheet" />


</head>
<body>