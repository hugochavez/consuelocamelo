<?php

/*
 * @file               : Ajax.php
 * @brief              : Clase interaccion consultas Ajax
 * @version            : 1.0
 * @ultima_modificacion: 02-feb-2012
 * @author             : Ruben Dario Cifuentes T
 */

/*
 * @class: Ajax
 * @brief: Clase interaccion consultas Ajax
 */

class Ajax {
  /*
   * Metodo Publico para Inicializar las variables necesarias de la clase
   * @fn __construct
   * @brief Inicializa variables necesarias de la clase
   */

  public function __construct($mSecurity = NULL) {
    
  }

  // Funcion po defecto
  public function FunctDefault() {
    echo json_encode(array("title" => "Error", "message" => "Funcion por defecto en el ajax"));
  }

  // Funcion para registrar mi voto en una foto
  public function FunctAjaxEjemplo() {
    // Aca puede ir todo el PHP, consultas a DB y ejecucion de operaciones del lado del servidor
    //$valorRecibe = GetData("valor", 0);
    //$mapas = DbHandler::GetAll("SELECT * FROM ciudades WHERE idDepartamento =$valorRecibe");
    // Entregamos la respuesta impresa
    // event es la funcion en JS que captura los datos enviados por el servidor
    //echo $mapas;
    //echo json_encode(array("event"=>"AjaxEjemploJS(".json_encode($mapas).",'".$valorRecibe."')"));

    $valor = (int) GetData("valor", 0);

    $consulta = array("idDepartamento" => $valor);
    $cCiudades = new Dbciudades();
    $ciudades = $cCiudades->getList($consulta);
    for ($i = 0, $tot = count($ciudades); $i < $tot; $i++) {
      $ciudades[$i]["nombre"] = utf8_encode($ciudades[$i]["nombre"]);
    }

    $datos = json_encode(array("count" => count($ciudades), "ciudad" => $ciudades));
    echo json_encode(array("event" => "AjaxEjemploJS(" . $datos . ")"));
  }

  public function FunctAjaxEjemplo1() {
    $html = '';
    $cPqr = new Dbtipos_pqr();
    $pqr = $cPqr->getList(array("id_pqr" => (int) GetData("valor", 0)));
    
    
    for ($d = 0, $tot = count($pqr); $d < $tot; $d++) {
      $pqr[$d]["texto"] = utf8_encode($pqr[$d]["texto"]);
      $html .= '<option value="' . $pqr[$d]["texto"] . '">' . $pqr[$d]["texto"] . '</option>';
    }

    echo json_encode(array("event" => "AjaxEjemploJS1('" . $html . "');"));
  }
  
  // Obtenemos las ciudades segun el pais
  public function FunctTraerCiudades(){
    $cCiudad = new Dbciudad();
    $ciudades = $cCiudad->getList(array("pais"=>(int) GetData("valor", 0)));
    $html = '<option selected="selected" value="">Seleccione</option>';
    for ($d = 0, $tot = count($ciudades); $d < $tot; $d++) {
      $html .= '<option value="'.$ciudades[$d]["id"].'">'.utf8_encode($ciudades[$d]["nombre"]).'</option>';
    }

    echo json_encode(array("event" => "$('#ciudad').html('".$html."');$('.comboBox2').msDropDown().data('dd');"));
  }
  
  // Funcion para guardar los datos basicos del formulario
  public function FunctGuardarFrm(){
    // Guardamos los datos basicos
    $cInsert = new AjaxInsert();
    $_POST["clase"] = "proveedores";
    $_POST["estado"] = "pendiente";
    $_POST["fecha"] = date("Y-m-d H:i:s");
    $id = $cInsert->FunctInsert();
    
    $_SESSION["id_temp"] = $id;
    
    // validamos los checks marcados
    $cServicios = new Dbtipos_servicios();
    $servicios = $cServicios->getList();
    $cProvedoresTiposServicios = new Dbproveedores_tipos_servicios();
    $cProvedoresTiposServicios->setid_proveedores($id);
    for($i=0,$tot=count($servicios);$i<$tot;$i++){
      if(isset($_POST["check".$servicios[$i]["id"]])){
        $cProvedoresTiposServicios->setid_tipos_servicios($servicios[$i]["id"]);
        $cProvedoresTiposServicios->save();
      }
    }
    // Generamos submit
    echo json_encode(array("event" => "$('#frmdata').submit();"));
  }

}

?>
