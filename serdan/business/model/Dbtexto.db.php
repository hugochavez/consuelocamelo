<?php class Dbtexto extends DbDAO{
  public $id;
  protected $nombre;
  protected $valor;
  protected $tipo;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setvalor($valor){
    $this->valor = $valor;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
}
