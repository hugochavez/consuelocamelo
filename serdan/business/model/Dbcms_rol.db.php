<?php class Dbcms_rol extends DbDAO{
  public $id;
  protected $nombre;
  protected $estado;
  protected $permisos;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setestado($estado){
    $this->estado = $estado;
  }
  public function setpermisos($permisos){
    $this->permisos = $permisos;
  }
}
