<?php class Dbinstitucional extends DbDAO{
  public $id;
  protected $nombre;
  protected $imagen;
  protected $texto;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setimagen($imagen){
    $this->imagen = $imagen;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
