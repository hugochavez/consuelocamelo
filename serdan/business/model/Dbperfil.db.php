<?php class Dbperfil extends DbDAO{
  public $id;
  protected $nombre;
  protected $texto;
  protected $url;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function seturl($url){
    $this->url = $url;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
