<?php class Dbcontacto extends DbDAO{
  public $id;
  protected $nombres;
  protected $apellidos;
  protected $email;
  protected $telefono;
  protected $entidad;
  protected $asunto;
  protected $mensaje;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombres($nombres){
    $this->nombres = $nombres;
  }
  public function setapellidos($apellidos){
    $this->apellidos = $apellidos;
  }
  public function setemail($email){
    $this->email = $email;
  }
  public function settelefono($telefono){
    $this->telefono = $telefono;
  }
  public function setentidad($entidad){
    $this->entidad = $entidad;
  }
  public function setasunto($asunto){
    $this->asunto = $asunto;
  }
  public function setmensaje($mensaje){
    $this->mensaje = $mensaje;
  }
}
