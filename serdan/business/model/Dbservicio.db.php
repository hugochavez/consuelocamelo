<?php class Dbservicio extends DbDAO{
  public $id;
  protected $nombre;
  protected $texto;
  protected $logo;
  protected $imagen;
  protected $orden;
  protected $color;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setlogo($logo){
    $this->logo = $logo;
  }
  public function setimagen($imagen){
    $this->imagen = $imagen;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
  public function setcolor($color){
    $this->color = $color;
  }
  public function get_colores(){
      $colores = array();
      $colores[1]['color'] = "Verde";
      $colores[1]['clase'] = "green";
      $colores[2]['color'] = "Rojo";
      $colores[2]['clase'] = "red";
      $colores[3]['color'] = "Azul";
      $colores[3]['clase'] = "blue";
      $colores[4]['color'] = "Amarillo";
      $colores[4]['clase'] = "yellow";
      $colores[5]['color'] = "Rosa";
      $colores[5]['clase'] = "pink";
      $colores[6]['color'] = "Púrpura";
      $colores[6]['clase'] = "purple";
      $colores[7]['color'] = "Verde Claro";
      $colores[7]['clase'] = "lightgreen";
      $colores[8]['color'] = "Marrón";
      $colores[8]['clase'] = "maroon";
      return $colores;
  } 
}

