<?php class Dbpuntos extends DbDAO{
  public $id;
  protected $nombre;
  protected $direccion;
  protected $latitud;
  protected $longitud;
  protected $texto;
  protected $correo;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setdireccion($direccion){
    $this->direccion = $direccion;
  }
  public function setlatitud($latitud){
    $this->latitud = $latitud;
  }
  public function setlongitud($longitud){
    $this->longitud = $longitud;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setcorreo($correo){
    $this->correo = $correo;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
