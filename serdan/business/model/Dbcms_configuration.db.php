<?php class Dbcms_configuration extends DbDAO{
  public $config_id;
  protected $config_date;
  protected $config_path;
  protected $config_web;
  protected $config_mail_remitent;
  protected $config_company;
  public function setconfig_id($config_id){
    $this->config_id = $config_id;
  }
  public function setconfig_date($config_date){
    $this->config_date = $config_date;
  }
  public function setconfig_path($config_path){
    $this->config_path = $config_path;
  }
  public function setconfig_web($config_web){
    $this->config_web = $config_web;
  }
  public function setconfig_mail_remitent($config_mail_remitent){
    $this->config_mail_remitent = $config_mail_remitent;
  }
  public function setconfig_company($config_company){
    $this->config_company = $config_company;
  }
}
