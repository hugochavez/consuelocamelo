<?php class Dbbanner extends DbDAO{
  public $id;
  protected $tipo;
  protected $imagen;
  protected $texto;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function setimagen($imagen){
    $this->imagen = $imagen;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
