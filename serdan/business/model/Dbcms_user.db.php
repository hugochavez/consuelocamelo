<?php class Dbcms_user extends DbDAO{
  public $id_user;
  protected $username_user;
  protected $password_user;
  protected $email_user;
  protected $rol_user;
  public function setid_user($id_user){
    $this->id_user = $id_user;
  }
  public function setusername_user($username_user){
    $this->username_user = $username_user;
  }
  public function setpassword_user($password_user){
    $this->password_user = $password_user;
  }
  public function setemail_user($email_user){
    $this->email_user = $email_user;
  }
  public function setrol_user($rol_user){
    $this->rol_user = $rol_user;
  }
}
