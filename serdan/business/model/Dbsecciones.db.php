<?php class Dbsecciones extends DbDAO{
  public $id;
  protected $nombre;
  protected $banner;
  protected $estado;
  public function setid($id){
    $this->id = $id;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setbanner($banner){
    $this->banner = $banner;
  }
  public function setestado($estado){
    $this->estado = $estado;
  }
}
