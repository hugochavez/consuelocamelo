<?php class Dbdetalle_servicio extends DbDAO{
  public $id;
  protected $servicio;
  protected $nombre;
  protected $texto;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setservicio($servicio){
    $this->servicio = $servicio;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
