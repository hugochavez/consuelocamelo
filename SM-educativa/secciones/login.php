<!DOCTYPE>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js ie9"> <!--<![endif]-->

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=1124, maximum-scale=2">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<link href="assets/css/login.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="assets/js/lib/jquery-1.8.3.js"></script>

<script>
    function logueo_doc(){
        $.post("secciones/valida_userpass.php", $("#login_doc").serialize(), function(msg_2){
                        if (msg_2 == 1){
                            window.top.location.href="index.php?seccion=mis_libros";
                        }else if(msg_2 == 2){
                            window.top.location.href="index.php?seccion=mis_libros_alumno";
                        }else{
                            alert("Usuaurio y/o contaseña incorrecto");
                        }
		});
    }
    
    function logueo_alu(){
        $.post("secciones/valida_userpass.php", $("#login_alumn").serialize(), function(msg_2){
                        if (msg_2 == 1){
                            window.top.location.href="index.php?seccion=mis_libros";
                        }else if(msg_2 == 2){
                            window.top.location.href="index.php?seccion=mis_libros_alumno";
                        }else{
                            alert("Usuaurio y/o contaseña incorrecto");
                        }
		});
    }
</script>

</head>
<body class="modal">
    
   <div class="tit-line"> 
   		<h1>INGRESO</h1>
   </div>
        <div class="btn-block">
        	<div class="btn btn-alumn">
            	ALUMNO
            </div>
            <div class="btn btn-prof">
            	PROFESOR
            </div>
        </div>
        <div class="alumn-form">
            <form class="alumn-login" id="login_alumn" name="login_alumn" onsubmit="return false;">
            	<p>Ingrese sus datos de acceso</p>
                <input type="text" placeholder="Usuario" id="usuario" name="usuario">
                <input type="password" placeholder="Contraseña" id="clave" name="clave">
                <input type="submit"  onclick="logueo_alu()" class="btn" value="ACCEDER">
                <div class="opts">
                	<a class="btn-rec-alumn">Olvidé mi contraseña</a>
                    <!--<a>No estoy registrado</a> -->
                </div>
                <div class="clear"></div>
            </form>
            <form class="alumn-rec"  >
            	<p>Ingrese su email</p>
                <input type="text" placeholder="Email">
                <input type="submit"  class="btn" value="ENVIAR">
                <div class="opts">
                	<a class="btn-back-alumn">Volver</a>
                </div>
            </form>
        </div>
        
        <div class="prof-form">
        	<form class="prof-login" id="login_doc" name="login_doc"  onsubmit="return false;">
            	<p>Ingrese sus datos de acceso</p>
                <input type="text" placeholder="Usuario" id="usuario" name="usuario">
                <input type="password" placeholder="Contraseña" id="clave" name="clave">
                <input type="submit" class="btn" value="ACCEDER" onclick="logueo_doc()">
                <div id="div_sql"></div>
                <div class="opts">
                	<a class="btn-rec-prof">Olvidé mi contraseña</a>
                </div>
                <div class="clear"></div>
            </form>
            <form class="prof-rec">
            	<p>Ingrese su email</p>
                <input type="text" placeholder="Email">
                <input type="submit" class="btn" value="ENVIAR">
                <div class="opts">
                	<a class="btn-back-prof">Volver</a>
                </div>
            </form>
        </div>
	
    <!-- PLACEHOLDER EXPLORE -->
	<script src="assets/js/jquery.placeholder.js"></script>
    <!---->
    
    <script type="text/javascript" src="assets/js/sm.js"></script>
    
</body>
</html>