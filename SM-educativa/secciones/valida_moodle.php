<?php include( '../include/define.php' );
include( '../include/config.php' );
include( '../business/function/plGeneral.fnc.php' );
require_once('../lib/nusoap.php');
$username = $_GET['username'];
$fixedkey = "1N7E6R4C10NM00DL3";
$tstamp = $_GET['tstamp'];
$curso = $_GET['curso'];
$salt = time();
$token = $_GET['token'];
$isbn = $_GET['isbn'];

$token_2 = sha1($username . $fixedkey .$isbn. $tstamp);

if (($salt-$tstamp) > 30){
    //echo "Token ya caducado";
    //exit;
}
if ($token_2 != $token){
    //echo "Token no válido";
    //exit;
}

$cusuario = new Dbusuario();
$datsuser['campos_esp'] = "a.email,per.tipo,per.documento";
$datsuser['email'] = $username;
$datsuser['join'] = " INNER JOIN persona per on per.usuario = a.id ";
$dat_user = $cusuario->getList($datsuser);


    $cmetodos_ws = new Dbmetodos_ws();

$cedula = $dat_user[0]['documento'];
if ($dat_user[0]['tipo'] == 3){
    //echo "cedula".$cedula;
    $xml_libros = $cmetodos_ws->mislibros($cedula, $cedula);
    $respuesta = explode("obtenerMisLibrosResult",$xml_libros);
    if (count($respuesta)>1){
    $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
    $licencias = explode("Licencias",$contenido);
    if (count($licencias)>1){;
    $cont = $licencias[1];
    $cont = "<Licencias".$licencias[1]."Licencias>";
    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->loadXML($cont);
    $libros = $DOM->getElementsByTagName('libro');
    }
    else{
       $libros = array();   
    }
    }else{
    $libros = array();    
    }
    
    foreach($libros as $libro) {
        //echo $libro->getAttribute('isbn')."<br>";
    if ($isbn == $libro->getAttribute('isbn')){
        $licencias_arr = $libro->getElementsByTagName('licencia');
        $datos_lic = $licencias_arr->item(0)->getElementsByTagName('codigo');
        $val_codigo_licencia = $datos_lic->item(0)->nodeValue;
        
        //echo "valcodigo=".$val_codigo_licencia."guid".$guid;
        $datos_autenticacion = $cmetodos_ws->autentica_user($val_codigo_licencia, $cedula, $isbn,0);
        //var_dump($datos_autenticacion);
        $respuesta = explode("AutenticarUsuarioContenidoResult",$datos_autenticacion);
//echo $datos_autenticacion;
        $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
        $cont = "<AutenticarUsuarioContenidoResult>".$contenido."</AutenticarUsuarioContenidoResult>";
        $DOM = new DOMDocument('1.0', 'utf-8');
        $DOM->loadXML($cont);
        $url = $DOM->getElementsByTagName('URL');
        echo ($url->item(0)->nodeValue);
    }
    }
    
}elseif($dat_user[0]['tipo'] == 4){
    $clicenciasarticulo = new Dblicencias_articulo();
    $datos_licencias['campos_esp'] = "DISTINCT a.licencia,a.articulo";
    $datos_licencias['join'] = "INNER JOIN articulo art ON art.id = a.articulo
INNER JOIN detalle_paquete dp ON dp.articulo = art.id
INNER JOIN paquete pq ON pq.id = dp.paquete
INNER JOIN compradetalle cd ON cd.paquete = pq.id
INNER JOIN compra co ON co.id = cd.compra
INNER JOIN alumno al ON al.id = co.alumno
";
    
    $datos_licencias['where']=" AND al.codigo = ".$dat_user[0]['documento']." order by a.articulo";
    $lista_licencias_2 = $clicenciasarticulo->getList($datos_licencias);
   
        $arti = 0;
        $lista_licencias = array();
        
            foreach($lista_licencias_2 as $item_lic){
            if ($arti != $item_lic['articulo']){
            $lista_licencias[]=$item_lic['licencia'];
            $arti = $item_lic['articulo'];
            }
       }
       //var_dump($lista_licencias);
    $lista_licencias_def = array();
    $libros_mostrar = array();
    for ($a=0;$a<count($lista_licencias);$a++){
    //echo $lista_licencias[$a]."<br>";
    $datos_libro = $cmetodos_ws->datos_licencia($lista_licencias[$a]);
    //echo "final";

    $respuesta = explode("ObtenerDatosLicenciaResult",$datos_libro);



    $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
    $licencias = explode("<libro",$contenido);
    $licencias2 = explode("</libro>",$licencias[1]);
    $cont = $licencias2[0];
    if (count($licencias) > 1){
    $cont = "<libro".$licencias2[0]."</libro>";

    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->loadXML($cont);
    $libros_mostrar[] = $DOM->getElementsByTagName('libro');
    $lista_licencias_def[] = $lista_licencias[$a];
    }
    }
    $ind=0;
    //var_dump($libros_mostrar);
    foreach($libros_mostrar as $ob_libro) {
        //echo "entra";
        $ind++;
        //echo $ob_libro->item(0)->getElementsByTagName('ISBN')."<br>";
        $ob_isbn = $ob_libro->item(0)->getElementsByTagName('ISBN');
       // echo "isbn:".$ob_isbn->item(0)->nodeValue."<br>";
        if ($isbn == $ob_isbn->item(0)->nodeValue){
            $val_codigo_licencia = $lista_licencias_def[($ind-1)];
        }
    }
    
    
    $datos_libro = $cmetodos_ws->datos_libros($isbn);
//echo "<textarea>".$datos_libro."</textarea>";
    $respuesta = explode("ObtenerFichaLibroResult",$datos_libro);
    $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
    $licencias = explode("<Ficha",$contenido);
    if (count($licencias) <= 1){
        echo "Libro no encontrado";
        exit;
    }
    $licencias2 = explode("</Ficha>",$licencias[1]);
    $cont = $licencias2[0];
    $cont = "<Ficha".$licencias2[0]."</Ficha>";
    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->loadXML($cont);
    $libro = $DOM->getElementsByTagName('libro');
    $ob_libro = $libro->item(0);
    $datos_autenticacion = $cmetodos_ws->autentica_user($val_codigo_licencia, "", $isbn,0);
    $respuesta = explode("AutenticarUsuarioContenidoResult",$datos_autenticacion);
    //var_dump($respuesta);
//echo $datos_autenticacion;
    $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
    $cont = "<AutenticarUsuarioContenidoResult>".$contenido."</AutenticarUsuarioContenidoResult>";
    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->loadXML($cont);
    $url = $DOM->getElementsByTagName('URL');
    echo ($url->item(0)->nodeValue);
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo "FINAL";
?>

