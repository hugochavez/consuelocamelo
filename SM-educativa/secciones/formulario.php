<?php include("head_pagos.php");?>
<?php $consultas = new Dbconsultas_brujula();
$deptos = $consultas->lista_deptos();
$ccolegios = new Dbcolegio();
$lista_colegios = $ccolegios->getList();
$areas = $consultas->lista_areas_conocimiento();
?>
<script>
function cambia_depto(depto){
		$.post("secciones/sel_ciudades.php", {depto:depto,tipo:1}, function(msg_2){
           	$("#sel_ciudades").html(msg_2);
    	});
	}
var id_colegio = 0;        
var id_grado = 0;
function  cambio_grado(grado){
    id_grado = grado;
    trae_paquetes();
}

function cambio_colegio(colegio){
    id_colegio = colegio;
    trae_paquetes();
}
function trae_paquetes(){
        $.post("secciones/sel_paquetes.php", {colegio:id_colegio,grado:id_grado}, function(msg_2){
           	$("#div_paquetes").html(msg_2);
    	}); 
}

function detalles_paquete(id){
        $.post("secciones/detalles_paquete.php", {paquete:id,colegio:id_colegio}, function(msg_2){
           	$("#detalle_paquete").html(msg_2);
    	});
}

function registro(){
    $.post("secciones/guarda_registro.php", $('#form_registro').serialize(), function(msg_2){
           	if (msg_2 > 0){
                    location.href="index.php?seccion=resumen&id_usuario="+msg_2;
                }else{
                    alert("Ocurrió un problema");
                }
    	});
}


</script>
<div class="interna">
	<?php include("home_section.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="form-box">
                    <form name="form_registro" id="form_registro" method="post" onsubmit="return false;">
                	<h3>RESUMEN</h3>
                	<div class="medium">
                    	<p>Departamento</p>
                        <select id="departamento" name="departamento" onChange="cambia_depto($(this).val())">
                        <option value="">Departamento</option>
                                        <?php foreach ($deptos as $item){?>
                        <option value="<?php echo $item['DeId']?>"><?php echo $item['DeNombre']?></option>
                                        <?php }?>
                        </select>
                    </div>
                    <div class="medium no-marg">
                    	<p>Ciudad</p>
                        <div id="sel_ciudades">
				<select id="ciudad_p" name="ciudad_p" >
                	<option value="">Ciudad</option>
                        </select>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="medium">
                    	<p>Colegio</p>
                        <select id="colegio" name="colegio"  onchange="cambio_colegio($(this).val())">
                        <option value="">Colegio</option>
                                        <?php foreach ($lista_colegios as $item){?>
                        <option value="<?php echo $item['id']?>"><?php echo $item['nombre']?></option>
                                        <?php }?>
                        </select>
                    </div>
                    <div class="medium no-marg">
                    	<p>Grado</p>
                        <select id="grado" name="grado" onchange="cambio_grado($(this).val())">
                        <option value="">Grado</option>
                                        <?php for ($a=1;$a<=11;$a++){?>
                        <option value="<?php echo $a?>"><?php echo $a?></option>
                                        <?php }?>
                        </select>
                    </div>
                    <div class="clear"></div>
                    
                    <div class="medium">
                    	<p>Nombre del estudiante</p>
                        <input type="text" name="nombres">
                    </div>
                    <div class="medium no-marg">
                    	<p>Apellidos del estudiante</p>
                        <input type="text" name="apellidos">
                    </div>
                    <div id="medium">
				<select id="area" name="area[]"   multiple="multiple">
					<option value="0">Área</option>
										<?php foreach ($areas as $item){?>
                		<option value="<?php echo $item['AeId']?>"><?php echo utf8_encode($item['AeNombre'])?></option>
					<?php }?>
                </select>
		    </div>
                    <div class="clear"></div>
                    <div class="long">
                    	<p>Correo electrónico</p>
                        <input type="text" name="correo">
                    </div>
                    <div class="clear"></div>
                    <div class="clear"></div>
                    <h3>TU COMPRA</h3>
                    
                    <div class="long">
                    	<p>Paquete</p>
                        <div id="div_paquetes">
                            
                        </div>
                    </div>
                    <div class="short" id="detalle_paquete">
                    	
                    </div>
                    <div class="clear"></div>
                    <div class="check">
                    	<input type="checkbox">
                        <p>Acepto los <a class="iframe" href="terminos.php">Términos y Condiciones</a></p>
                        <div class="clear"></div>
                    </div>
                    <input type="submit" class="btn" value="ENVIAR" onclick="registro()">
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include("footer_pagos.php");?>
