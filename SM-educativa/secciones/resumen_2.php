<?php include("head_pagos.php");
    $ccolegios = new Dbcolegio();
    $id_usuario = $_GET['id_usuario'];
    $calumno = new Dbalumno();
    $datos_alu = $calumno->getByPk($id_usuario);
    $cconsultas = new Dbconsultas_brujula();
    $cpersona = new Dbpersona();
    $datos['codigo'] = $datos_alu['codigo'];
    $datpersona = $cpersona->getList($datos);
    $datos_ciu = $cconsultas->detalles_ciudad($datpersona[0]['ciudad']);
    $datos_depto = $cconsultas->detalles_depto($datos_ciu[0]['CiDepartamento']);
    $datos_cole = $ccolegios->getByPk($datos_alu['colegio']);
    $cpaquete = new Dbpaquete();
    $datos_pa['campos_esp'] = "a.*";
    $datos_pa['join'] = "INNER JOIN paquetecolegio pc on pc.paquete = a.id";
    $datos_pa['where'] = " AND pc.colegio = ".$datos_alu['colegio']." AND pc.grado = ".$datos_alu['grado']." AND fecha2 > CURDATE()";
    $lista_paquetes = $cpaquete->getList($datos_pa);
    $cdetallespaquete = new Dbdetalle_paquete();
    $datos_deta['campos_esp'] = "art.nombre as articulo,a.precio - ifnull((a.precio*dc.valor/100),0) as precio";
    $datos_deta['join'] = " INNER JOIN articulo art on art.id = a.articulo 
    INNER JOIN paquetecolegio pc on pc.paquete = a.paquete AND pc.colegio = ".$datos_alu['colegio']."
     LEFT JOIN descuento dc on dc.paquete = a.paquete AND pc.colegio = dc.colegio AND dc.detalle = art.id";
    $datos_deta['paquete'] = $lista_paquetes[0]['id'];
    $lista_detalles = $cdetallespaquete->getList($datos_deta);
    //variables_pagos
    
?>

<script>
    function act_datos(){
        $.post("secciones/act_datos.php", {id_usuario:<?php echo $id_usuario;?>,apellidos:$("#apellidos").val(),nombres:$("#nombres").val(),correo:$("#correo").val()}, function(msg_2){
           	if (msg_2 == 1){
                    $("#div_compra").show();
                }
    	});
        
    }
</script>

<div class="interna resumen">
	<?php include("home_section.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="form-box">
                    <form action="https://gateway.payulatam.com/ppp-web-gateway/" method="post">
                	<h3>RESUMEN</h3>
                	<div class="medium">
                    	<p>Departamento</p>
                        <?php echo $datos_depto[0]['DeNombre']?>
                    </div>
                    <div class="medium">
                    	<p>Ciudad</p>
                        <?php echo $datos_depto[0]['CiNombre']?>
                    </div>
                    <div class="clear"></div>
                    <div class="medium">
                    	<p>Colegio</p>
                        <?php echo $datos_cole['nombre']?>
                    </div>
                    <div class="medium">
                    	<p>Grado</p>
                        <?php echo $datos_alu['grado']?>
                    </div>
                    <div class="clear"></div>
                    
                    <div class="medium">
                    	<p>Nombres</p>
                        <?php if ($datos_alu['nombres'] != ""){
                        echo $datos_alu['nombres'];
                        }else{
                            ?>
                        <input type="text" name="nombres" id="nombres">
                            <?php
                        }?>
                    </div>
                    <div class="medium no-marg">
                    	<p>Apellidos</p>
                        <?php if ($datos_alu['apellidos'] != ""){
                        echo $datos_alu['apellidos'];
                        }else{
                            ?>
                        <input type="text" name="apellidos" id="apellidos">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    <div class="long">
                    	<p>Correo electrónico</p>
                        <?php if ($datos_alu['correo'] != ""){
                        echo $datos_alu['correo'];
                        }else{
                            ?>
                        <input type="text" name="correo" id="correo">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    <?php if("" == $datos_alu['apellidos'] && "" == $datos_alu['nombres'] && "" == $datos_alu['correo']){?>
                    <input type="button" class="btn" value="ACTUALIZAR DATOS" onclick="act_datos();">
                    <div class="clear"></div>
                    <?php }?>
                    <h3>TU COMPRA</h3>
                    <div id="div_compra" <?php if("" == $datos_alu['apellidos'] && "" == $datos_alu['nombres'] && "" == $datos_alu['correo']){?>
                         style="display: none"<?php }?>>
                    <div class="medium">
                    	<p><?php echo $lista_paquetes[0]['nombre']?></p>
                        <?php foreach ($lista_detalles as $item){?>
                            <?php echo $item['articulo']?><br>
                            <?php 
                            $total += $item['precio'];
                            }
                            $fecha = $lista_paquetes[0]['fecha1'];
                            if (mktime(0,0,0,substr($lista_paquetes[0]['fecha1'],5,2),substr($lista_paquetes[0]['fecha1'],8,2),substr($lista_paquetes[0]['fecha1'],0,4) > mktime(0,0,0,date('m'),date('d'),date('Y')))){
                                  $fecha =  $lista_paquetes[0]['fecha2'];
                                  $total += 15000;
                            }
                            ?>
                    </div>
                    <div class="clear"></div>
                    <div class="short">
                    	<p>Valor</p>
                        <?php echo number_format($total);?> Hasta <b><?php echo $fecha;?></b>
                    </div>
                    <div class="clear"></div>
                    <input type="submit" class="btn" value="COMPRAR">
                    <input type="button" class="btn" value="CANCELAR">
                    
                    <div class="clear"></div>
                    <?php $id_comercio = "501692";
                        $llave = "1tk9m64h47pla9invtl3kji9kt";
                        $login = "420d644448f0f6c";
                        $moneda = "COP";
                        $referencia = $id_usuario."a".$lista_paquetes[0]['id'];
                        $firma = $llave.'~'.$id_comercio.'~'.$referencia.'~'.$total.'~'.$moneda;
                        $signature = md5($firma);
                        $iva = 16;
                        $base_iva = $total/1.16;
                        $valor_iva = $base_iva*0.16;
                        
                        ?>
                    <input type="hidden" name="merchantId" id="merchantId" value="<?php echo $id_comercio?>" />
                    <input type="hidden" name="referenceCode" id="referenceCode" value="<?php echo $referencia?>" />
                    <input type="hidden" name="description" id="description" value="Compra de paquete<?php echo $lista_paquetes[0]['nombre']?>" />
                    <input type="hidden" name="amount" id="amount" value="<?php echo $total?>" />
                    <input type="hidden" name="tax" id="tax" value="<?php echo number_format($valor_iva,2)?>" />
                    <input type="hidden" name="taxReturnBase" id="taxReturnBase" value="<?php echo number_format($base_iva,2);?>" />
                    <input type="hidden" name="signature" id="signature" value="<?php echo $signature;?>" />
                    <input type="hidden" name="accountId" id="accountId" value="" />
                    <input type="hidden" name="currency" id="currency" value="<?php echo $moneda?>" />
                    <input type="hidden" name="buyerEmail" id="buyerEmail" value="<?php echo $datos_alu['correo'];?>" />
                    <input type="hidden" name="responseUrl" id="responseUrl" value="http://repositorio.imaginamos.com.co/SENA/SM-educativa/index.php?seccion=repuesta_pago"/>
                    <input type="hidden" name="confirmationUrl" id="confirmationUrl" value="http://repositorio.imaginamos.com.co/SENA/SM-educativa/index.php?seccion=confirmacion_pago"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include("footer_pagos.php");?>
