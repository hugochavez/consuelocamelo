<?php $consultas = new Dbconsultas_brujula();
$deptos = $consultas->lista_deptos();
$areas = $consultas->lista_areas_conocimiento();
?>
<!DOCTYPE>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js ie9"> <!--<![endif]-->

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=1124, maximum-scale=2">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<!-- SELECT Y MULTISELECT -->
<link rel="stylesheet" href="assets/css/chosen.css">
<!---->

<link href="assets/css/login.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  });
  </script>
<script>
	
	function guarda_registrodoc(){
		$.post("secciones/guarda_usuario.php", $("#form_docente").serialize(), function(msg_2){
			window.top.location.href="index.php?seccion=mis_libros";
		});
	}
        
        function guarda_registroalu(){
            $.post("secciones/guarda_usuario.php", $("#form_alumno").serialize(), function(msg_2){
			window.top.location.href="index.php?seccion=mis_libros_alumno";
		});
        }

	function comprobar_cedula(cedula){
		$.post("secciones/comprueba_cedula.php", {cedula:cedula}, function(msg_2){
            
			if (msg_2 != "no hay"){
				$("#nombres_doc").val(2);
				var myArray = msg_2.split(';');
				var Array2 = new Array;
				var depto = 0;
				var ciudad = 0;
				for(var i=0;i<myArray.length;i++){
					
					Array2 = myArray[i].split('=');
					switch (Array2[0]){
						case "PeNombres":
							$("#nombres_doc").val(Array2[1]);
								
						break;
						case "PePrimerApellido":
							$("#apellidos_doc").val(Array2[1]);	
						break;
						case "PeSegundoApellido":
							$("#apellidos_doc").val($("#apellidos_doc").val()+" "+Array2[1]);	
						break;
						case "IeId":
							$.post("secciones/sel_instituciones.php", {ciudad:ciudad,id:Array2[1]}, function(msg_3){
								$("#sel_instituciones").html(msg_3);
							});							
						break;
						case "DeId": 
							
							depto = Array2[1];
							$.post("secciones/sel_deptos.php", {id:depto}, function(msg_3){
								$("#sel_deptos2").html(msg_3);
							});
						break;
						case "PeCiudadResidencia": 
							ciudad = Array2[1];
							$.post("secciones/sel_ciudades.php", {depto :depto,id:Array2[1]}, function(msg_3){
								$("#sel_ciudades2").html(msg_3);
							});
						break;						
					}
				}
			}else{
				//alert("no registrado");
			}
    	});
		
	}
	
	function cambia_depto(depto){
		$.post("secciones/sel_ciudades.php", {depto:depto,tipo:1}, function(msg_2){
           	$("#sel_ciudades").html(msg_2);
    	});
	}
	
	function cambia_depto2(depto){
		$.post("secciones/sel_ciudades.php", {depto:depto,tipo:2}, function(msg_2){
           	$("#sel_ciudades2").html(msg_2);
    	});
	}
	
	function cambia_institucion(institucion,tipo){
		$.post("secciones/sel_grados.php", {institucion:institucion,tipo:tipo}, function(msg_2){
			if (tipo == 2){
			$("#sel_grados2").html(msg_2);
			}else{
           	$("#sel_grados").html(msg_2);
			}
    	});
	}
	
	function cambia_ciudad(ciudad,tipo){
		$.post("secciones/sel_instituciones.php", {ciudad:ciudad,tipo:tipo}, function(msg_3){
                                                            if (tipo == 2){
                                                                $("#sel_instituciones2").html(msg_3);
                                                            }else{
                                                                $("#sel_instituciones").html(msg_3);
                                                            }
							});	
	}
	
</script>

</head>
<body class="modal register-form">
    
   <div class="tit-line"> 
   		<h1>REGISTRO</h1>
   </div>
        <div class="btn-block">
        	<div class="btn btn-alumn">
            	ALUMNO
            </div>
            <div class="btn btn-prof">
            	PROFESOR
            </div>
        </div>
        <div class="alumn-form">
        	<form class="alumn-login" id="form_alumno"  onSubmit="return false;">
            	<p>Complete los campos del formulario</p>
                
                <input type="text" placeholder="C&oacute;digo" name="cedula" id="cedula">
                <input type="text" placeholder="Nombres" name="nombres" id="nombres">
                <input type="text" placeholder="Apellidos" name="apellidos" id="apellidos">
                
                
                <input type="text" placeholder="Teléfono" name="telefono" id="telefono">
                <div id="sel_deptos">
                <select id="departamento" name="departamento" onChange="cambia_depto($(this).val())">
                	<option value="">Departamento</option>
					<?php foreach ($deptos as $item){?>
                    <option value="<?php echo $item['DeId']?>"><?php echo utf8_encode($item['DeNombre']);?></option>
					<?php }?>
                </select>
				</div>
				
				<div id="sel_ciudades">
				<select id="ciudad_p" name="ciudad_p" >
                	<option value="">Ciudad</option>
                </select>
				</div>
                
                
                <div id="sel_instituciones">
                <select name="institucion" id="institucion" onChange="cambia_institucion($(this).val(),2)">
					<option value="0">Instituci&oacute;n</option>
					
				</select>
				</div>
				
				<div id="sel_grados">
				<select id="grados_p" name="grados_p" >
                	<option value="">Grado</option>
                </select>
				</div>
				
				<div id="sel_areas">
				<select id="area" name="area"  multiple="multiple">
					<option value="0">Área</option>
					<?php foreach ($areas as $item){?>
                		<option value="<?php echo $item['AeId']?>"><?php echo utf8_encode($item['AeNombre'])?></option>
					<?php }?>
                </select>
				</div>
				<div id="sel_genero">
                <select  name="genero" id="genero">
                	<option value="">Género</option>
                    <option value="1">Hombre</option>
                    <option value="2">Mujer</option>
                </select>
				</div>
                
                <input type="text" placeholder="Nacimiento" id="nacimiento" name="nacimiento" class="datepicker">
                
                <input type="text" placeholder="Email" name="email" id="email">
                <input type="text" placeholder="Repetir Email" name="email2" id="email2">
                
                <input type="password" placeholder="Contraseña" name="contrasena" id="contrasena">
                <input type="password" placeholder="Repetir Contraseña" name="contrasena2" id="contrasena2">
                
                <input type="hidden" name="tipo_reg" id="tipo_reg" value="4" >
                
                <input type="submit" class="btn" value="REGISTRARSE" onClick="guarda_registroalu();">
                <div class="opts">
                	<input type="checkbox"><a class="terms-btn">Acepto los Términos y Condiciones</a>
                </div>
                <div class="clear"></div>
                <div class="terminos">
                	<h2>TÉRMINOS Y CONDICIONES</h2>
                    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec hendrerit turpis, sed tempor arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel magna sem. Pellentesque vulputate tempor cursus. Pellentesque eros leo, iaculis vitae nisi sit amet, molestie porttitor dui. Duis vel lectus tristique, cursus magna quis, adipiscing nisl. Aliquam sed rutrum felis. Sed consequat rutrum turpis eget aliquam. Duis vel arcu id nisi elementum tincidunt ac bibendum neque. Quisque sed velit a felis dignissim rhoncus dapibus vel odio. Nunc nec convallis arcu, ac mattis massa. Integer tempus tortor at turpis porta porttitor. Etiam egestas tortor non felis cursus malesuada.<br><br>

Mauris elementum pellentesque dolor viverra laoreet. Donec erat quam, iaculis ut varius ut, dapibus sit amet metus. Suspendisse porta, ipsum in imperdiet cursus, tortor purus tincidunt felis, ac viverra risus leo sed felis. Nulla sed leo elementum, sollicitudin erat ut, feugiat magna. Duis imperdiet imperdiet nisi, eu viverra augue tempor in. Nullam quis metus mollis, aliquet metus non, pellentesque sapien. Curabitur aliquet eget mi eu mollis. Vivamus sagittis augue sed pharetra sagittis. Proin condimentum mollis dui. Mauris mollis vehicula facilisis. Mauris sit amet sagittis massa. Sed mollis tempus massa sed scelerisque. Aliquam lobortis vehicula elit, eu dictum diam laoreet condimentum.<br><br>

Vivamus pharetra eros eget bibendum bibendum. Vivamus placerat libero nisl, ac condimentum ipsum ullamcorper sit amet. Nam convallis, nibh ac sollicitudin luctus, turpis massa elementum arcu, non lobortis dui arcu eu nibh. Quisque dapibus, ipsum at aliquet malesuada, elit risus lobortis leo, non accumsan mauris dui in diam. Morbi iaculis ullamcorper neque vel tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec malesuada hendrerit rutrum. Curabitur vel enim dolor. Phasellus tempus diam et turpis tempus condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque bibendum massa sem, et elementum nunc pharetra vitae. </p>
                </div>
            </form>
           
        </div>
        
        <div class="prof-form">
        	<form class="prof-login"  id="form_docente" onSubmit="return false;">
            	<p>Complete los campos del formulario</p>
                <input type="text" placeholder="Nombres" name="nombres" id="nombres_doc">
                <input type="text" placeholder="Apellidos" name="apellidos" id="apellidos_doc">
                
                <input type="text" placeholder="Número de Cédula" name="cedula" id="cedula" onBlur="comprobar_cedula($(this).val())">
                <input type="text" placeholder="Teléfono" name="telefono" id="telefono_doc">
                <div id="sel_deptos2">
                <select id="departamento" name="departamento" onChange="cambia_depto2($(this).val())">
                	<option value="">Departamento</option>
					<?php foreach ($deptos as $item){?>
                    <option value="<?php echo $item['DeId']?>"><?php echo utf8_encode($item['DeNombre']);?></option>
					<?php }?>
                </select>
				</div>
				<div id="sel_ciudades2">
				<select id="ciudad_p" name="ciudad_p" >
                	<option value="">Ciudad</option>
                </select>
				</div>
                
                <div id="sel_instituciones2">
                <select name="institucion" id="institucion" onChange="cambia_institucion($(this).val(),2)">
					<option value="0">Instituci&oacute;n</option>
					
				</select>
				</div>
				
				<div id="sel_grados2">
				<select id="grados_p" name="grados_p"   multiple="multiple">
                	
                </select>
				</div>
				
				<div id="sel_areas">
				<select id="area" name="area[]"   multiple="multiple">
					<option value="0">Área</option>
										<?php foreach ($areas as $item){?>
                		<option value="<?php echo $item['AeId']?>"><?php echo utf8_encode($item['AeNombre'])?></option>
					<?php }?>
                </select>
				</div>
				
				<div id="sel_genero">
                <select name="genero" id="genero">
                	<option value="">Género</option>
                    <option value="1">Hombre</option>
                    <option value="2">Mujer</option>
                </select>
				</div>
                
               <input type="text" placeholder="Fecha de Nacimiento" id="nacimiento_doc" name="nacimiento" class="datepicker">
                
                <input type="text" placeholder="Email" name="email" id="email">
                <input type="text" placeholder="Repetir Email" name="email2" id="email2">
                
                <input type="password" placeholder="Contraseña" name="contrasena" id="contrasena">
                <input type="password" placeholder="Repetir Contraseña" name="contrasena2" id="contrasena2">
                
                
                <input type="hidden" name="tipo_reg" id="tipo_reg" value="3" >
                <input type="submit" class="btn" value="REGISTRARSE" onClick="guarda_registrodoc();">
                <div class="opts">
                	<input type="checkbox"><a class="terms-btn">Acepto los Términos y Condiciones</a>
                </div>
                <div class="clear"></div>
                <div class="terminos">
                	<h2>TÉRMINOS Y CONDICIONES</h2>
                    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec hendrerit turpis, sed tempor arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel magna sem. Pellentesque vulputate tempor cursus. Pellentesque eros leo, iaculis vitae nisi sit amet, molestie porttitor dui. Duis vel lectus tristique, cursus magna quis, adipiscing nisl. Aliquam sed rutrum felis. Sed consequat rutrum turpis eget aliquam. Duis vel arcu id nisi elementum tincidunt ac bibendum neque. Quisque sed velit a felis dignissim rhoncus dapibus vel odio. Nunc nec convallis arcu, ac mattis massa. Integer tempus tortor at turpis porta porttitor. Etiam egestas tortor non felis cursus malesuada.<br><br>

Mauris elementum pellentesque dolor viverra laoreet. Donec erat quam, iaculis ut varius ut, dapibus sit amet metus. Suspendisse porta, ipsum in imperdiet cursus, tortor purus tincidunt felis, ac viverra risus leo sed felis. Nulla sed leo elementum, sollicitudin erat ut, feugiat magna. Duis imperdiet imperdiet nisi, eu viverra augue tempor in. Nullam quis metus mollis, aliquet metus non, pellentesque sapien. Curabitur aliquet eget mi eu mollis. Vivamus sagittis augue sed pharetra sagittis. Proin condimentum mollis dui. Mauris mollis vehicula facilisis. Mauris sit amet sagittis massa. Sed mollis tempus massa sed scelerisque. Aliquam lobortis vehicula elit, eu dictum diam laoreet condimentum.<br><br>

Vivamus pharetra eros eget bibendum bibendum. Vivamus placerat libero nisl, ac condimentum ipsum ullamcorper sit amet. Nam convallis, nibh ac sollicitudin luctus, turpis massa elementum arcu, non lobortis dui arcu eu nibh. Quisque dapibus, ipsum at aliquet malesuada, elit risus lobortis leo, non accumsan mauris dui in diam. Morbi iaculis ullamcorper neque vel tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec malesuada hendrerit rutrum. Curabitur vel enim dolor. Phasellus tempus diam et turpis tempus condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque bibendum massa sem, et elementum nunc pharetra vitae. </p>
                </div>
            </form>
          
        </div>
	
    <!-- PLACEHOLDER EXPLORE -->
	<script src="assets/js/jquery.placeholder.js"></script>
    <!---->
    
    <!-- SELECT Y MULTISELECT -->
	<script src="assets/js/chosen.jquery.js"></script>
    <!---->
    
    <script type="text/javascript" src="assets/js/sm.js"></script>
    
</body>
</html>