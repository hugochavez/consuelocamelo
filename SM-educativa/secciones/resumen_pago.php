<?php include("head_pagos.php");
    $ccolegios = new Dbcolegio();
    $clicenciasarticulo = new Dblicencias_articulo();
    $id_usuario = $_GET['id_usuario'];
    $calumno = new Dbalumno();
    $datos_alu = $calumno->getByPk($id_usuario);
    $ccompra = new Dbcompra();
    $datos_compras['alumno']=$id_usuario;
    $datos_compras['estado']=1;
    $datos_compras['join']= "INNER JOIN compradetalle cd on cd.compra = a.id INNER JOIN paquete pq on pq.id = cd.paquete 
    INNER JOIN detalle_paquete dp ON dp.paquete = pq.id INNER JOIN articulo art on art.id = dp.articulo 
    INNER JOIN alumno al on al.id = a.alumno INNER JOIN colegio col on col.id = al.colegio 
    LEFT JOIN descuento descu ON descu.paquete = pq.id AND col.id = descu.colegio";
    $datos_compras['campos_esp'] = " DISTINCT pq.nombre as paquete, a.fecha as fecha_pago, art.nombre as articulo, cd.valor as valor,a.id as idcompra,art.id as idarticulo";
    $lista_compras = $ccompra->getList($datos_compras);
    $cconsultas = new Dbconsultas_brujula();
    //$datos['codigo'] = $datos_alu['codigo'];
    $datos_ciu = $cconsultas->detalles_ciudad($datos_alu['ciudad']);
    //$datos_depto = $cconsultas->detalles_depto($datos_ciu[0]['CiDepartamento']);
    $datos_cole = $ccolegios->getByPk($datos_alu['colegio']);
    
?>

<script>
    function act_datos(){
        
        
    }
    
    function val_datos(){
                if($("#correo").val() == '')
		{
	            alert("Ingrese un email");
                    return false;    
		}else if (!validar_email($("#correo").val())){
                    alert("El correo no es válido");
                    return false;
                }
                $.post("secciones/act_datos_compra.php", {id_usuario:<?php echo $id_usuario;?>,apellidos:$("#apellidos").val(),nombres:$("#nombres").val(),correo:$("#correo").val(),id_compra:<?php echo $lista_compras[0]['idcompra']?>}, function(msg_2){
                    alert("Datos Actualizados con éxito, se le ha enviado a su correo los datos de la compra");
                });
    }
    
    function cancelar(){
        location.href="index.php?seccion=index_conecta";
    }
    
    
    function validar_email(valor)
	{
		// creamos nuestra regla con expresiones regulares.
		var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		// utilizamos test para comprobar si el parametro valor cumple la regla
		if(filter.test(valor))
			return true;
		else
			return false;
	}
    
</script>

<div class="interna resumen">
	<?php include("home_section.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="form-box">
                    <div class="imagen_texto">
                        <img src="assets/img/logo_conecta.jpg" width="250px" >
                    </div>
                    <div class="texto">
                    <p>
                        Estimado usuario<br><br>
Queremos agradecerle la confianza depositada en SM y en su apuesta por la innovación y la calidad educativa. Esperamos poder acompañarle a lo largo de este curso con nuestros materiales, contenidos y servicios. 
<br><br>A continuación encontrará los datos básicos y la descripción de los materiales adquiridos, por favor diligencie el correo electrónico al cual serán enviados las licencias de los libros digitales  y los datos de ingreso a la plataforma educativa.

                    </p>
                    </div>
                    <form action="https://gateway.payulatam.com/ppp-web-gateway/" method="post"  id="form_pagos">
                	<h3>RESUMEN</h3>
                        
                	
                        <div class="medium" style="margin: 20px  0px 0px 0px">
                        <p>C&oacute;digo</p>
                        <input type="text" readonly="" name="codigo" id="codigo" value="<?php echo $datos_alu['codigo']?>">
                        
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Ciudad</p>
                        <input type="text" readonly="" name="ciudad" id="codigo" value="<?php echo utf8_encode($datos_ciu[0]['CiNombre'])?>">
                        
                    </div>
                    <div class="clear"></div>
                    <div class="medium" style="margin: 20px  0px 0px 0px">
                    	<p>Colegio</p>
                        <input type="text" readonly="" name="colegio" id="colegio" value="<?php echo $datos_cole['nombre']?>">
                        
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Grado</p>
                        
                        <input type="text" readonly="" name="grado" id="grado" value="<?php echo $datos_alu['grado']?>">
                    </div>
                    <div class="clear"></div>
                    
                    <div class="medium" style="margin: 20px  0px 0px 0px">
                    	<p>Nombres</p>
                        <?php if ($datos_alu['nombres'] != ""){
                        //echo $datos_alu['nombres'];
                        ?>
                        <input type="text" readonly="" name="nombres" id="nombres" value="<?php echo $datos_alu['nombres']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="nombres" id="nombres">
                            <?php
                        }?>
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Apellidos</p>
                        <?php if ($datos_alu['apellidos'] != ""){
                        //echo $datos_alu['apellidos'];
                        ?>
                        <input type="text" readonly="" name="apellidos" id="apellidos" value="<?php echo $datos_alu['apellidos']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="apellidos" id="apellidos">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    <div class="long" style="margin: 20px  0px 0px 0px">
                    	<p>Correo electrónico</p>
                        <?php if ($datos_alu['correo'] != ""){
                        //echo $datos_alu['correo'];
                        ?>
                        <input type="text" name="correo" id="correo" value="<?php echo $datos_alu['correo']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="correo" id="correo">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    
                    <div id="div_compra"  style="margin: 20px 0px 0px 0px">
                        <h3>TU COMPRA</h3>
                    <div class="medium" style="margin: 20px 0px 0px 0px">
                    	<p><b><input type="text" readonly="" name="articulo"  value="Paquete <?php echo $lista_compras[0]['paquete']?>"></b></p>
						<ul>
                        <?php foreach ($lista_compras as $item){
                            $datos_lic['articulo']=$item['idarticulo'];
                            $licencias = $clicenciasarticulo->getList($datos_lic);
                            ?>
                            <li><input type="text" readonly="" name="articulo"  value="<?php echo ($item['articulo']); 
                            if (count($licencias) > 0){
                                echo "   /   ".$licencias[0]['licencia'];
                            }
                            ?>"> </li>
                            <?php 
                            
                            }
                            
                            ?>
						</ul>
                    </div>
                        <div class="short precio">
                        
                        <p>Valor</p>
                        <input type="text" readonly="" name="fecha"  value="<?php echo number_format($lista_compras[0]['valor']);?>">
                        <input type="button" class="btn" value="ENVIAR" style="float:left;margin:20px 10px 0 0;" onclick="val_datos()">
                    <input onclick="cancelar();" type="button" class="btn" value="CANCELAR" style="float:left;margin:20px 10px 0 0;">
                    </div>
                        <div class="clear"></div>
                    
                    
                    <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

<?php include("footer_pagos.php");
?>
