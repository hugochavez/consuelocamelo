<?php include("head_pagos.php");?>



<div class="home">
	<?php include("home_header.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="login-box">
				<div class="box-header">
                	<span>CÓDIGO DEL ALUMNO</span>
                </div>
                <div class="box-body">
                    <form id="form_codigo" name="form_codigo" method="post" onsubmit="return false;">
                        <input type="text" name="codigo" id="codigo">
                        <input type="submit" class="btn" value="ENVIAR" onclick="valida_codigo();">
                        <input type="submit" class="btn" value="REGISTRO" onclick="registro();">
                        <div class="clear"></div>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<script>
    function valida_codigo(){
        $.post("secciones/valida_codigo.php", $('#form_codigo').serialize(), function(msg_2){
           	if (msg_2 > 0){
                    location.href="index.php?seccion=resumen&id_usuario="+msg_2;
                }else{
                    alert("EL código ingresado no existe.");
                }
    	});
    }
    
    function registro(){
        location.href = "index.php?seccion=formulario";
    }
</script>

<?php include("footer_pagos.php");?>
