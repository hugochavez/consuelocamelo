<?php include("head.php");
ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
$cusuario = new Dbusuario();
$cpersona = new Dbpersona();
$clicencias = new Dblicencias_cursos();



//echo "user=".$_SESSION['id_usuario'];
$datos_user = $cusuario->getByPk($_SESSION['id_usuario']);

$datos['usuario'] =  $datos_user['id'];
$datos_persona = $cpersona->getList($datos);
$datos_lic['colegio']=$datos_persona[0]['institucion'];
$datos_lic['curso']=$datos_persona[0]['grado'];

$carea = new Dbareausuario();
$dats_area['usuario']=$datos_user['id'];
$lista_areas = $carea->getList($dats_area);
$areas = "";
foreach ($lista_areas as $area){
    $areas.=",".$area;
}
$areas = substr($areas, 1);

$datos_lic['where']=" AND area IN (".$areas.")";
$licencias = $clicencias->getList($dats_area);
$laslicencias = "";
foreach ($licencias as $lic){
    $laslicencias .= ",".$lic['licencia'];
}
$laslicencias = substr($laslicencias,1);
$lista_licencias = explode(",",$laslicencias);
$libros_mostrar = array();
$persona = $datos_persona[0];
$cmetodos_ws = new Dbmetodos_ws();
$libros_mostrar = array();

$calumno = new Dbalumno();
$dats_alum['codigo'] = $datos_persona[0]['documento'];
$dats_alumno = $calumno->getList($dats_alum);
if (count($dats_alumno) > 0){
    $clicenciasarticulo = new Dblicencias_articulo();
    $datos_licencias['campos_esp'] = "DISTINCT a.licencia,a.articulo";
    $datos_licencias['join'] = "INNER JOIN articulo art ON art.id = a.articulo
INNER JOIN detalle_paquete dp ON dp.articulo = art.id
INNER JOIN paquete pq ON pq.id = dp.paquete
INNER JOIN compradetalle cd ON cd.paquete = pq.id
INNER JOIN compra co ON co.id = cd.compra
INNER JOIN alumno al ON al.id = co.alumno";
    $datos_licencias['where']=" AND al.id = ".$dats_alumno[0]['id']." order by a.articulo";
    $lista_licencias_2 = $clicenciasarticulo->getList($datos_licencias);
    $arti = 0;
    foreach($lista_licencias_2 as $item_lic){
        if ($arti != $item_lic['articulo']){
        $lista_licencias[]=$item_lic['licencia'];
        $arti = $item_lic['articulo'];
        }
    }
}

$lista_licencias_def = array();
for ($a=0;$a<count($lista_licencias);$a++){
//echo $lista_licencias[$a]."<br>";
$datos_libro = $cmetodos_ws->datos_licencia($lista_licencias[$a]);
//echo "final";

$respuesta = explode("ObtenerDatosLicenciaResult",$datos_libro);



$contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
$licencias = explode("<libro",$contenido);
$licencias2 = explode("</libro>",$licencias[1]);
$cont = $licencias2[0];
if (count($licencias) > 1){
$cont = "<libro".$licencias2[0]."</libro>";

$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->loadXML($cont);
$libros_mostrar[] = $DOM->getElementsByTagName('libro');
$lista_licencias_def[] = $lista_licencias[$a];
}
}

//exit;
?>
<script>
    function modifica_clave(){
        $.post("secciones/modifica_clave.php", $("#form_nuevopass").serialize(), function(msg_2){
            //$("#div_xml").html(msg_2);
            if (msg_2 == "OK"){
                alert("Contraseña modificada con éxito");
            }
	});
    }
    
    function detalle_libro(isbn,cod_licencia){
        $.post("secciones/detalle_libro.php", {isbn:isbn,cod_licencia:cod_licencia}, function(msg_2){
            $("#div_detalibro").html(msg_2);
		});
    }
    
    function activa_licencia(){
        $.post("secciones/activa_licencia.php", $("#form_licencia").serialize(), function(msg_2){
            //$("#div_xml").html(msg_2);
            if (msg_2 == "OK"){
                alert("licencia agregada con éxito");
                location.href="index.php?seccion=mis_libros";
            }
	});
    }
</script>


<script>
    function mandar_moddle(){
        $.post("secciones/mandar_moodle.php", {documento:'<?php echo $datos_user['email'];?>'}, function(msg_2){
            location.href=msg_2;
	});
    }
</script>

<div id="div_xml"></div>

<div class="results-block">
    <div class="div_datosusu">
        <b>Bienvenido</b> <?php echo $persona['nombre']." ".$persona['apellido']?>
                 </div>
    <div class="zona-actions">
            
        <ul>
        	
            <li><a class="btn-login"  href="javascript:;" onclick="mandar_moddle()">AULA DIGITAL</a></li>
            <li><a class="btn-login btn-nuevaclave-book">CAMBIAR CLAVE</a></li>
                <li><a class="btn-login" href="secciones/salir.php" >CERRAR SESIÓN</a></li>
                
        </ul>
    </div>
    <div class="clear"></div>
    <div class="act-book">
        <form name="form_licencia" id="form_licencia"  onsubmit="return false;">
        	<label>Ingrese su número de licencia para registrar un nuevo libro</label>
                <input type="text" name="licencia" id="licencia">
            <input type="submit" value="REGISTRAR" class="btn" onclick="activa_licencia();">
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="nuevaclave-book">
        <form name="form_nuevopass" id="form_nuevopass"  onsubmit="return false;">
        	<label>Ingrese su nueva contaseña</label>
                <input type="password" name="contrasena" id="contrasena">
            <input type="submit" value="ACEPTAR" class="btn" onclick="modifica_clave();">
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
	<h3>MIS LIBROS</h3>
    
</div>
<div class="slider">
	<a class="btn-prev"></a>
    <div class="slider-block">
        
        <ul>
            <li>
                <?php $ind=0;
                foreach($libros_mostrar as $ob_libro) {
                    $ind++;
$titulo = $ob_libro->item(0)->getElementsByTagName('titulo');
$portada = $ob_libro->item(0)->getElementsByTagName('urlportada');
$isbn = $ob_libro->item(0)->getElementsByTagName('ISBN');
$val_codigo = $lista_licencias_def[($ind-1)];
?>
                <a><div class="book">
                        
                    <div class="book-img">
                        <img src="<?php echo $portada->item(0)->nodeValue;?>">
                    </div>
                        <a href="javascript:;" onclick ="detalle_libro('<?php echo $isbn->item(0)->nodeValue;?>','<?php echo $val_codigo?>')">    
                    <div class="book-info">
                        <h3><?php echo $titulo->item(0)->nodeValue;?></h3>
                        <p><br> <a href="javascript:;" onclick ="detalle_libro('<?php echo $isbn->item(0)->nodeValue;?>','<?php echo $val_codigo?>')">» Más información</a
                                ></p>
                    </div></a>
                </div></a>
                <?php if ($ind % 4 == 0){
                    ?>
                    <div class="clear"></div>
                    </li>
                    </ul>
                    <ul>
                    <li>
                <?php
                }
                
                }?>
                <div class="clear"></div>
            </li>
          </ul>
    </div>
    <a class="btn-next"></a>
    <div class="clear"></div>
</div>

<div class="results-block" id="div_detalibro">
	
</div>

<?php include("footer.php");?>
