<?php session_start();
include( '../include/define.php' );
include( '../include/config.php' );
include( '../business/function/plGeneral.fnc.php' );
require_once('../lib/nusoap.php');
ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
$cod_licencia = $_REQUEST['cod_licencia'];

$cusuario = new Dbusuario();
$cpersona = new Dbpersona();

$datos_user = $cusuario->getByPk($_SESSION['id_usuario']);
$datos['usuario'] =  $datos_user['id'];
$datos_persona = $cpersona->getList($datos);
$persona = $datos_persona[0];

$isbn = $_REQUEST['isbn'];
$cdbmetodos_ws = new Dbmetodos_ws();
$datos_libro = $cdbmetodos_ws->datos_libros($isbn);
//echo "<textarea>".$datos_libro."</textarea>";
$respuesta = explode("ObtenerFichaLibroResult",$datos_libro);
$contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
$licencias = explode("<Ficha",$contenido);
if (count($licencias) <= 1){
    echo "Libro no encontrado";
    exit;
}
$licencias2 = explode("</Ficha>",$licencias[1]);
$cont = $licencias2[0];
$cont = "<Ficha".$licencias2[0]."</Ficha>";
$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->loadXML($cont);
$libro = $DOM->getElementsByTagName('libro');
$ob_libro = $libro->item(0);
$titulo = $ob_libro->getElementsByTagName('titulo');
$portada = $ob_libro->getElementsByTagName('urlPortadaAlta');
$sinopsis = $ob_libro->getElementsByTagName('sinopsis');
$unidades = $ob_libro->getElementsByTagName('recursosLibro');
$guid = $ob_libro->getElementsByTagName('guid');
$lista_unidades = $unidades->item(0)->getElementsByTagName('unidad');

$ccomplemtos = new Dbcomplementos_libro();
$datos_cl['libro'] = $isbn;
$num_cols = 2;
?>
<script>
    $(document).ready(function () {
    $(".tab-elem").hide();
	$(".tab li").eq(0).addClass("active").show();
	$(".tab-elem:first").show();
	$(".tab li").click(function()
	   {
		$(".tab li").removeClass("active");
		$(this).addClass("active");
		$(".tab-elem").hide();

		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false;
	});
	
	var estado_act = 0;
	
	$(".btn-act-book").click(function(){
		if(estado_act == 0){
			$(".act-book").stop(true);
			$(".act-book").animate({
				'height':'150'	
			})
			estado_act = 1;
		}else{
			$(".act-book").stop(true);
			$(".act-book").animate({
				'height':'0'	
			})
			estado_act = 0;
		}	
	})
    });
    
    function redirecciona_libro(licencia,guid){
        <?php if ($persona['tipo'] == 3){?>
            var cedula = "<?php echo $persona['documento']?>";
        <?php }else{?>
            var cedula = "";
        <?php }?>
        $.post("secciones/redireccion.php", {licencia:licencia,guid:guid,cedula:cedula}, function(msg_2){
                       //alert(msg_2);
                       $("#div_vinculo").html("<a id='lk_ir' href='"+msg_2+"' target='_blank' onclick='cerrar()'>IR</a>");
                       setTimeout('$("#lk_ir")[0].click()','1000');
                       
		});
                
             
    }
    
    function ir_recurso(licencia,guid,rec){
        <?php if ($persona['tipo'] == 3){?>
            var cedula = "<?php echo $persona['documento']?>";
        <?php }else{?>
            var cedula = "";
        <?php }?>
        $.post("secciones/redireccion.php", {licencia:licencia,guid:guid,cedula:cedula,rec:rec}, function(msg_2){
                       //alert(msg_2);
                       $("#div_vinculo").html("<a id='lk_ir_2' href='"+msg_2+"' target='_blank' onclick='cerrar()'>IR</a>");
                       setTimeout('$("#lk_ir_2")[0].click()','1000');
                       
		});
    }
    
    function cerrar(){
        $("#div_vinculo").html("");
    }
    
    
</script>
<div id="div_vinculo">
    
</div>
<h3><?php echo $titulo->item(0)->nodeValue?></h3>
    
<div class="result-img">
    	<img src="<?php echo $portada->item(0)->nodeValue?>">
    </div>
    
    <div class="result-info">
        <p><?php echo strip_tags($sinopsis->item(0)->nodeValue)?></p>
        <a class="btn" onclick="redirecciona_libro('<?php echo $cod_licencia?>','<?php echo $isbn?>')">VER LIBRO DIGITAL</a>
        
    </div>

    <div class="clear"></div>
    
    <div class="tabs">
        <ul class="tab">
            <li><a href="#tab1">Contenido</a></li>
            <li><a href="#tab2">Actividades interactivas</a></li>
            <li><a href="#tab3">Enlaces </a></li>
            <li><a href="#tab4">Lección digital</a></li>
            <?php if ($persona['tipo'] == 3){?>
            <li><a href="#tab5">Actividades evaluativas</a></li>
            <li><a href="#tab6">Clases Maestras</a></li>
			<li><a href="#tab7">PLEI</a></li>
            <?php }?>
            <div class="clear"></div>
        </ul>
        <div class="tab-content">
            <div class="tab-elem" id="tab1">
                <?php $total = $lista_unidades->length;
                $actividades = array();
                $externos = array();
                $lecciones = array();
                $porlis = ceil($total/$num_cols);
                for ($a=1;$a<=$num_cols;$a++){
                    ?>
                    <ul>
                        <?php for ($b = $porlis*($a-1); $b < $porlis*($a);$b++){
                            if ($b < $total){
                            $unidad = $lista_unidades->item($b);
                            //echo "->".$b."<br>";
                            //echo $porlis;
                            //var_dump($unidad);
                            $recursos = $unidad->getElementsByTagName('rec');
                            foreach ($recursos as $rec){
                                if(substr($rec->getAttribute('titulo'),0,10) == "Actividad:"){
                                    $actividades[]=$rec;
                                }elseif($rec->getAttribute('url') != ""){
                                    $externos[] = $rec;
                                }else{
                                    $lecciones[]=$rec;
                                }
                            }
                            ?>
                                <li>
                            <div class="file"><?php echo $unidad->getAttribute('titulo');?></div>
                            <div class="type"><?php echo $unidad->getAttribute('pagIni');?></div>
                            <div class="clear"></div>
                            </li>
                       <?php }
                        }?>
                            
                    </ul>
                    <?php
                }
                ?> 
                <div class="clear"></div>
            </div>
            <div class="tab-elem" id="tab2">
                 <?php $total = count($actividades);
                    $porlis = ceil($total/$num_cols);
                    for ($a=1;$a<=$num_cols;$a++){?>
                <ul>
                <?php for ($b = $porlis*($a-1); $b < $porlis*($a);$b++){
                            if ($b < $total){
                            $act = $actividades[$b];
							$parts_titulo = explode(" p.",$act->getAttribute('titulo'));
							
                    ?>
                	<a><li>
                    	<div class="file"><a href="javascript:;" onclick="ir_recurso('<?php echo $cod_licencia?>','<?php echo $isbn?>','<?php echo $act->getAttribute('id');?>');"><?php echo $parts_titulo[0];?></a></div>
                    	<div class="type"><?php echo $parts_titulo[1];?></div>
                        <div class="clear"></div>
                    </li></a>
                    <?php }
                    }?>
                </ul>
                <?php }?>
                <div class="clear"></div>
            </div>
            <div class="tab-elem" id="tab3">
                <?php $total = count($externos);
                    $porlis = ceil($total/$num_cols);
                    for ($a=1;$a<=$num_cols;$a++){?>
                <ul>
                <?php for ($b = $porlis*($a-1); $b < $porlis*($a);$b++){
                            if ($b < $total){
                            $ext = $externos[$b];
                    ?>
                	<a><li>
                    	<div class="file"><a href="<?php echo $ext->getAttribute('url');?>" target="_blank" ><?php echo $ext->getAttribute('titulo');?></a></div>
                    	<div class="type"></div>
                        <div class="clear"></div>
                    </li></a>
                    <?php }
                    }?>
                </ul>
                <?php }?>                   
                    <div class="clear"></div>                
            </div>
            <?php for ($p=1;$p <= 4;$p++){
                if ($p == 1 || $persona['tipo'] == 3){
                $datos_cl['tipo'] = $p;
                $lista_compl1 = $ccomplemtos->getList($datos_cl);
                ?>
                <div class="tab-elem" id="tab<?php echo ($p+3);?>">
                    <?php $total = count($lista_compl1);
                    $porlis = ceil($total/$num_cols);
                    for ($a=1;$a<=$num_cols;$a++){?>
                    <ul>
                        <?php for ($b = $porlis*($a-1); $b < $porlis*($a);$b++){
                            if ($b < $total){
                            $item = $lista_compl1[$b];?>
                            <a><li>
                    	<div class="file"><a <?php if ($item['tipo_recurso'] == 1){?> href="<?php echo $item['valor'];?>" <?php }else{ ?> href="../complementos/<?php echo $item['valor'];?>" <?php }?> target="_blank" ><?php echo $item['nombre'];?></a></div>
                    	<div class="type"></div>
                        <div class="clear"></div>
                    </li></a>
                        <?php }
                        }?>
                    </ul>
                    <?php }?>
                    <div class="clear"></div>
            </div>
            <?php 
                }
            }?>
        </div>
    </div>