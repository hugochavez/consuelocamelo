<?php include("head.php");
ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
$cusuario = new Dbusuario();
$cpersona = new Dbpersona();
//echo "user=".$_SESSION['id_usuario'];
$datos_user = $cusuario->getByPk($_GET['id_usuario']);
$datos['usuario'] =  $datos_user['id'];
$datos_persona = $cpersona->getList($datos);
$persona = $datos_persona[0];
$cmetodos_ws = new Dbmetodos_ws();
$xml_libros = $cmetodos_ws->mislibros($persona['documento'], $persona['documento']);
//echo "final";
$respuesta = explode("obtenerMisLibrosResult",$xml_libros);
$contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
$licencias = explode("Licencias",$contenido);
$cont = $licencias[1];
$cont = "<Licencias".$licencias[1]."Licencias>";
$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->loadXML($cont);
$libros = $DOM->getElementsByTagName('libro');

//exit;
?>
<script>
    function detalle_libro(isbn,cod_licencia){
        $.post("secciones/detalle_libro.php", {isbn:isbn,cod_licencia:cod_licencia}, function(msg_2){
            $("#div_detalibro").html(msg_2);
		});
    }
    
    function modifica_clave(){
        $.post("secciones/modifica_clave.php", $("#form_nuevopass").serialize(), function(msg_2){
            //$("#div_xml").html(msg_2);
            if (msg_2 == "OK"){
                alert("Contraseña modificada con éxito");
            }
	});
    }
    
    function activa_licencia(){
        $.post("secciones/activa_licencia.php", $("#form_licencia").serialize(), function(msg_2){
            //$("#div_xml").html(msg_2);
            if (msg_2 == "OK"){
                alert("licencia agregada con éxito");
                location.href="index.php?seccion=mis_libros";
            }
	});
    }
</script>

<div id="div_xml"></div>

<div class="results-block">
	<div class="zona-actions">
        <ul>
        	<li><a class="btn-login btn-act-book">ACTIVAR LIBROS</a></li>
        	<li><a class="btn-login btn-nuevaclave-book">CAMBIAR CLAVE</a></li>
                <li><a class="btn-login" href="secciones/salir.php" >CERRAR SESIÓN</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="act-book">
        <form name="form_licencia" id="form_licencia"  onsubmit="return false;">
        	<label>Ingrese su número de licencia para registrar un nuevo libro</label>
                <input type="text" name="licencia" id="licencia">
            <input type="submit" value="REGISTRAR" class="btn" onclick="activa_licencia();">
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="nuevaclave-book">
        <form name="form_nuevopass" id="form_nuevopass"  onsubmit="return false;">
        	<label>Ingrese su nueva contaseña</label>
                <input type="password" name="contrasena" id="contrasena">
            <input type="submit" value="ACEPTAR" class="btn" onclick="modifica_clave();">
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
    
	<h3>MIS LIBROS</h3>
    
</div>
<div class="slider">
	<a class="btn-prev"></a>
    <div class="slider-block">
        
        <ul>
            <li>
                <?php $ind=0;
                foreach($libros as $libro) {
                    $ind++;
$ob_libro = array();
$ob_libro['id'] = $libro->getAttribute('id');
$ob_libro['guid'] = $libro->getAttribute('guid');
$ob_libro['comentarios'] = $libro->getAttribute('comentarios');
$ob_libro['entradilla'] = $libro->getAttribute('entradilla');
$ob_libro['totalPaginas'] = $libro->getAttribute('totalPaginas');
$ob_libro['isbn'] = $libro->getAttribute('isbn');
$ob_libro['idUsuario'] = $libro->getAttribute('idUsuario');
$nombre = $libro->getElementsByTagName('nombre');
$titulo = $libro->getElementsByTagName('titulo');
$licencias_arr = $libro->getElementsByTagName('licencia');
$datos_lic = $licencias_arr->item(0)->getElementsByTagName('codigo');
$val_codigo = $datos_lic->item(0)->nodeValue;
?>
                <a><div class="book">
                        
                    <div class="book-img">
                        <img src="http://contenidodigital.services.grupo-sm.com<?php echo $libro->getAttribute('urlPortada');?>">
                    </div>
                    <a href="javascript:;" onclick ="detalle_libro('<?php echo $libro->getAttribute('isbn');?>','<?php echo $val_codigo?>')">    
                    <div class="book-info">
                        <h3><?php echo $titulo->item(0)->nodeValue;?></h3>
                        <p><br> <a href="javascript:;" onclick ="detalle_libro('<?php echo $libro->getAttribute('isbn');?>','<?php echo $val_codigo?>')">» Más información</a
                                ></p>
                    </div></a>
                </div></a>
                <?php if ($ind % 4 == 0){
                    ?>
                    <div class="clear"></div>
                    </li>
                    </ul>
                    <ul>
                    <li>
                <?php
                }
                
                }?>
                <div class="clear"></div>
            </li>
          </ul>
    </div>
    <a class="btn-next"></a>
    <div class="clear"></div>
</div>

<div class="results-block" id="div_detalibro">
	
</div>

<?php include("footer.php");?>
