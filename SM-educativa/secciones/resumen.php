<?php include("head_pagos.php");
    $ccolegios = new Dbcolegio();
    $id_usuario = $_GET['id_usuario'];
    $calumno = new Dbalumno();
    $datos_alu = $calumno->getByPk($id_usuario);
    $ccompra = new Dbcompra();
    $datos_compras['alumno']=$id_usuario;
    $datos_compras['estado']=1;
    $lista_compras = $ccompra->getList($datos_compras);
    if (count($lista_compras) > 0){?>
        <div class="interna resumen">
	<?php include("home_section.php");?>
	<div class="section-block">
            <div class="section">
                <div class="form-box">
                        <h3>ESTE USUARIO YA HA ADQUIRIDO EL CORRESPONDIENTE PAQUETE</h3>
                       <input onclick="cancelar();" type="button" class="btn" value="Voler al Inicio" style="float:left;margin:20px 10px 0 0;">

                </div>
            </div>
        </div>
            <script>
                function cancelar(){
                    location.href="index.php?seccion=index_pagos";
                }
        </script>
        <?php include("footer_pagos.php");?>    
        <?php
    }
    else{
    $cconsultas = new Dbconsultas_brujula();
    //$datos['codigo'] = $datos_alu['codigo'];
    
    $datos_ciu = $cconsultas->detalles_ciudad($datos_alu['ciudad']);
    //$datos_depto = $cconsultas->detalles_depto($datos_ciu[0]['CiDepartamento']);
    $datos_cole = $ccolegios->getByPk($datos_alu['colegio']);
    $cpaquete = new Dbpaquete();
    $datos_pa['campos_esp'] = "a.*";
    $datos_pa['join'] = "INNER JOIN paquetecolegio pc on pc.paquete = a.id";
    $datos_pa['where'] = " AND pc.colegio = ".$datos_alu['colegio']." AND pc.grado = ".$datos_alu['grado']." AND fecha2 > CURDATE()";
    $lista_paquetes = $cpaquete->getList($datos_pa);
    $cdetallespaquete = new Dbdetalle_paquete();
    $datos_deta['campos_esp'] = "art.nombre as articulo,a.precio - ifnull((a.precio*dc.valor/100),0) as precio";
    $datos_deta['join'] = " INNER JOIN articulo art on art.id = a.articulo 
    INNER JOIN paquetecolegio pc on pc.paquete = a.paquete AND pc.colegio = ".$datos_alu['colegio']." AND pc.grado = ".$datos_alu['grado']."
     LEFT JOIN descuento dc on dc.paquete = a.paquete AND pc.colegio = dc.colegio AND dc.detalle = art.id";
    $datos_deta['paquete'] = $lista_paquetes[0]['id'];
    $lista_detalles = $cdetallespaquete->getList($datos_deta);
    //variables_pagos
    
?>

<script>
    function act_datos(){
        
        
    }
    
    function val_datos(){
                if($("#correo").val() == '')
		{
	            alert("Ingrese un email");
                    return false;    
		}else if (!validar_email($("#correo").val())){
                    alert("El correo no es válido");
                    return false;
                }
                $.post("secciones/act_datos.php", {id_usuario:<?php echo $id_usuario;?>,apellidos:$("#apellidos").val(),nombres:$("#nombres").val(),correo:$("#correo").val()}, function(msg_2){
           	
                if (($("#apellidos").val() != "" || $("#nombres").val() != "") && $("#correo").val() != ""){
                    $("#buyerEmail").val($("#correo").val());
                    $("#form_pagos").submit();
                    return true;
                }else{
                    alert("Debe tener completos los datos de nombres y correo");
                    return false;
                }
                });
    }
    
    function cancelar(){
        location.href="index.php?seccion=index_pagos";
    }
    
    function validar_email(valor)
	{
		// creamos nuestra regla con expresiones regulares.
		var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		// utilizamos test para comprobar si el parametro valor cumple la regla
		if(filter.test(valor))
			return true;
		else
			return false;
	}
    
</script>

<div class="interna resumen">
	<?php include("home_section.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="form-box">
                    <form action="https://gateway.payulatam.com/ppp-web-gateway/" method="post"  id="form_pagos">
                	<h3>RESUMEN</h3>
                        
                	
                        <div class="medium" style="margin: 20px  0px 0px 0px">
                        <p>C&oacute;digo</p>
                        <input type="text" readonly="" name="codigo" id="codigo" value="<?php echo $datos_alu['codigo']?>">
                        
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Ciudad</p>
                        
                        <input type="text" class="" readonly="" name="ciudad" id="ciudad" value="<?php echo utf8_encode($datos_ciu[0]['CiNombre'])?>">
                    </div>
                    <div class="clear"></div>
                    <div class="medium" style="margin: 20px  0px 0px 0px">
                    	<p>Colegio</p>
                        
                        <input type="text" readonly="" name="colegio" id="colegio" value="<?php echo $datos_cole['nombre']?>">
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Grado</p>
                        <input type="text" readonly="" name="grado" id="grado" value="<?php echo $datos_alu['grado']?>">
                        
                    </div>
                    <div class="clear"></div>
                    
                    <div class="medium" style="margin: 20px  0px 0px 0px">
                    	<p>Nombres</p>
                        <?php if ($datos_alu['nombres'] != ""){
                        //echo $datos_alu['nombres'];
                        ?>
                        <input type="text" readonly="" name="nombres" id="nombres" value="<?php echo $datos_alu['nombres']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="nombres" id="nombres">
                            <?php
                        }?>
                    </div>
                    <div class="medium no-marg" style="margin: 20px  0px 0px 0px">
                    	<p>Apellidos</p>
                        <?php if ($datos_alu['apellidos'] != ""){
                        //echo $datos_alu['apellidos'];
                        ?>
                        <input type="text" readonly="" name="apellidos" id="apellidos" value="<?php echo $datos_alu['apellidos']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="apellidos" id="apellidos">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    <div class="long" style="margin: 20px  0px 0px 0px">
                    	<p>Correo electrónico</p>
                        <?php if ($datos_alu['correo'] != ""){
                        //echo $datos_alu['correo'];
                        ?>
                        <input type="text" name="correo" id="correo" value="<?php echo $datos_alu['correo']?>">
                        <?php
                        }else{
                            ?>
                        <input type="text" name="correo" id="correo">
                            <?php
                        }?>
                    </div>
                    <div class="clear"></div>
                    
                    <div id="div_compra"  style="margin: 20px 0px 0px 0px">
                        <h3>TU COMPRA</h3>
                    <div class="medium" style="margin: 20px 0px 0px 0px">
                    	<p><b>
                            <input type="text" readonly="" name="articulo"  value="Paquete <?php echo $lista_paquetes[0]['nombre']?>">
                            </b></p>
						<ul>
                        <?php foreach ($lista_detalles as $item){?>
                            <li><input type="text" readonly="" name="articulo"  value="<?php echo ($item['articulo']); ?>">
                                </li>
                            <?php 
                            $total += $item['precio'];
                            }
                            $fecha = $lista_paquetes[0]['fecha1'];
                            //echo substr($fecha,0,4).substr($fecha,5,2).substr($fecha,8,2)."*".date('Ymd');
                            
                            if (substr($fecha,0,4).substr($fecha,5,2).substr($fecha,8,2) < date('Ymd')){
                                  $fecha =  $lista_paquetes[0]['fecha2'];
                                  $total += 15000;
                            }
                            $total = (int)$total;
                            $total = round($total/100)*100;
                            ?>
						</ul>
                    </div>
                    <div class="short precio">
                        <p>Fecha Pago</p>
                        <input type="text" readonly="" name="fecha"  value="<?php echo $fecha;?>">
                        <p>Valor</p>
                        <input type="text" readonly="" name="fecha"  value="<?php echo number_format($total);?>">
                        <input type="button" class="btn" value="COMPRAR" style="float:left;margin:20px 10px 0 0;" onclick="val_datos()">
                    <input onclick="cancelar();" type="button" class="btn" value="CANCELAR" style="float:left;margin:20px 10px 0 0;">
                    </div>
                    
                    <div class="clear"></div>
                    
                    
                    <div class="clear"></div>
                    <?php $id_comercio = "501692";
                        $llave = "1tk9m64h47pla9invtl3kji9kt";
                        $login = "420d644448f0f6c";
                        $moneda = "COP";
                        $referencia = $id_usuario."a".$lista_paquetes[0]['id'];
                        $firma = $llave.'~'.$id_comercio.'~'.$referencia.'~'.$total.'~'.$moneda;
                        $signature = md5($firma);
                        $iva = 0;
                        $base_iva = 0;
                        $valor_iva = 0;
                        
                        ?>
                    <input type="hidden" name="merchantId" id="merchantId" value="<?php echo $id_comercio?>" />
                    <input type="hidden" name="referenceCode" id="referenceCode" value="<?php echo $referencia?>" />
                    <input type="hidden" name="description" id="description" value="Compra de paquete<?php echo $lista_paquetes[0]['nombre']?>" />
                    <input type="hidden" name="amount" id="amount" value="<?php echo $total?>" />
                    <input type="hidden" name="tax" id="tax" value="<?php echo number_format($valor_iva,2)?>" />
                    <input type="hidden" name="taxReturnBase" id="taxReturnBase" value="<?php echo number_format($base_iva,2);?>" />
                    <input type="hidden" name="signature" id="signature" value="<?php echo $signature;?>" />
                    <input type="hidden" name="accountId" id="accountId" value="" />
                    <input type="hidden" name="currency" id="currency" value="<?php echo $moneda?>" />
                    <input type="hidden" name="test" id="test" value="0" />
                    <input type="hidden" name="buyerEmail" id="buyerEmail" value="<?php echo $datos_alu['correo'];?>" />
                    <input type="hidden" name="responseUrl" id="responseUrl" value="http://repositorio.imaginamos.com.co/SENA/SM-educativa/index.php?seccion=repuesta_pago"/>
                    <input type="hidden" name="confirmationUrl" id="confirmationUrl" value="http://repositorio.imaginamos.com.co/SENA/SM-educativa/index.php?seccion=confirmacion_pago"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include("footer_pagos.php");
    }?>
