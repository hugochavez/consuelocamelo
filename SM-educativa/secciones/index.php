<?php include("head.php");?>


<div class="results-block">
	<div class="zona-actions">
        <ul>
        	<li><a class="btn-login btn-act-book">ACTIVAR LIBROS</a></li>
        	<li><a class="btn-login">ACTUALIZAR DATOS</a></li>
        	<li><a class="btn-login" >CERRAR SESIÓN</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="act-book">
    	<form>
        	<label>Ingrese su número de licencia para registrar un nuevo libro</label>
            <input type="text">
            <input type="submit" value="REGISTRAR" class="btn">
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
	<h3>MIS LIBROS</h3>
    
</div>
<div class="slider">
	<a class="btn-prev"></a>
    <div class="slider-block">
        
        <ul>
            <li>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <div class="clear"></div>
            </li>
            
            <li>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <a><div class="book">
                    <div class="book-img">
                        <img src="assets/img/book-slide.jpg">
                    </div>
                    <div class="book-info">
                        <h3>NOMBRE DEL LIBRO</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum ultricies lectus eu accumsan. Suspendisse tempus justo nec enim vehicula quis pretium purus fringilla. Donec sed<br> » Más información</p>
                    </div>
                </div></a>
                <div class="clear"></div>
            </li>
            
            <div class="clear"></div>
        </ul>
        
    </div>
    <a class="btn-next"></a>
    <div class="clear"></div>
</div>

<div class="results-block">
	<h3>NOMBRE DEL LIBRO</h3>
    <div class="result-img">
    	<img src="assets/img/book-img.jpg">
    </div>
    
    <div class="result-info">
    	<p>Dignissim facilisis. Curabitur ultrices vel odio ut blandit. Curabitur rhoncus gravida nunc, ac tincidunt enim accumsan ut. Phasellus vulputate felis nulla, quis lobortis dolor lobortis eu. Aenean laoreet urna sit amet varius blandit. Fusce porta elit neque, id dignissim quam pulvinar vitae. Praesent arcu ipsum, lacinia non egestas in, malesuada quis nibh. Vestibulum vehicula tincidunt venenatis. Aliquam erat volutpat. Proin nisi quam, lacinia a lacus quis, luctus rhoncus augue. Nullam commodo, justo pharetra ultricies bibendum, libero tellus molestie lectus, non venenatis massa libero id lectus. Nam eget tellus quis.<br><br>

Praesent arcu ipsum, lacinia non egestas in, malesuada quis nibh. Vestibulum vehicula tincidunt venenatis. Aliquam erat volutpat. Proin nisi quam, lacinia a lacus quis, luctus rhoncus augue. Nullam commodo, justo pharetra ultricies bibendum, libero tellus molestie lectus.</p>
		<a class="btn">VER LIBRO DIGITAL</a>

    </div>
    <div class="clear"></div>
    
    <div class="tabs">
        <ul class="tab">
            <li><a href="#tab1">Contenido</a></li>
            <li><a href="#tab2">Lección digital</a></li>
            <li><a href="#tab3">Actividades interactivas</a></li>
            <li><a href="#tab4">Clases maestras</a></li>
            <li><a href="#tab5">Actividades evaluativas</a></li>
            <div class="clear"></div>
        </ul>
        <div class="tab-content">
            <div class="tab-elem" id="tab1">
                <ul>
                	<a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                   
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="tab-elem" id="tab2">
                <ul>
                	<a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                   
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="tab-elem" id="tab3">
                <ul>
                	<a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                   
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="tab-elem" id="tab4">
                <ul>
                	<a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                   
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="tab-elem" id="tab5">
                <ul>
                	<a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                    <a><li>
                    	<div class="file">Lorem ipsum</div>
                    	<div class="type">(pdf)</div>
                        <div class="clear"></div>
                    </li></a>
                   
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>

</div>

<?php include("footer.php");?>
