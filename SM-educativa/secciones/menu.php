<div class="site-header">
    <div class="header-cont">
        <h1 class="site-title"><a><img src="assets/img/logo.png"></a></h1>
        
        <div class="header-right-block">
            <div class="header-top-block">
                <div class="header-social">
                    <ul>
                        <li><a class="btn-login b-register fancybox fancybox.iframe" href="http://imaginamosds.com/ARG/sm-login/register.php">REGISTRO</a></li>
                        <li><a class="btn-login b-login fancybox fancybox.iframe" href="http://imaginamosds.com/ARG/sm-login/login.php">INGRESAR</a></li>
                        <li><img src="assets/img/flag-col.jpg"></li>
                        <li><img src="assets/img/flag-ecu.jpg"></li>
                        <li><a href="#" target="_blank"><img src="assets/img/ico-yt.png"></a></li>
                        <li><a href="https://www.facebook.com/pages/SM-Colombia/156921591176046" target="_blank"><img src="assets/img/ico-fb.png"></a></li>
                        <li><a href="https://twitter.com/EdicionesSM_Col" target="_blank"><img src="assets/img/ico-tw.png"></a></li>
                        <li><a href="http://imaginamosds.com/ARG/sm/contactenos" rel="Contáctenos"><img src="assets/img/ico-mail.png"></a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <div class="main-nav">
                <ul>
                    <li><a href="http://imaginamosds.com/ARG/sm/quienes-somos/?sec=1" target="_blank" rel="Quiénes Somos">Quiénes Somos</a></li>
                    <li><a href="http://imaginamosds.com/ARG/sm/sm-en-el-mundo/?sec=2" target="_blank" rel="SM en el Mundo">SM en el Mundo</a></li>
                    <li><a href="http://imaginamosds.com/ARG/sm/proyecto-educativo/?sec=3" target="_blank" rel="Proyecto Educativo">Proyecto Educativo</a></li>
                    <li><a href="http://imaginamosds.com/ARG/sm/lineas-de-negocio/?sec=4" target="_blank" rel="Líneas de negocio">Líneas de Negocio</a></li>
                    <li><a href="http://imaginamosds.com/ARG/sm/enlaces-de-interes/?sec=5" target="_blank" rel="Enlaces de Interés">Enlaces de Interés</a></li>
                    <li><a>Catálogo</a></li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>

</div>

<div class="second-nav">
    <ul>
        <li><a>PROYECTOS</a></li>
        <div class="nav-div"></div>
        <li><a href="http://imaginamosds.com/ARG/sm-educativa/educacion-integral/?subsec=5" rel="Educación integral" target="_blank">EDUCACIÓN INTEGRAL</a></li>
        <div class="nav-div"></div>
        <li><a href="http://imaginamosds.com/ARG/sm-educativa/asesoria/?subsec=4" rel="Asesorías pedagógicas" target="_blank">ASESORÍAS PEDAGÓGICAS</a></li>
        <div class="nav-div"></div>
        <li><a href="http://imaginamosds.com/ARG/sm-educativa/aula-digital/?subsec=1" rel="Aula Digital" target="_blank">AULA DIGITAL</a></li>
        <div class="nav-div"></div>
        <li><a href="http://imaginamosds.com/ARG/sm-educativa/padres/?subsec=3" rel="Padres" target="_blank">PADRES</a></li>
        <div class="nav-div"></div>
        <li><a href="http://imaginamosds.com/ARG/sm-educativa/contacte-al-editor/?subsec=6" rel="Contacte al editor" target="_blank">CONTACTE AL EDITOR</a></li>
        <div class="clear"></div>
    </ul>
</div>