<!DOCTYPE>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js ie10"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>SM</title>

<meta name="viewport" content="width=device-width; initial-scale=1.0">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />


<link href="assets/css/sm.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="assets/js/lib/jquery-1.8.3.js"></script>

</head>
<body>
	
	<h3>TÉRMINOS Y CONDICIONES</h3>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse a enim metus. Curabitur faucibus risus sit amet odio porttitor fermentum. Ut congue a diam sit amet bibendum. Pellentesque ac mi ornare, pellentesque lectus id, pharetra ante. Morbi dolor tortor, bibendum nec facilisis eu, scelerisque eget mauris. Sed at orci felis. Duis ut condimentum nibh. Nam a sodales elit, non ultrices velit. Nam vel vulputate nibh. Aliquam euismod est eu vehicula consequat. In nec nibh quis libero suscipit pretium eu in est. Suspendisse in odio lectus. Etiam accumsan tristique mattis. Nunc vel dignissim augue. Fusce varius lacus vitae molestie bibendum. Phasellus eget aliquet nunc.<br><br>

Sed ultricies magna ut lorem convallis, ut tempus sapien posuere. Donec sollicitudin turpis nec turpis mattis, vitae sagittis tellus dignissim. Nullam ipsum sapien, facilisis ac tempor et, feugiat euismod magna. Suspendisse et urna in odio iaculis vehicula eget quis ante. Vestibulum lacus felis, eleifend nec pretium nec, varius vitae neque. Quisque tincidunt venenatis lacinia. Quisque at pretium tellus. Maecenas scelerisque gravida fermentum. Praesent sit amet convallis mi, vel posuere justo. Donec a velit eu velit volutpat tempor quis id eros.<br><br>

Phasellus eget libero vel sapien dictum luctus. Curabitur aliquam commodo vulputate. Sed vel ultrices mauris, sit amet venenatis velit. Curabitur et consequat tellus. Donec at ante accumsan, viverra turpis ut, facilisis purus. Etiam interdum dignissim nunc vitae luctus. Mauris egestas sit amet ante at cursus. Morbi lobortis enim a elit lacinia ultrices. Donec vitae mauris lectus. Proin fringilla pharetra diam, eget aliquet orci varius at. Ut eu urna quis justo blandit varius a at purus. Aliquam eget porta purus. Donec vitae lacus fermentum, feugiat ligula vel, dictum justo. Aliquam erat volutpat. Nulla at leo id lorem suscipit egestas ac quis elit.<br><br>

Sed tincidunt lectus turpis, vitae convallis elit aliquet eu. In hac habitasse platea dictumst. Aenean vitae fermentum mauris, sed ornare nisl. Nunc non ipsum non ante malesuada tempor. Donec orci ligula, mollis ac enim quis, consectetur dictum lacus. Suspendisse a diam erat. Sed ultrices, mi eget sagittis pellentesque, metus lacus tincidunt massa, et tincidunt leo sem vel arcu. Praesent purus metus, dignissim nec lectus semper, commodo faucibus nibh. Vivamus adipiscing libero at velit ultrices malesuada. Proin consectetur nisi vel scelerisque eleifend. Phasellus pharetra facilisis dui, nec tristique magna vulputate id. Mauris ultricies, lectus at suscipit mattis, purus massa ultricies odio, id lacinia orci justo sit amet tortor. </p>

	<!-- CHECK / RADIO STYLE -->
	<script type="text/javascript" language="Javascript" src="assets/js/ezmark/js/jquery.ezmark.js"></script>
    <!---->
    <script src="assets/js/sm.js"></script>
    
	</body>
</html>

