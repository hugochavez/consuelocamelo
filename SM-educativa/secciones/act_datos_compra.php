<?php include( '../include/define.php' );
include( '../include/config.php' );
include( '../business/function/plGeneral.fnc.php' );
header("Content-Type: text/html; charset=iso-8859-1");
$id_usuario = "";
$ccolegio = new Dbcolegio();
if (isset($_POST['id_usuario'])){
	$id_usuario = $_POST['id_usuario'];
}
$id_compra = 0;
if (isset($_POST['id_compra'])){
	$id_compra = $_POST['id_compra'];
}
$clicenciasarticulo = new Dblicencias_articulo();
$cconsultas = new Dbconsultas_brujula();
$ccolegios = new Dbcolegio();
$ccompra = new Dbcompra();
$cpaquete = new Dbpaquete();
$cdetallespaquete = new Dbdetalle_paquete();
$ccompradetalle = new Dbcompradetalle();
$apellidos = $_POST['apellidos'];
$nombres = $_POST['nombres'];
$correo = $_POST['correo'];
$calumno = new Dbalumno();
$calumno->setid($id_usuario);
$calumno->setapellidos($apellidos);
$calumno->setnombres($nombres);
$calumno->setcorreo($correo);
$calumno->save();
$dats_alumnos = $calumno->getByPk($id_usuario);
$cusuario = new Dbusuario();
$cusuario->setemail($dats_alumnos['correo']);
$cusuario->setfecha(date('Y-m-d'));
$cusuario->setcontrasena(md5($dats_alumnos['codigo']));
$cusuario->save();
$id_user = $cusuario->getMaxId();
$cpersona = new Dbpersona();
$cpersona->setusuario($id_user);
$cpersona->setapellido($dats_alumnos['apellidos']);
$cpersona->setnombre($dats_alumnos['nombres']);
$cpersona->setciudad($dats_alumnos['ciudad']);
$cpersona->setcurso(0);
$cpersona->setdocumento($dats_alumnos['codigo']);
$dats_colegio = $ccolegio->getByPk($dats_alumnos['colegio']);
$cpersona->setinstitucion($dats_colegio['codigobrujula']);
$cpersona->settipo(4);
$cpersona->save();
$datos_ciu = $cconsultas->detalles_ciudad($dats_alumnos['ciudad']);
$datos_cole = $ccolegios->getByPk($dats_alumnos['colegio']);
    $datos_deta['compra'] = $id_compra;
    $detalle_compra = $ccompradetalle->getList($datos_deta);
    $dats_paquete = $cpaquete->getByPk($detalle_compra[0]['paquete']);
    $detalles_paquete['campos_esp'] = "art.id as idarticulo,art.nombre as articulo,a.precio - ifnull((a.precio*dc.valor/100),0) as precio";
    $detalles_paquete['join'] = " INNER JOIN articulo art on art.id = a.articulo 
    INNER JOIN paquetecolegio pc on pc.paquete = a.paquete AND pc.colegio = ".$dats_alumnos['colegio']."
     LEFT JOIN descuento dc on dc.paquete = a.paquete AND pc.colegio = dc.colegio AND dc.detalle = art.id";
    $detalles_paquete['paquete'] = $detalle_compra[0]['paquete'];
    
    $lidetalles_paquete = $cdetallespaquete->getList($detalles_paquete);
$texto_correo='<html class="no-js ie10"> <!--<![endif]-->
<head>    
<link rel="stylesheet" id="twentytwelve-style-css" href="http://www.ediciones-sm.com.co/wp-content/themes/sm/style.css?ver=3.5.1" type="text/css" media="all">
    <link rel="stylesheet" id="twentytwelve-style-css" href="http://repositorio.imaginamos.com.co/SENA/SM-educativa/assets/css/sm_pagos.css" type="text/css" media="all">
</head>
<body>
Estimado estudiante<br><br>
Queremos agradecerle la confianza depositada en SM y en su apuesta por la innovación y la calidad educativa. Esperamos poder acompañarle a lo largo de este curso con nuestros materiales, contenidos y servicios. 
<br><br>A continuación encontrará los datos básicos, la descripción de los materiales adquiridos y el usuario y la contraseña para acceder a la plataforma educativa.

<form>
                	<h3>RESUMEN</h3>
                        
                	<table><tr><td>
                        <p>C&oacute;digo</p>
                        '.$dats_alumnos['codigo'].'
                    </td>
                    <td>
                    	<p>Ciudad</p>
                        '.utf8_encode($datos_ciu[0]['CiNombre']).'
                    </td>
                    </tr>
                    <tr><td>
                    	<p>Colegio</p>
                        '.$datos_cole['nombre'].'
                    </td>
                    <td>
                    	<p>Grado</p>
                        '.$dats_alumnos['grado'].'
                    </td>
                    </tr>                    
                    <tr><td>
                    	<p>Nombres</p>
                        '.$dats_alumnos['nombres'].'
                    </td>
                    <td>
                    	<p>Apellidos</p>
                        '.$dats_alumnos['apellidos'].'
                    </td>
                    </tr>
                    <tr><td colspan="2">
                    	<p>Correo electrónico</p>
                        '.$dats_alumnos['correo'].'
                    </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                        <h3>TU COMPRA</h3><br>                    
                    	<b> Paquete'.$dats_paquete['nombre'].'</b>                   
                    <ul>';
                        foreach ($lidetalles_paquete  as $item){
                            $datos_lic['articulo']=$item['idarticulo'];
                            $licencias = $clicenciasarticulo->getList($datos_lic);
                            $texto_correo.='<li>'.($item['articulo']);
                            if (count($licencias) > 0){
                                $texto_correo.= "/".$licencias[0]['licencia'];
                            }
                            $texto_correo.='</li>';                            
                            $total += $item['precio'];
                            }
                            $fecha = $lidetalles_paquete[0]['fecha1'];
                            //echo substr($fecha,0,4).substr($fecha,5,2).substr($fecha,8,2)."*".date('Ymd');
                            if (substr($fecha,0,4).substr($fecha,5,2).substr($fecha,8,2) < date('Ymd')){
                                  
                                  $total += 15000;
                            }
                            $texto_correo.='</ul>
                    </td>
                    </tr>
                    <tr><td colspan="2">
                    	<p>Valor</p>
                        $'.number_format($total).'</b>
                    </td> 
                    </tr>
                    <tr><td colspan="2"><b></b></td></tr>
                    <tr><td colspan="2"><b><br>DATOS DE ACCESO</b></td></tr>
                    <tr><td colspan="2"><br>El acceso al Aula Digital estará disponible a partir del 27 de Enero.</td></tr>
                    <tr><td colspan="2">
                    	<p>Usuario</p>
                        '.$dats_alumnos['correo'].'
                    </td></tr>
                    <tr><td colspan="2">
                    	<p>Contraseña</p>
                        '.$dats_alumnos['codigo'].'
                    </td></tr><table>
                    <br><br>
                    Ingrese a http://www.ediciones-sm.com.co/sm-educativa/  haga clic sobre el botón de Ingreso, seleccione Alumno, digite usuario y contraseña, y haga clic en el botón Enviar.
<br><br>En la zona segura del estudiante, haga clic en el botón de Ir a moodle y tendrá acceso al Aula digital de SM.
<br><br>Un saludo

                    </body>
                    </html>
                    ';
                            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

// Cabeceras adicionales

$cabeceras .= 'From: CONECTA SM <conecta.co@grupo-sm.com>' . "\r\n";
mail($dats_alumnos['correo'], "Credenciales SM", $texto_correo,$cabeceras);
//$ccorreo = new Correo();
//$ccorreo->SendEmail($dats_alumnos['correo'], "Credenciales SM", $texto_correo);
    
if (($apellidos == "" && $nombres == "") || $correo == ""){
    echo "0";
}else{
    //echo "1";
}
?>
