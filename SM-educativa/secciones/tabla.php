<?php include("head.php");?>

<div class="interna tabla">
	<?php include("home_section.php");?>
	<div class="section-block">
    	<div class="section">
        	<div class="form-box">
            	<h3>Esta es la información sobre su <b>Pago:</b></h3>
                <table cellpadding="0" cellspacing="0">
                	<thead>
                    	<tr>
                        	<th colspan="2" width="50%">Datos del colegio</th>
                            <th colspan="2" class="no-border" width="50%">Datos del estudiante</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                        	<td class="grey" width="25%">Departamento</td>
                            <td width="25%">Lorem</td>
                            <td class="grey" width="25">IP</td>
                            <td width="25">Lorem</td>
                        </tr>
                        <tr>
                            <td class="grey">Ciudad</td>
                            <td>Lorem</td>
                            <td class="grey">Código</td>
                            <td>Lorem</td>
                        </tr>
                            <td class="grey">Colegio</td>
                            <td>Lorem</td>
                            <td class="grey">Nombre</td>
                            <td>Lorem</td>
                        <tr>
                            <td class="grey">Grado</td>
                            <td>Lorem</td>
                            <td class="grey">Apellido</td>
                            <td>Lorem</td>
                        </tr>
                    </tbody>
                    
                    <thead>
                    	<tr>
                        	<th colspan="4" width="100%" class="no-border">Información del pago ACH PSE</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                        	<td class="grey" width="25%">Medio de pago</td>
                            <td width="25%">Lorem</td>
                            <td class="grey" width="25">ID factura o pago</td>
                            <td width="25">Lorem</td>
                        </tr>
                        <tr>
                            <td class="grey">Estado</td>
                            <td>Lorem</td>
                            <td class="grey">Ticket</td>
                            <td>Lorem</td>
                        </tr>
                            <td class="grey">Total</td>
                            <td>Lorem</td>
                            <td class="grey">Descripción</td>
                            <td>Lorem</td>
                        <tr>
                            <td class="grey">Total IVA</td>
                            <td>Lorem</td>
                            <td class="grey">Código servicio</td>
                            <td>Lorem</td>
                        </tr>
                        <tr>
                            <td class="grey">Fecha</td>
                            <td>Lorem</td>
                            <td class="grey">Código banco</td>
                            <td>Lorem</td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<?php include("footer.php");?>
