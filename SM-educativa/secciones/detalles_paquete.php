<?php include( '../include/define.php' );
include( '../include/config.php' );
include( '../business/function/plGeneral.fnc.php' );
header("Content-Type: text/html; charset=iso-8859-1");
$paquete = $_REQUEST['paquete'];
$colegio = $_REQUEST['colegio'];
$cpaquete = new Dbpaquete();
$cdetallespaquete = new Dbdetalle_paquete();
$datos_paquete = $cpaquete->getByPk($paquete);
$datos['campos_esp'] = "art.nombre as articulo,a.precio - ifnull((a.precio*dc.valor/100),0) as precio";
$datos['join'] = " INNER JOIN articulo art on art.id = a.articulo 
    INNER JOIN paquetecolegio pc on pc.paquete = a.paquete AND pc.colegio = ".$colegio."
     LEFT JOIN descuento dc on dc.paquete = a.paquete AND pc.colegio = dc.colegio AND dc.detalle = art.id";
$datos['paquete'] = $paquete;
$lista_detalles = $cdetallespaquete->getList($datos);
?>
<div>
    <div>Art&iacute;culos</div>
    <ul>
    <?php $total = 0;
    foreach($lista_detalles as $item){?>
        <li>
        <?php echo $item['articulo']?>
            </li>
    <?php $total += $item['precio'];
    }?>
        </ul>
</div>
<div class="medium"><?php echo substr($datos_paquete['fecha1'],8,2)."/".substr($datos_paquete['fecha1'],5,2)."/".substr($datos_paquete['fecha1'],0,4)?></div><div class="medium no-marg">$<?php echo number_format($total)?></div><div class="clear"></div>
<div  class="medium"><?php echo substr($datos_paquete['fecha2'],8,2)."/".substr($datos_paquete['fecha2'],5,2)."/".substr($datos_paquete['fecha2'],0,4)?></div><div class="medium no-marg">$<?php echo number_format($total + 15000)?></div>
