SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `usuariosena_smeducativa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `usuariosena_smeducativa` ;

-- -----------------------------------------------------
-- Table `usuariosena_smeducativa`.`usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `usuariosena_smeducativa`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(300) NULL ,
  `contrasena` VARCHAR(300) NULL ,
  `fecha` DATE NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `usuariosena_smeducativa`.`persona`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `usuariosena_smeducativa`.`persona` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `tipo` INT NULL ,
  `documento` VARCHAR(45) NULL ,
  `nombre` VARCHAR(300) NULL ,
  `apellido` VARCHAR(300) NULL ,
  `telefono` VARCHAR(45) NULL ,
  `ciudad` INT NULL ,
  `institucion` INT NULL ,
  `grado` INT NULL ,
  `curso` INT NULL ,
  `genero` INT NULL ,
  `fecha_nacim` DATE NULL ,
  `usuario` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_persona_usuario_idx` (`usuario` ASC) ,
  CONSTRAINT `fk_persona_usuario`
    FOREIGN KEY (`usuario` )
    REFERENCES `usuariosena_smeducativa`.`usuario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `usuariosena_smeducativa`.`licencia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `usuariosena_smeducativa`.`licencia` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `usuario` INT NULL ,
  `codigo` VARCHAR(300) NULL ,
  `estado` INT NULL ,
  `codlibro` VARCHAR(300) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_licencia_usuario1_idx` (`usuario` ASC) ,
  CONSTRAINT `fk_licencia_usuario1`
    FOREIGN KEY (`usuario` )
    REFERENCES `usuariosena_smeducativa`.`usuario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
