$(document).ready(function () {
	
	/*  SELECTS  */
	if($('select').length) {
		$("select").chosen();
	}
	
	/*  PLACEHOLDER  */
	$(function() {
	 $('input, textarea').placeholder();
	});
	
	
	
	$(".book").hover(function(){
		$(".book-info", this).stop(true);
		$(".book-info", this).animate({
			'opacity':'1'	
		})	
	}, function(){
		$(".book-info", this).stop(true);
		$(".book-info", this).animate({
			'opacity':'0'	
		})	
	})
	
	/* SLIDE */
	var numThumbs = $(".slider-block ul li").size();
	var thumbsWidth = $(".slider-block ul li").width();
	var widthBox = thumbsWidth * numThumbs;
	$(".slider-block ul").width(widthBox);
	
	var currentPos = 0;
	var numOnDisplay = 1;
	$(".btn-next").click(function(){
		if (currentPos<numThumbs-numOnDisplay){
			$(".slider-block ul").animate({'left': '-='+thumbsWidth+'px'}, 300)
			currentPos++
		}
	});
	$(".btn-prev").click(function(){
		if(currentPos>0){
			$(".slider-block ul").animate({'left': '+='+thumbsWidth+'px'}, 300)
			currentPos--
		}
	});
	
	
	/* TABS */
	$(".tab-elem").hide();
	$(".tab li").eq(0).addClass("active").show();
	$(".tab-elem:first").show();
	$(".tab li").click(function()
	   {
		$(".tab li").removeClass("active");
		$(this).addClass("active");
		$(".tab-elem").hide();

		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false;
	});
	
	var estado_act = 0;
	
	$(".btn-act-book").click(function(){
		if(estado_act == 0){
			$(".act-book").stop(true);
			$(".act-book").animate({
				'height':'150'	
			})
			estado_act = 1;
		}else{
			$(".act-book").stop(true);
			$(".act-book").animate({
				'height':'0'	
			})
			estado_act = 0;
		}	
	})
        
        $(".btn-nuevaclave-book").click(function(){
		if(estado_act == 0){
			$(".nuevaclave-book").stop(true);
			$(".nuevaclave-book").animate({
				'height':'150'	
			})
			estado_act = 1;
		}else{
			$(".nuevaclave-book").stop(true);
			$(".nuevaclave-book").animate({
				'height':'0'	
			})
			estado_act = 0;
		}	
	})
        /* LOGIN */

	$(".btn-alumn").click(function(){
		$(".btn-block").animate({
			'opacity' : 0	
		}, 300, function(){
			$(".btn-block")	.hide();
			$(".alumn-form").animate({
				'height':'200'
			}, 300, function(){
				$(".alumn-form").css({
					'height':'auto'
				})
			})
		})	
	})	
	
	$(".btn-rec-alumn").click(function(){
		$(".alumn-login").animate({
			'height':'0'	
		}, 300, function(){
			$(".alumn-rec").animate({
				'height':'200'	
			}, 300, function(){
				$(".alumn-rec").css({
					'height':'auto'
				})
			})
		})	
	})
	
	$(".btn-back-alumn").click(function(){
		$(".alumn-rec").animate({
			'height':'0'	
		}, 300, function(){
			$(".alumn-login").animate({
				'height':'200'	
			}, 300, function(){
				$(".alumn-login").css({
					'height':'auto'
				})
			})
		})	
	})
	
	
	
	
	$(".btn-prof").click(function(){
		$(".btn-block").animate({
			'opacity' : 0	
		}, 300, function(){
			$(".btn-block")	.hide();
			$(".prof-form").animate({
				'height':'200'
			}, 300, function(){
				$(".prof-form").css({
					'height':'auto'
				})
			})
		})	
	})	
	
	$(".btn-rec-prof").click(function(){
		$(".prof-login").animate({
			'height':'0'	
		}, 300, function(){
			$(".prof-rec").animate({
				'height':'200'	
			}, 300, function(){
				$(".prof-rec").css({
					'height':'auto'
				})
			})
		})	
	})
	
	$(".btn-back-prof").click(function(){
		$(".prof-rec").animate({
			'height':'0'	
		}, 300, function(){
			$(".prof-login").animate({
				'height':'200'	
			}, 300, function(){
				$(".prof-login").css({
					'height':'auto'
				})
			})
		})	
	})
	
})
