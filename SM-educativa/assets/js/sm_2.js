﻿

$(document).ready(function () {
	
	
	/*  PLACEHOLDER  */
	$(function() {
	 $('input, textarea').placeholder();
	});
	
	/*  SELECTS  */
	if($('select').length) {
		$("select").chosen();
	}
	
	
	$(".terms-btn").click(function(){
		$(".terminos").show();	
	})
	/* LOGIN */

	$(".btn-alumn").click(function(){
		$(".btn-block").animate({
			'opacity' : 0	
		}, 300, function(){
			$(".btn-block")	.hide();
			$(".alumn-form").animate({
				'height':'200'
			}, 300, function(){
				$(".alumn-form").css({
					'height':'auto'
				})
			})
		})	
	})	
	
	$(".btn-rec-alumn").click(function(){
		$(".alumn-login").animate({
			'height':'0'	
		}, 300, function(){
			$(".alumn-rec").animate({
				'height':'200'	
			}, 300, function(){
				$(".alumn-rec").css({
					'height':'auto'
				})
			})
		})	
	})
	
	$(".btn-back-alumn").click(function(){
		$(".alumn-rec").animate({
			'height':'0'	
		}, 300, function(){
			$(".alumn-login").animate({
				'height':'200'	
			}, 300, function(){
				$(".alumn-login").css({
					'height':'auto'
				})
			})
		})	
	})
	
	
	
	
	$(".btn-prof").click(function(){
		$(".btn-block").animate({
			'opacity' : 0	
		}, 300, function(){
			$(".btn-block")	.hide();
			$(".prof-form").animate({
				'height':'200'
			}, 300, function(){
				$(".prof-form").css({
					'height':'auto'
				})
			})
		})	
	})	
	
	$(".btn-rec-prof").click(function(){
		$(".prof-login").animate({
			'height':'0'	
		}, 300, function(){
			$(".prof-rec").animate({
				'height':'200'	
			}, 300, function(){
				$(".prof-rec").css({
					'height':'auto'
				})
			})
		})	
	})
	
	$(".btn-back-prof").click(function(){
		$(".prof-rec").animate({
			'height':'0'	
		}, 300, function(){
			$(".prof-login").animate({
				'height':'200'	
			}, 300, function(){
				$(".prof-login").css({
					'height':'auto'
				})
			})
		})	
	})
	
})
