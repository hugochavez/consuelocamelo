<?php class Dbpaquetecolegio extends DbDAO{
  public $id;
  protected $colegio;
  protected $paquete;
  protected $grado;
  public function setid($id){
    $this->id = $id;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setpaquete($paquete){
    $this->paquete = $paquete;
  }
  public function setgrado($grado){
    $this->grado = $grado;
  }
}
