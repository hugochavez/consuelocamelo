<?php class Dblicencias_cursos extends DbDAO{
  public $id;
  protected $colegio;
  protected $curso;
  protected $area;
  protected $licencia;
  public function setid($id){
    $this->id = $id;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setcurso($curso){
    $this->curso = $curso;
  }
  public function setarea($area){
    $this->area = $area;
  }
  public function setlicencia($licencia){
    $this->licencia = $licencia;
  }
}
