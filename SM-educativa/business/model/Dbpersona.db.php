<?php class Dbpersona extends DbDAO{
  public $id;
  protected $tipo;
  protected $documento;
  protected $nombre;
  protected $apellido;
  protected $telefono;
  protected $ciudad;
  protected $institucion;
  protected $grado;
  protected $curso;
  protected $genero;
  protected $fecha_nacim;
  protected $usuario;
  public function setid($id){
    $this->id = $id;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function setdocumento($documento){
    $this->documento = $documento;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setapellido($apellido){
    $this->apellido = $apellido;
  }
  public function settelefono($telefono){
    $this->telefono = $telefono;
  }
  public function setciudad($ciudad){
    $this->ciudad = $ciudad;
  }
  public function setinstitucion($institucion){
    $this->institucion = $institucion;
  }
  public function setgrado($grado){
    $this->grado = $grado;
  }
  public function setcurso($curso){
    $this->curso = $curso;
  }
  public function setgenero($genero){
    $this->genero = $genero;
  }
  public function setfecha_nacim($fecha_nacim){
    $this->fecha_nacim = $fecha_nacim;
  }
  public function setusuario($usuario){
    $this->usuario = $usuario;
  }
}
