<?php class Dbcomplementos_libro extends DbDAO{
  public $id;
  protected $libro;
  protected $tipo;
  protected $tipo_recurso;
  protected $nombre;
  protected $valor;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setlibro($libro){
    $this->libro = $libro;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function settipo_recurso($tipo_recurso){
    $this->tipo_recurso = $tipo_recurso;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setvalor($valor){
    $this->valor = $valor;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
