<?php class Dbdetalle_paquete extends DbDAO{
  public $id;
  protected $paquete;
  protected $articulo;
  protected $precio;
  public function setid($id){
    $this->id = $id;
  }
  public function setpaquete($paquete){
    $this->paquete = $paquete;
  }
  public function setarticulo($articulo){
    $this->articulo = $articulo;
  }
  public function setprecio($precio){
    $this->precio = $precio;
  }
}
