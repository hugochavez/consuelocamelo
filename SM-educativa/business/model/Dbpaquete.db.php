<?php class Dbpaquete extends DbDAO{
  public $id;
  protected $codigo;
  protected $nombre;
  protected $descripcion;
  protected $imagen;
  protected $fecha1;
  protected $fecha2;
  protected $precio;
  protected $precio2;
  public function setid($id){
    $this->id = $id;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setdescripcion($descripcion){
    $this->descripcion = $descripcion;
  }
  public function setimagen($imagen){
    $this->imagen = $imagen;
  }
  public function setfecha1($fecha1){
    $this->fecha1 = $fecha1;
  }
  public function setfecha2($fecha2){
    $this->fecha2 = $fecha2;
  }
  public function setprecio($precio){
    $this->precio = $precio;
  }
  public function setprecio2($precio2){
    $this->precio2 = $precio2;
  }
}
