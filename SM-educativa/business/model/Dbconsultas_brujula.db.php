<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Dbconsultas_brujula{
    
    function lista_colegios($ciudad){
        $sql="SELECT
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	institucioneseducativas.IeNombreSede,
	tiposidentificacion.TpNombre,
	institucioneseducativas.IeIdentificacion,
	ciudades.CiNombre,
	departamentos.DeNombre
FROM
	institucioneseducativas
INNER JOIN tiposidentificacion ON institucioneseducativas.IeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN barrios ON institucioneseducativas.IeBarrio = barrios.BaId
INNER JOIN localidades ON barrios.BaLocalidad = localidades.LoId
INNER JOIN ciudades ON localidades.LoCiudad = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
where ciudades.CiId = ".$ciudad."
ORDER BY
	institucioneseducativas.IeId";
        return DbHandler_2::GetAll($sql);
        
    }
    
    function lista_colegios_todos(){
        $sql="SELECT
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	institucioneseducativas.IeNombreSede,
	tiposidentificacion.TpNombre,
	institucioneseducativas.IeIdentificacion,
	ciudades.CiNombre,
	departamentos.DeNombre
FROM
	institucioneseducativas
INNER JOIN tiposidentificacion ON institucioneseducativas.IeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN barrios ON institucioneseducativas.IeBarrio = barrios.BaId
INNER JOIN localidades ON barrios.BaLocalidad = localidades.LoId
INNER JOIN ciudades ON localidades.LoCiudad = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
ORDER BY
	institucioneseducativas.IeNombre";
        return DbHandler_2::GetAll($sql);
        
    }
    
    function detalle_colegiobycod($id){
        $sql="SELECT
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	institucioneseducativas.IeNombreSede,
	tiposidentificacion.TpNombre,
	institucioneseducativas.IeIdentificacion,
	ciudades.CiNombre,
	departamentos.DeNombre
FROM
	institucioneseducativas
INNER JOIN tiposidentificacion ON institucioneseducativas.IeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN barrios ON institucioneseducativas.IeBarrio = barrios.BaId
INNER JOIN localidades ON barrios.BaLocalidad = localidades.LoId
INNER JOIN ciudades ON localidades.LoCiudad = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
WHERE institucioneseducativas.IeIdentificacion = '".$id."'
ORDER BY
	institucioneseducativas.IeNombre";
        return DbHandler_2::GetAll($sql);
    }
    
    function detalle_colegio($id){
        $sql="SELECT
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	institucioneseducativas.IeNombreSede,
	tiposidentificacion.TpNombre,
	institucioneseducativas.IeIdentificacion,
	ciudades.CiNombre,
	departamentos.DeNombre
FROM
	institucioneseducativas
INNER JOIN tiposidentificacion ON institucioneseducativas.IeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN barrios ON institucioneseducativas.IeBarrio = barrios.BaId
INNER JOIN localidades ON barrios.BaLocalidad = localidades.LoId
INNER JOIN ciudades ON localidades.LoCiudad = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
WHERE institucioneseducativas.IeId = ".$id."
ORDER BY
	institucioneseducativas.IeNombre";
        return DbHandler_2::GetAll($sql);
    }
    
    function lista_cursos(){
        $sql="SELECT
	institucioneseducativas.IeId AS ID,
	institucioneseducativas.IeIdentificacion AS Dane,
	institucioneseducativas.IeCodSAP AS Sap,
	institucioneseducativas.IeNombre AS Colegio,
	jornadas.JeNombre AS Jornada,
	gradosnivelpais.GnNombre AS Grado,
	paralelos.PgNombre AS Paralelo,
	paralelos.PgNumeroAlumnos AS Poblacion,
	tipossector.TcNombre,
	temporadasnegocioperiodocalendariopais.TnNombre
FROM
	gradosjornadainstitucioneducativa
INNER JOIN gradosnivelperiodoinstitucioneducativa ON gradosjornadainstitucioneducativa.GjGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId
INNER JOIN jornadas ON gradosjornadainstitucioneducativa.GjJornada = jornadas.JeId
INNER JOIN gradosnivelpais ON gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId
INNER JOIN institucioneseducativas ON gradosnivelperiodoinstitucioneducativa.GiInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN paralelos ON gradosnivelperiodoinstitucioneducativa.GiId = paralelos.PgGradoNivelPeriodoInstitucionEducativa
INNER JOIN territoriosinstitucioneducativa ON institucioneseducativas.IeId = territoriosinstitucioneducativa.ToInstitucionEducativa
INNER JOIN territorios ON territoriosinstitucioneducativa.ToTerritorio = territorios.TsId
INNER JOIN tipossector ON institucioneseducativas.IeSector = tipossector.TcId
INNER JOIN temporadasnegocioperiodocalendariopais ON gradosnivelperiodoinstitucioneducativa.GiPeriodoCalendarioPais = temporadasnegocioperiodocalendariopais.TnPeriodoCalendarioPais
WHERE
	temporadasnegocioperiodocalendariopais.TnId = 8
ORDER BY
	Colegio ASC,
	Jornada ASC,
	Grado ASC,
	Paralelo ASC,
	Paralelo ASC";
        return DbHandler_2::GetAll($sql);
    }
    
    function detalle_grado($grado){
        $sql="SELECT DISTINCT
	institucioneseducativas.IeId AS ID,
	institucioneseducativas.IeIdentificacion AS Dane,
	institucioneseducativas.IeCodSAP AS Sap,
	institucioneseducativas.IeNombre AS Colegio,
	jornadas.JeNombre AS Jornada,
	gradosnivelpais.GnNombre AS Grado,
	gradosnivelpais.GnId,        
	paralelos.PgNombre AS Paralelo,
	paralelos.PgNumeroAlumnos AS Poblacion,
	tipossector.TcNombre,
	temporadasnegocioperiodocalendariopais.TnNombre	
FROM
	gradosjornadainstitucioneducativa
INNER JOIN gradosnivelperiodoinstitucioneducativa ON gradosjornadainstitucioneducativa.GjGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId
INNER JOIN jornadas ON gradosjornadainstitucioneducativa.GjJornada = jornadas.JeId
INNER JOIN gradosnivelpais ON gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId
INNER JOIN institucioneseducativas ON gradosnivelperiodoinstitucioneducativa.GiInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN paralelos ON gradosnivelperiodoinstitucioneducativa.GiId = paralelos.PgGradoNivelPeriodoInstitucionEducativa
INNER JOIN territoriosinstitucioneducativa ON institucioneseducativas.IeId = territoriosinstitucioneducativa.ToInstitucionEducativa
INNER JOIN territorios ON territoriosinstitucioneducativa.ToTerritorio = territorios.TsId
INNER JOIN tipossector ON institucioneseducativas.IeSector = tipossector.TcId
INNER JOIN temporadasnegocioperiodocalendariopais ON gradosnivelperiodoinstitucioneducativa.GiPeriodoCalendarioPais = temporadasnegocioperiodocalendariopais.TnPeriodoCalendarioPais
WHERE
	temporadasnegocioperiodocalendariopais.TnId = 8
	and gradosnivelpais.GnId = ".$grado."
";
        return DbHandler_2::GetAll($sql);
    }
    
    function colegio_bynombre($nombre){
        $sql="SELECT
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	institucioneseducativas.IeNombreSede,
	tiposidentificacion.TpNombre,
	institucioneseducativas.IeIdentificacion,
	ciudades.CiNombre,
	departamentos.DeNombre
FROM
	institucioneseducativas
INNER JOIN tiposidentificacion ON institucioneseducativas.IeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN barrios ON institucioneseducativas.IeBarrio = barrios.BaId
INNER JOIN localidades ON barrios.BaLocalidad = localidades.LoId
INNER JOIN ciudades ON localidades.LoCiudad = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
WHERE institucioneseducativas.IeNombre = '".$nombre."'
ORDER BY
	institucioneseducativas.IeNombre";
        return DbHandler_2::GetAll($sql);
    }
    
    function ciudadbynombre($ciudad){
        $sql="select CiId,CiNombre,CiDepartamento from ciudades where CiNombre = '".$ciudad."' order by CiNombre";
	return DbHandler_2::GetAll($sql);
    }
    
    function detalle_grados($grados){
        $sql="SELECT DISTINCT
	institucioneseducativas.IeId AS ID,
	institucioneseducativas.IeIdentificacion AS Dane,
	institucioneseducativas.IeCodSAP AS Sap,
	institucioneseducativas.IeNombre AS Colegio,
	jornadas.JeNombre AS Jornada,
	gradosnivelpais.GnNombre AS Grado,
	gradosnivelpais.GnId,        
	paralelos.PgNombre AS Paralelo,
	paralelos.PgNumeroAlumnos AS Poblacion,
	tipossector.TcNombre,
	temporadasnegocioperiodocalendariopais.TnNombre	
FROM
	gradosjornadainstitucioneducativa
INNER JOIN gradosnivelperiodoinstitucioneducativa ON gradosjornadainstitucioneducativa.GjGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId
INNER JOIN jornadas ON gradosjornadainstitucioneducativa.GjJornada = jornadas.JeId
INNER JOIN gradosnivelpais ON gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId
INNER JOIN institucioneseducativas ON gradosnivelperiodoinstitucioneducativa.GiInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN paralelos ON gradosnivelperiodoinstitucioneducativa.GiId = paralelos.PgGradoNivelPeriodoInstitucionEducativa
INNER JOIN territoriosinstitucioneducativa ON institucioneseducativas.IeId = territoriosinstitucioneducativa.ToInstitucionEducativa
INNER JOIN territorios ON territoriosinstitucioneducativa.ToTerritorio = territorios.TsId
INNER JOIN tipossector ON institucioneseducativas.IeSector = tipossector.TcId
INNER JOIN temporadasnegocioperiodocalendariopais ON gradosnivelperiodoinstitucioneducativa.GiPeriodoCalendarioPais = temporadasnegocioperiodocalendariopais.TnPeriodoCalendarioPais
WHERE
	temporadasnegocioperiodocalendariopais.TnId = 8
	and gradosnivelpais.GnId IN (".$grados.")
ORDER BY
	Colegio ASC,
	Jornada ASC,
	Grado ASC,
	Paralelo ASC,
	Paralelo ASC";
        return DbHandler_2::GetAll($sql);
    }
	
	function lista_grados($id_institucion){
		$sql="SELECT DISTINCT
	institucioneseducativas.IeId AS ID,
	institucioneseducativas.IeIdentificacion AS Dane,
	institucioneseducativas.IeCodSAP AS Sap,
	institucioneseducativas.IeNombre AS Colegio,
	jornadas.JeNombre AS Jornada,
	gradosnivelpais.GnNombre AS Grado,
	gradosnivelpais.GnId,        
	paralelos.PgNombre AS Paralelo,
	paralelos.PgNumeroAlumnos AS Poblacion,
	tipossector.TcNombre,
	temporadasnegocioperiodocalendariopais.TnNombre	
FROM
	gradosjornadainstitucioneducativa
INNER JOIN gradosnivelperiodoinstitucioneducativa ON gradosjornadainstitucioneducativa.GjGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId
INNER JOIN jornadas ON gradosjornadainstitucioneducativa.GjJornada = jornadas.JeId
INNER JOIN gradosnivelpais ON gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId
INNER JOIN institucioneseducativas ON gradosnivelperiodoinstitucioneducativa.GiInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN paralelos ON gradosnivelperiodoinstitucioneducativa.GiId = paralelos.PgGradoNivelPeriodoInstitucionEducativa
INNER JOIN territoriosinstitucioneducativa ON institucioneseducativas.IeId = territoriosinstitucioneducativa.ToInstitucionEducativa
INNER JOIN territorios ON territoriosinstitucioneducativa.ToTerritorio = territorios.TsId
INNER JOIN tipossector ON institucioneseducativas.IeSector = tipossector.TcId
INNER JOIN temporadasnegocioperiodocalendariopais ON gradosnivelperiodoinstitucioneducativa.GiPeriodoCalendarioPais = temporadasnegocioperiodocalendariopais.TnPeriodoCalendarioPais
WHERE
	temporadasnegocioperiodocalendariopais.TnId = 8
	and institucioneseducativas.IeId = ".$id_institucion."
ORDER BY
	Colegio ASC,
	Jornada ASC,
	Grado ASC,
	Paralelo ASC,
	Paralelo ASC";
        return DbHandler_2::GetAll($sql);
	}
	
    function detalle_area($id){
        $sql="SELECT
	areas.AeId,
	areas.AeNombre
FROM
	areas
WHERE  areas.AeId = ".$id."       
";
        return DbHandler_2::GetAll($sql);
    }    
        
    function lista_areas_conocimiento(){
        $sql="SELECT
	areas.AeId,
	areas.AeNombre
FROM
	areas";
        return DbHandler_2::GetAll($sql);
    }
    function datos_docente($id){   
        $sql="SELECT
	contactosinstitucioneducativa.CnId,
	personas.PeNombres,
	personas.PePrimerApellido,
	personas.PeSegundoApellido,
	tiposidentificacion.TpNombre,
	personas.PeIdentificacion,
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	rolescontacto.RcNombre
FROM
	contactosinstitucioneducativa
INNER JOIN personas ON contactosinstitucioneducativa.CnPersona = personas.PeId
INNER JOIN tiposidentificacion ON personas.PeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN institucioneseducativas ON contactosinstitucioneducativa.CnInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN rolescontacto ON contactosinstitucioneducativa.CnRolContacto = rolescontacto.RcId
WHERE
	contactosinstitucioneducativa.CnTemporada = 8";
        return DbHandler_2::GetAll($sql);
    }
    function valida_usuario($cedula){
        $sql="SELECT
	contactosinstitucioneducativa.CnId,
	personas.PeNombres,
	personas.PePrimerApellido,
	personas.PeSegundoApellido,
	tiposidentificacion.TpNombre,
	personas.PeIdentificacion,
	institucioneseducativas.IeId,
	institucioneseducativas.IeNombre,
	rolescontacto.RcNombre,
	departamentos.DeId,
	PeCiudadResidencia
	
FROM
	contactosinstitucioneducativa
INNER JOIN personas ON contactosinstitucioneducativa.CnPersona = personas.PeId
INNER JOIN tiposidentificacion ON personas.PeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN institucioneseducativas ON contactosinstitucioneducativa.CnInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN rolescontacto ON contactosinstitucioneducativa.CnRolContacto = rolescontacto.RcId
INNER JOIN ciudades ON personas.PeCiudadResidencia = ciudades.CiId
INNER JOIN departamentos ON ciudades.CiDepartamento = departamentos.DeId
WHERE
	contactosinstitucioneducativa.CnTemporada = 8
	AND PeIdentificacion = '".$cedula."'";
	return DbHandler_2::GetAll($sql);
    }
	
	function varios_usuarios(){
        $sql="SELECT
	*
FROM
	contactosinstitucioneducativa
INNER JOIN personas ON contactosinstitucioneducativa.CnPersona = personas.PeId
INNER JOIN tiposidentificacion ON personas.PeTipoIdentificacion = tiposidentificacion.TpId
INNER JOIN institucioneseducativas ON contactosinstitucioneducativa.CnInstitucionEducativa = institucioneseducativas.IeId
INNER JOIN rolescontacto ON contactosinstitucioneducativa.CnRolContacto = rolescontacto.RcId
WHERE
	contactosinstitucioneducativa.CnTemporada = 8
	LIMIT 0,20
	";
	return DbHandler_2::GetAll($sql);
    }
	
    function lista_docentes(){
        $sql = "SELECT

personas.PeIdentificacion AS `user`,

ucase(

                               concat_ws(

                                               ' ',

                                               `personas`.`PePrimerApellido`,

                                               `personas`.`PeSegundoApellido`,

                                               `personas`.`PeNombres`

                               )

                ) AS Contacto,

CONCAT(

                               `personas`.`PePrimerApellido`,

                               ' ',

                               `personas`.`PeSegundoApellido`

                ) AS apellidos,

personas.PeNombres AS nombres,

adopcionproductos.AoProducto AS course,

productos.PcNombre AS fullname,

rolescontacto.RcId AS role,

rolescontacto.RcNombre AS Nombre_Rol,

niveleseducativospais.NvId AS category1,

niveleseducativospais.NvNombre AS Nombrecategory1,

gradosnivelpais.GnId AS category2,

gradosnivelpais.GnNombre AS Nombrecategory2,

paralelos.PgNombre AS category3,

adopciones.ApTemporadaNegocio AS ApTemporadaNegocio,

agrupaciones.AcNombre AS Serie,

adopciones.ApInstitucionEducativa,

asignaturas.AsNombre,

areas.AeNombre AS Area

FROM

(((((((((((contactosadopcion

JOIN adopciones ON ((contactosadopcion.CpAdopcion = adopciones.ApId)))

JOIN contactosinstitucioneducativa ON ((contactosadopcion.CpContactoInstitucionEducativa = contactosinstitucioneducativa.CnId)))

JOIN personas ON ((contactosinstitucioneducativa.CnPersona = personas.PeId)))

JOIN adopcionproductos ON ((adopcionproductos.AoAdopcion = adopciones.ApId)))

JOIN rolescontacto ON ((contactosinstitucioneducativa.CnRolContacto = rolescontacto.RcId)))

JOIN productos ON ((adopcionproductos.AoProducto = productos.PcId)))

JOIN asignaturasparalelo ON ((adopciones.ApAsignaturaParalelo = asignaturasparalelo.AuId)))

JOIN paralelos ON ((asignaturasparalelo.AuParalelo = paralelos.PgId)))

JOIN gradosnivelperiodoinstitucioneducativa ON ((paralelos.PgGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId)))

JOIN gradosnivelpais ON ((gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId)))

JOIN niveleseducativospais ON ((gradosnivelpais.GnNivelEducativo = niveleseducativospais.NvId)))

INNER JOIN agrupaciones ON productos.PcAgrupacion = agrupaciones.AcId

INNER JOIN asignaturasgradosperiodoinstitucioneducativa ON asignaturasparalelo.AuAsignaturaGradoPeriodoInstitucionEducativa = asignaturasgradosperiodoinstitucioneducativa.AdId

INNER JOIN asignaturasgradopaisdefecto ON asignaturasgradosperiodoinstitucioneducativa.AdAsignaturaGradoPais = asignaturasgradopaisdefecto.AgId

INNER JOIN asignaturas ON asignaturasgradopaisdefecto.AgAsignatura = asignaturas.AsId

INNER JOIN areas ON asignaturas.AsArea = areas.AeId

WHERE

                (

                               adopciones.ApTemporadaNegocio IN (8)

                )

AND agrupaciones.AcId = 2891";
        return DbHandler_2::GetAll($sql);
        
    }
    function lista_libros($docente){
        $sql = "select * from productos where PcPathPortada is null LIMIT 0,10";
        return DbHandler_2::GetAll($sql);
    }
	
	function lista_deptos(){
		$sql="select DeId,DeNombre from departamentos order by DeNombre";
		return DbHandler_2::GetAll($sql);
	}
	
	function lista_ciudades($depto){
		$sql="select CiId,CiNombre from ciudades where CiDepartamento = ".$depto." order by CiNombre";
		return DbHandler_2::GetAll($sql);
	}
        function detalles_ciudad($ciudad){
                $sql="select CiId,CiNombre,CiDepartamento from ciudades where CiId = ".$ciudad." order by CiNombre";
		return DbHandler_2::GetAll($sql);
        }
        function detalles_depto($depto){
                $sql="select DeId,DeNombre from departamentos WHERE DeId = ".$depto." order by DeNombre";
		return DbHandler_2::GetAll($sql);
        }
        
        function productosasignaturasgrado($grado){
            if ($grado != '0'){
            $comple = "gradosnivelpais.GnNombre = ".$grado." AND";
            }
            
            $sql="SELECT

            productos.PcId AS IdTitulo,

            productos.PcNombre AS Titulo,

            agrupaciones.AcId AS IdSerie,

            agrupaciones.AcNombre AS Serie,

            casaseditoriales.CeNombre AS Sello,

            areas.AeNombre AS Area,

            asignaturas.AsNombre AS Asignatura,

            gradosnivelpais.GnNombre AS Grado,

            temporadasnegocioperiodocalendariopais.TnNombre,

            empresas.EmNombre

            FROM

            productosasignaturasgrado

            INNER JOIN productos ON productosasignaturasgrado.PsProducto = productos.PcId

            INNER JOIN asignaturasgradopaisdefecto ON productosasignaturasgrado.PsAsignaturaGrado = asignaturasgradopaisdefecto.AgId

            INNER JOIN gradosnivelpais ON asignaturasgradopaisdefecto.AgGradoNivel = gradosnivelpais.GnId

            INNER JOIN asignaturas ON asignaturasgradopaisdefecto.AgAsignatura = asignaturas.AsId

            INNER JOIN agrupaciones ON productos.PcAgrupacion = agrupaciones.AcId

            INNER JOIN casaseditoriales ON agrupaciones.AcCasaEditorial = casaseditoriales.CeId

            INNER JOIN comercializacion ON productos.PcId = comercializacion.ComProducto

            INNER JOIN areas ON asignaturas.AsArea = areas.AeId

            INNER JOIN temporadasnegocioperiodocalendariopais ON comercializacion.ComTemporada = temporadasnegocioperiodocalendariopais.TnId

            INNER JOIN empresas ON casaseditoriales.CeEmpresas = empresas.EmId

            WHERE
            
            ".$comple."

            comercializacion.ComTemporada = 8 AND

            agrupaciones.AcId IN (2891)

            ORDER BY

            productos.PcNombre ASC,

            Sello ASC,

            Grado ASC";
            return DbHandler_2::GetAll($sql);
        }
        
        
        function getemailbycedula($cedula){
            $sql="SELECT

                personas.PeId AS IdContacto,

                personas.PeIdentificacion AS Cedula,

                informacioncomplementariapersonas.IcEmail AS Correo1,

                informacioncomplementariapersonas.IcEMailDos AS Correo2

                FROM

                personas

                INNER JOIN informacioncomplementariapersonas ON informacioncomplementariapersonas.IcPersona = personas.PeId
                
                WHERE personas.PeIdentificacion = '".$cedula."' LIMIT 0,1";
            
            return DbHandler_2::GetAll($sql);
            
        }
        
        function lista_insti_conecta(){
            $sql = "SELECT
DISTINCT institucioneseducativas.IeNombre,institucioneseducativas.IeId
FROM

(((((((((((contactosadopcion

JOIN adopciones ON ((contactosadopcion.CpAdopcion = adopciones.ApId)))

JOIN contactosinstitucioneducativa ON ((contactosadopcion.CpContactoInstitucionEducativa = contactosinstitucioneducativa.CnId)))

JOIN personas ON ((contactosinstitucioneducativa.CnPersona = personas.PeId)))

JOIN adopcionproductos ON ((adopcionproductos.AoAdopcion = adopciones.ApId)))

JOIN rolescontacto ON ((contactosinstitucioneducativa.CnRolContacto = rolescontacto.RcId)))

JOIN productos ON ((adopcionproductos.AoProducto = productos.PcId)))

JOIN asignaturasparalelo ON ((adopciones.ApAsignaturaParalelo = asignaturasparalelo.AuId)))

JOIN paralelos ON ((asignaturasparalelo.AuParalelo = paralelos.PgId)))

JOIN gradosnivelperiodoinstitucioneducativa ON ((paralelos.PgGradoNivelPeriodoInstitucionEducativa = gradosnivelperiodoinstitucioneducativa.GiId)))

JOIN gradosnivelpais ON ((gradosnivelperiodoinstitucioneducativa.GiGradoNivelPais = gradosnivelpais.GnId)))

JOIN niveleseducativospais ON ((gradosnivelpais.GnNivelEducativo = niveleseducativospais.NvId)))

INNER JOIN agrupaciones ON productos.PcAgrupacion = agrupaciones.AcId

inner join institucioneseducativas ON institucioneseducativas.IeId = adopciones.ApInstitucionEducativa

WHERE

(adopciones.ApTemporadaNegocio IN (8)) AND

agrupaciones.AcId = 2891";
        return DbHandler_2::GetAll($sql);
        }
        
}


?>
