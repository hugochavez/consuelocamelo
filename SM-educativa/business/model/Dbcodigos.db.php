<?php class Dbcodigos extends DbDAO{
  public $id;
  protected $colegio;
  protected $curso;
  protected $area;
  protected $codigo;
  public function setid($id){
    $this->id = $id;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setcurso($curso){
    $this->curso = $curso;
  }
  public function setarea($area){
    $this->area = $area;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
}
