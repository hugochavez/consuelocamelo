<?php class Dbcompradetalle extends DbDAO{
  public $id;
  protected $compra;
  protected $paquete;
  protected $valor;
  public function setid($id){
    $this->id = $id;
  }
  public function setcompra($compra){
    $this->compra = $compra;
  }
  public function setpaquete($paquete){
    $this->paquete = $paquete;
  }
  public function setvalor($valor){
    $this->valor = $valor;
  }
}
