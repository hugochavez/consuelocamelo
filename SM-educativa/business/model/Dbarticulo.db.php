<?php class Dbarticulo extends DbDAO{
  public $id;
  protected $codigo;
  protected $tipo;
  protected $nombre;
  protected $sociedad;
  protected $organizacion;
  protected $centrodistribucion;
  protected $sector;
  protected $centroalmacen;
  protected $tipopedido;
  public function setid($id){
    $this->id = $id;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setsociedad($sociedad){
    $this->sociedad = $sociedad;
  }
  public function setorganizacion($organizacion){
    $this->organizacion = $organizacion;
  }
  public function setcentrodistribucion($centrodistribucion){
    $this->centrodistribucion = $centrodistribucion;
  }
  public function setsector($sector){
    $this->sector = $sector;
  }
  public function setcentroalmacen($centroalmacen){
    $this->centroalmacen = $centroalmacen;
  }
  public function settipopedido($tipopedido){
    $this->tipopedido = $tipopedido;
  }
}
