<?php class Dblicencia extends DbDAO{
  public $id;
  protected $usuario;
  protected $codigo;
  protected $estado;
  protected $codlibro;
  public function setid($id){
    $this->id = $id;
  }
  public function setusuario($usuario){
    $this->usuario = $usuario;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function setestado($estado){
    $this->estado = $estado;
  }
  public function setcodlibro($codlibro){
    $this->codlibro = $codlibro;
  }
}
