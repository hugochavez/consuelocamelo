<?php class Dbcompra extends DbDAO{
  public $id;
  protected $alumno;
  protected $fecha;
  protected $estado;
  protected $tipo;
  public function setid($id){
    $this->id = $id;
  }
  public function setalumno($alumno){
    $this->alumno = $alumno;
  }
  public function setfecha($fecha){
    $this->fecha = $fecha;
  }
  public function setestado($estado){
    $this->estado = $estado;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
}
