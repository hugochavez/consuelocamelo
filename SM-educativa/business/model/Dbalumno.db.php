<?php class Dbalumno extends DbDAO{
  public $id;
  protected $colegio;
  protected $grado;
  protected $codigo;
  protected $nombres;
  protected $apellidos;
  protected $correo;
  protected $consecutivo;
  protected $ciudad;
  public function setid($id){
    $this->id = $id;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setgrado($grado){
    $this->grado = $grado;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function setnombres($nombres){
    $this->nombres = $nombres;
  }
  public function setapellidos($apellidos){
    $this->apellidos = $apellidos;
  }
  public function setcorreo($correo){
    $this->correo = $correo;
  }
  public function setconsecutivo($consecutivo){
    $this->consecutivo = $consecutivo;
  }
  public function setciudad($ciudad){
    $this->ciudad = $ciudad;
  }
}
