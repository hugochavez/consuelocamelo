<?php class Dbtemplate extends DbDAO{
  public $id;
  protected $diccionario;
  protected $colegio;
  protected $area;
  protected $paralelo;
  protected $codigo;
  protected $nombre;
  public function setid($id){
    $this->id = $id;
  }
  public function setdiccionario($diccionario){
    $this->diccionario = $diccionario;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setarea($area){
    $this->area = $area;
  }
  public function setparalelo($paralelo){
    $this->paralelo = $paralelo;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
}

