<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of metodos_ws
 *
 * @author znes
 */
class Dbmetodos_ws {
    
    public $encabezado;
    
    public function __construct() {
        $this->encabezado = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.grupo-sm.com/">
   <soapenv:Header>
      <ser:WSEAuthenticateHeader>
         <ser:User>'.USER_WSDL.'</ser:User>
         <ser:Password>'.PASS_WSDL.'</ser:Password>
      </ser:WSEAuthenticateHeader>
   </soapenv:Header>';
    }
    
    public function crear_licencias($licencias,$persona){
        $lic = "<![CDATA[<root>";
        for($a = 0;$a < count($licencias);$a++){
            $lic.="<licencia>".$licencias[$a]."</licencia>";
        }
        $lic.="</root>]]>";
        
        $xml=$this->encabezado.'<soapenv:Body>
      <ser:AsociacionLicencias>
         <ser:IdentificadorUnico>'.$persona['documento'].'</ser:IdentificadorUnico>
        <ser:Password>'.$persona['documento'].'</ser:Password>
        <ser:Nombre>'.$persona['nombre'].'</ser:Nombre>
        <ser:Apellidos>'.$persona['apellido'].'</ser:Apellidos>
        <ser:Email>'.$persona['email'].'</ser:Email>
        <ser:sxmlLicencias>'.$lic.'</ser:sxmlLicencias>
         <ser:idRol>'.$persona['rol'].'</ser:idRol>
      </ser:AsociacionLicencias>
   </soapenv:Body>
</soapenv:Envelope>';
        
        $client = new nusoap_client(URL_WSDL, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/AsociacionLicencias";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
    }
    
    public function confirmar_licencias($licencias){
        $lic = "<![CDATA[<root>";
        for($a = 0;$a < count($licencias);$a++){
            $lic.="<licencia>".$licencias[$a]."</licencia>";
        }
        $lic.="</root>]]>";
        
        $xml=$this->encabezado.'<soapenv:Body>
      <ser:ComprobacionLicencias>
         <ser:sxmlLicencias>'.$lic.'</ser:sxmlLicencias>
      </ser:ComprobacionLicencias>
   </soapenv:Body>
</soapenv:Envelope>';
        
        $client = new nusoap_client(URL_WSDL, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/ComprobacionLicencias";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
    }
    
    public function mislibros($usuario,$contrasena){
        
        $xml = $this->encabezado.'<soapenv:Body>
                <ser:obtenerMisLibros>
                      <ser:IdentificadorUnico>'.$usuario.'</ser:IdentificadorUnico>
                      <ser:password>'.$contrasena.'</ser:password>
                   </ser:obtenerMisLibros>
                </soapenv:Body>
             </soapenv:Envelope>';
        //echo $xml;
        
        $client = new nusoap_client(URL_WSDL, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/obtenerMisLibros";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
        
    }
    
    public function listalibros(){
        
        $xml = $this->encabezado.'<soapenv:Body>
      <ser:ObtenerTodos>
         <!--Optional:-->
         <ser:IdCentro>?</ser:IdCentro>
      </ser:ObtenerTodos>
   </soapenv:Body>
</soapenv:Envelope>';
        //echo $xml;
        
        $client = new nusoap_client(URL_WSDL_2, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/ObtenerTodos";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
        
    }
    
    public function datos_licencia($codigo){
        $xml = $this->encabezado.'<soapenv:Body>
      <ser:ObtenerDatosLicencia>
         <ser:Credencial>'.$codigo.'</ser:Credencial>
      </ser:ObtenerDatosLicencia>
   </soapenv:Body>
</soapenv:Envelope>';
      $client = new nusoap_client(URL_WSDL_2, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/ObtenerDatosLicencia";
        $client->send($xml, $soapaction);
        return $datos = $client->response;  
    }
    
    public function datos_libros($isbn){
        
        $xml = $this->encabezado.'<soapenv:Body>
                            <ser:ObtenerFichaLibro>
                               <ser:ISBN>'.$isbn.'</ser:ISBN>                               
                            <ser:OpcionesFicha>
                               <ser:IncContGenAlumno>true</ser:IncContGenAlumno>
                               <ser:IncContGenProfesor>true</ser:IncContGenProfesor>
                               <ser:IncRecPlatAlumno>true</ser:IncRecPlatAlumno>
                               <ser:IncRecPlatProfesor>true</ser:IncRecPlatProfesor>
                               <ser:IncTblContAlumno>true</ser:IncTblContAlumno>
                               <ser:IncTblContProfesor>true</ser:IncTblContProfesor>
                               <ser:IncPictosSMAlumno>true</ser:IncPictosSMAlumno>
                               <ser:IncPictosSMProfesor>true</ser:IncPictosSMProfesor>
                               <ser:IncPictosProfesorAlumno>true</ser:IncPictosProfesorAlumno>
                               <ser:IncPictosProfesorProfesor>true</ser:IncPictosProfesorProfesor>
                            </ser:OpcionesFicha>    
                            </ser:ObtenerFichaLibro>
                         </soapenv:Body>
                      </soapenv:Envelope>';
        
        $client = new nusoap_client(URL_WSDL_2, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/ObtenerFichaLibro";
        $client->send($xml, $soapaction);
        return $datos = $client->response;        
    }
    
    public function obtenerpedidolicencias($email,$isbn,$periodovigencia,$num_licencias,$idcentrolms = 16435){
        //echo "PRIMERO";
        $xml=$this->encabezado.'<soapenv:Body>

      <ser:ObtenerPedidoLicencias>

         <ser:Lista><![CDATA[<libros>

                                               <libro>

                                                               <ISBN>'.$isbn.'</ISBN>

                                                               <periodolicencia>1</periodolicencia>
                                                 
                                                               <idperiodovigencia>'.$periodovigencia.'</idperiodovigencia>                                       

                                                               <licencias tipolicencia= "1" numerolicencias="'.$num_licencias.'"></licencias>

                                                               <licencias tipolicencia="2" numerolicencias="1"></licencias>

                                               </libro>
                               </libros>]]>

                </ser:Lista>

        <ser:IdCentroLMS>'.$idcentrolms.'</ser:IdCentroLMS>

        <ser:NombreLista>Colombia ISBN '.$isbn.' 1 PRO '.$num_licencias.' ALU</ser:NombreLista>

        <!--Optional:-->

         <ser:Correo>'.$email.'</ser:Correo>

      </ser:ObtenerPedidoLicencias>

   </soapenv:Body>

</soapenv:Envelope>';
        //echo $xml;
        $client = new nusoap_client(URL_WSDL_3, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/ObtenerPedidoLicencias";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
    }
    
    public function autentica_user($licencia,$cedula,$isbn,$rec = 0){
        
        $comple = "";
        if ($rec > 0){
            $comple = "<ser:IdActividad>".$rec."</ser:IdActividad>";
        }
        
        $xml=$this->encabezado.'<soapenv:Body>
      <ser:AutenticarUsuarioContenido>
         <ser:Credencial>'.$licencia.'</ser:Credencial>
         <ser:ISBN>'.$isbn.'</ser:ISBN>
         <!--Optional:-->
         <ser:IdUsuario>'.$cedula.'</ser:IdUsuario>
         '.$comple.'
      </ser:AutenticarUsuarioContenido>
   </soapenv:Body>
</soapenv:Envelope>';
        $client = new nusoap_client(URL_WSDL, 'wsdl',"", "", "", "");
        $err = $client->getError();
        if ($err) {
	return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }else{
        //echo "otro";
        }
        $client->setUseCURL(true);
        $soapaction = "http://services.grupo-sm.com/AutenticarUsuarioContenido";
        $client->send($xml, $soapaction);
        return $datos = $client->response;
    }
    
    //put your code here
}

?>
