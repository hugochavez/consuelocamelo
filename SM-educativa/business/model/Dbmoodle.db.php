<?php class Dbmoodle extends DbDAO{
  public $id;
  protected $fecha;
  protected $user;
  protected $template;
  protected $course;
  protected $role;
  protected $fullname;
  protected $category1;
  protected $category2;
  protected $category3;
  public function setid($id){
    $this->id = $id;
  }
  public function setfecha($fecha){
    $this->fecha = $fecha;
  }
  public function setuser($user){
    $this->user = $user;
  }
  public function settemplate($template){
    $this->template = $template;
  }
  public function setcourse($course){
    $this->course = $course;
  }
  public function setrole($role){
    $this->role = $role;
  }
  public function setfullname($fullname){
    $this->fullname = $fullname;
  }
  public function setcategory1($category1){
    $this->category1 = $category1;
  }
  public function setcategory2($category2){
    $this->category2 = $category2;
  }
  public function setcategory3($category3){
    $this->category3 = $category3;
  }
}
