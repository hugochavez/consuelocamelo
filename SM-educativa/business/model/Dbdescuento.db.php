<?php class Dbdescuento extends DbDAO{
  public $id;
  protected $colegio;
  protected $paquete;
  protected $detalle;
  protected $valor;
  public function setid($id){
    $this->id = $id;
  }
  public function setcolegio($colegio){
    $this->colegio = $colegio;
  }
  public function setpaquete($paquete){
    $this->paquete = $paquete;
  }
  public function setdetalle($detalle){
    $this->detalle = $detalle;
  }
  public function setvalor($valor){
    $this->valor = $valor;
  }
}
