<?php 
$cpersona = new Dbpersona();
$datos['tipo']=4;
$datos['campos']=",us.email ";
$datos['join']=" INNER JOIN usuario us on us.id = a.usuario";
$datos['where']=" order by a.apellido, a.nombre";
$alumnos = $cpersona->getList($datos);
$cconsultas = new Dbconsultas_brujula();
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  $(function() { 
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Código</span></th>
                                              <th><span class="th_wrapp">Apellidos</span></th>
                                              <th><span class="th_wrapp">Nombres</span></th>
                                              <th><span class="th_wrapp">Teléfono</span></th>
                                              <th><span class="th_wrapp">Departamento</span></th>
                                              <th><span class="th_wrapp">Ciudad</span></th>
                                              <th><span class="th_wrapp">Instituci&oacute;n</span></th>
                                              <th><span class="th_wrapp">Grado</span></th>
                                              <th><span class="th_wrapp">Correo</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($alumnos as $item) {
                            if ($item['grado'] == ""){
                                $item['grado'] =0;
                            }
                            
                            if ($item['ciudad'] == ""){
                                $item['ciudad'] =0;
                            }
                            if ($item['institucion'] == ""){
                                $item['institucion'] =0;
                            }
                            $datos_ciuadad = $cconsultas->detalles_ciudad($item['ciudad']);
                            if (!isset($datos_ciuadad[0]['CiDepartamento'])){
                                $item['CiDepartamento'] =0;
                            }
                            $datos_depto = $cconsultas->detalles_depto($datos_ciuadad[0]['CiDepartamento']);
                            $datos_grado = $cconsultas->detalle_grado($item['grado']);
                            $datos_institucion = $cconsultas->detalle_colegio($item['institucion']);
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                      <?php echo $item['documento']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['apellido']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['nombre'] ?>
                  </td>
                  <td><?= $item["telefono"] ?></td>
                  <td><?= utf8_encode($datos_depto[0]['DeNombre']) ?></td>
                  <td><?= utf8_encode($datos_ciuadad[0]['CiNombre']) ?></td>
                  <td><?= utf8_encode($datos_institucion[0]['IeNombre']) ?></td>
                  <td><?= utf8_encode($datos_grado[0]['Grado']." - ".$datos_grado[0]['Paralelo']) ?></td>
                  <td><?= $item["email"] ?></td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>