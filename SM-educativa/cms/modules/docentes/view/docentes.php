<?php 
$cpersona = new Dbpersona();
$datos['tipo']=3;
$datos['campos']="a.id";
$datos['join']=" INNER JOIN usuario us on us.id = a.usuario ";
$pagina = 1;
if (isset($_GET['pagina'])){
    $pagina = $_GET['pagina'];
}
$cedula="";
if (isset($_POST['cedula'])){
    $cedula = $_POST['cedula'];
}
if($cedula != ""){
$pagina=1;
$datos['documento'] = $cedula;
}
$num_alumnos = $cpersona->getCount($datos);



$datos_pp=20;
$datos['where']=" order by a.apellido, a.nombre";

$datos['campos']=",us.email ";
$inicio = ($pagina-1)*$datos_pp;
$datos['where'].=" LIMIT $inicio,$datos_pp";
$alumnos = $cpersona->getList($datos);
$cconsultas = new Dbconsultas_brujula();
$cgradousuario = new Dbgradousuario();
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  
  
</script>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
           <div class="span5 pull-right tar">
		<label>Buscar por cédula: 
		<form method="post">
		<input type="text" aria-controls="example" id="cedula" name="cedula">
		<input type="submit" value="Buscar" />
		</form>
		</label>
	</div>
		   <table class="display">
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Código</span></th>
                                              <th><span class="th_wrapp">Apellidos</span></th>
                                              <th><span class="th_wrapp">Nombres</span></th>
                                              <th><span class="th_wrapp">Teléfono</span></th>
                                              <th><span class="th_wrapp">Departamento</span></th>
                                              <th><span class="th_wrapp">Ciudad</span></th>
                                              <th><span class="th_wrapp">Instituci&oacute;n</span></th>
                                              <th><span class="th_wrapp">Grado</span></th>
                                              <th><span class="th_wrapp">Correo</span></th>
											  <th><span class="th_wrapp">Libros</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($alumnos as $item) {
                            $dat_grado['usuario'] = $item['usuario'];
                            $grados = $cgradousuario->getList($dat_grado);
                            $txt_grados = "";
                            foreach ($grados as $item_grad){
                                $txt_grados =",".$item_grad['grado'];
                            }
                            $txt_grados=substr($txt_grados,1);
                            if ($item['ciudad'] > 0){
                            $datos_ciuadad = $cconsultas->detalles_ciudad($item['ciudad']);
                            $datos_depto = $cconsultas->detalles_depto($datos_ciuadad[0]['CiDepartamento']);
                            }
                            else{
                            $datos_ciuadad[0] = array("CiNombre"=>"No Registra");
                            $datos_depto[0] = array("DeNombre"=>"No Registra");
                            }
                            if($txt_grados != ""){
                            $datos_grados = $cconsultas->detalle_grados($txt_grados);
                            $te_grados="";
                            foreach ($datos_grados as $dat_grado){
                                $te_grados=",".$dat_grado['Grado']." - ".$dat_grado['Paralelo'];
                            }
                            }else{
                                $te_grados="";
                            }
                            if ($item['institucion'] > 0){
                            $datos_institucion = $cconsultas->detalle_colegio($item['institucion']);
                            }else{
                            $datos_institucion[0] = array("IeNombre"=>"No Registra");    
                            }
                            
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                      <?php echo $item['documento']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo utf8_encode($item['apellido'])?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo utf8_encode($item['nombre']) ?>
                  </td>
                  <td><?= $item["telefono"] ?></td>
                  <td><?= utf8_encode($datos_depto[0]['DeNombre']) ?></td>
                  <td><?= utf8_encode($datos_ciuadad[0]['CiNombre']) ?></td>
                  <td><?= utf8_encode($datos_institucion[0]['IeNombre']) ?></td>
                  <td><?= utf8_encode(substr($te_grados,1)) ?></td>
                  <td><?= $item["email"] ?></td>
				  <td><a href="../../../../index.php?seccion=libros_nosession&id_usuario=<?php echo $item['id']?>" target="_blank">Ver Libros</a></td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
      <div class="clear"></div>
      <div>
          <?php $num_paginas = ceil($num_alumnos[0]['count(a.id)']/$datos_pp);
          for ($a=1;$a<=$num_paginas;$a++){
              if ($a == $pagina){
                  ?>
                   <b><?php echo $a?></b>
                  <?php
              }else{
              ?>
          <a href="index.php?seccion=docentes&pagina=<?php echo $a;?>"><?php echo $a?></a>
              <?php }
          }?>
      </div>
  </div>
    

  <!--Fin del Contenido del Modulo-->
</div>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>