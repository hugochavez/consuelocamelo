<?php 
$tipos_recurso[1]="Enlace";
$tipos_recurso[2]="Archivo";
$ccomplemtos = new Dbcomplementos_libro();

$tipos[1]="Lección digital";
$tipos[2]="Actividades evaluativas";
$tipos[3]="Clases Maestras";
$id_comp = $_GET['id_comp'];
if ($id_comp){
$datos_compl = $ccomplemtos->getByPk($id_comp);
}
else{
$datos_compl = array();    
}
$cnewmetodos = new Dbmetodos_ws();
$isbns = $cnewmetodos->listalibros();
$respuesta = explode("ObtenerTodosResult",$isbns);
$contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
$licencias = explode("libros",$contenido);
$cont = $licencias[1];
$cont = "<libros".$licencias[1]."libros>";
$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->loadXML($cont);
$libros = $DOM->getElementsByTagName('libro');

?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  function cambia_libro(texto){
      $.post("datos_libro.php", {isbn:texto}, function(msg_2){
            $("#existente").html(msg_2);
            });
  }
  
  function editar(libro,nombre){
      $("#div_libro").html(nombre+"<input type='hidden' id='libro' name='libro' value='"+libro+"'/>");
      cambia_libro(libro);
  }
  
  function cambia_tipo(tipo){
      if (tipo == '1'){
          $("#div_archivo").hide();
          $("#div_url").show();
      }else{
          $("#div_archivo").show();
          $("#div_url").hide();
      }
  }
  
  $(function() { 
  $("#div_archivo").hide();    
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<?php
if(isset($_GET["id_del"])){
  //if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$ccomplemtos->delete(" where libro = '".$_GET["id_del"]."'");       
  //}
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
        
  $id = $_POST["id"];
  if ($id == 0) {	
        $isbn = $_POST['isbn'];
        $tipo = $_POST['tipo'];
        $nombre = $_POST['nombre'];
        $tipo_recurso = $_POST['tipo_recurso'];
        if ($tipo == "1"){
        $ccomplemtos->setvalor($_POST['url']);    
        }else{
            $retorno = ClassFile::UploadFile("imagen", "../../../../complementos", "compl_".rand(0,10000), "compl_".rand(0,10000));
            if($retorno["Status"]=="Uploader"){
                    $ccomplemtos->setvalor($retorno["NameFile"]);	
            }else{

            }
        }
        $ccomplemtos->settipo_recurso($tipo_recurso);
        $ccomplemtos->setlibro($isbn);
        $ccomplemtos->settipo($tipo);
        $ccomplemtos->setnombre($nombre);        
        $ccomplemtos->save();
  } else {
        $isbn = $_POST['isbn'];
        $tipo = $_POST['tipo'];
        $nombre = $_POST['nombre'];
        $tipo_recurso = $_POST['tipo_recurso'];
        if ($tipo == "1"){
        $ccomplemtos->setvalor($_POST['url']);    
        }else{
            $retorno = ClassFile::UploadFile("imagen", "../../../../complementos", "compl_".rand(0,10000), "compl_".rand(0,10000));
            if($retorno["Status"]=="Uploader"){
                    $ccomplemtos->setvalor($retorno["NameFile"]);	
            }else{

            }
        }
        $ccomplemtos->setlibro($isbn);
        $ccomplemtos->settipo_recurso($tipo_recurso);
        $ccomplemtos->settipo($tipo);
        $ccomplemtos->setnombre($nombre); 
        $ccomplemtos->setid($id);
        $ccomplemtos->save();
  }
}


$datos['campos_esp'] = "DISTINCT a.libro ";
$datos['join']=" ";
$recursos = $ccomplemtos->getList($datos);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      COMPLEMENTOS
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando libros" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
              <label>Libro</label>
              <div style="width: 500px; margin-left: 200px; margin-top: -25px;" id="div_libro">
                <select name="isbn" id="isbn" onchange="cambia_libro($(this).val())" style="width: 500px" >
                    <option value="0">Seleccione una opción</option>
                    <?php  foreach ($libros as $libro){
                        $titulo = $libro->getElementsByTagName('titulo');
                        $isbn = $libro->getElementsByTagName('isbn');
                        $sel="";
                        if ($datos_compl['libro'] == $isbn->item(0)->nodeValue){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $isbn->item(0)->nodeValue; ?>" <?php echo $sel;?> ><?php echo $titulo->item(0)->nodeValue; ?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          <div id="existente"  style="margin-top: 36px;">
              
          </div>        
          
          <div style="margin-top: 36px;">
            <label>Tipo</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="tipo" id="tipo" >
                    <option value="0">Seleccione una opción</option>
                    <?php for ($a=1;$a <= count($tipos);$a++){
                        $sel="";
                        if ($datos_compl['tipo'] == $a){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $a; ?>" <?php echo $sel;?> ><?php echo $tipos[$a]?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          
          <div style="margin-top: 36px;">
            <label>Tipo Recurso</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="tipo_recurso" id="tipo_recurso" onchange="cambia_tipo($(this).val())" >
                    <option value="0">Seleccione una opción</option>
                    <?php for ($a=1;$a <= count($tipos_recurso);$a++){
                        $sel="";
                        if ($datos_compl['tipo_recurso'] == $a){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $a; ?>" <?php echo $sel;?> ><?php echo $tipos_recurso[$a]?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          
          <div style="margin-top: 36px;">
            <label>Nombre</label>
            <input type="text" name="nombre" style="width: 325px; margin-left: 152px; margin-top: -25px;" value="<?php echo $datos_compl['nombre']?>"/>
                            
           
          </div>
          <div style="margin-top: 36px;" id="div_url">
            <label>Url</label>
            <input type="text" name="url" style="width: 325px; margin-left: 180px; margin-top: -25px;"  value="<?php echo $datos_compl['valor']?>"/>
                            
            
          </div>
          <div style="margin-top: 36px;" id="div_archivo">
            <label>Archivo (<?php echo $datos_compl['valor']?>)</label>
            <input type="file" name="archivo" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>
                
            </div>
          </div>		  
            <input type="hidden" name="id" id="id" value="<?php echo $id_comp?>" />
          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=complementos&id=0">Agregar Nuevo Complemento</a>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Libro</span></th>                                      
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($recursos as $item) {
                            $datos_libro = $cnewmetodos->datos_libros($item['libro']);
                            $respuesta = explode("ObtenerFichaLibroResult",$datos_libro);
                            $contenido = substr($respuesta[1],1,strlen($respuesta[1])-3);
                            $licencias = explode("<Ficha",$contenido);
                            $licencias2 = explode("</Ficha>",$licencias[1]);
                            $cont = $licencias2[0];
                            $cont = "<Ficha".$licencias2[0]."</Ficha>";
                            $DOM = new DOMDocument('1.0', 'utf-8');
                            $DOM->loadXML($cont);
                            $libro = $DOM->getElementsByTagName('libro');
                            $ob_libro = $libro->item(0);
                            $titulo = $ob_libro->getElementsByTagName('titulo');
                            
					?>
                <tr class="odd gradeX">
                  
                  <td class="center" width="150px">
                    	<?php echo $titulo->item(0)->nodeValue;?>
                  </td>
                  <td class="center titulo" width="100px">
                    
                      <a class="uibutton icon edit" href="javascript:;" onclick="editar('<?php echo $item['libro']?>','<?php echo $titulo->item(0)->nodeValue;?>')">Editar</a>
                    <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=complementos&id_del=<?= $item["libro"] ?>&confirm=<?= base64_encode(md5($item["libro"])) ?>">Eliminar</a>
                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>


<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>