<?php 
$clicencias = new Dblicencias_cursos();
$lista_licencias = $clicencias->getList();
require '../Excel/reader.php';
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  $(function() { 
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$clicencias->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
//var_dump($_POST);
if (isset($_POST['excel'])){
    $retorno = ClassFile::UploadFile("excel_licencias", "licencias", "licencias_".date("Y-m-d"));
	if($retorno["Status"]=="Uploader"){
                //echo "subió";
                $reader = new Spreadsheet_Excel_Reader();
                $reader->read("licencias/".$retorno["NameFile"]);
		$celdas = $reader->sheets[0]['cells'];
                $i=2; 
                while($celdas[$i][1]!='') { 
                //echo "fila";    
                $colegio = $celdas[$i][1];
                $curso = $celdas[$i][2]; 
                $area = $celdas[$i][3];
                $licencia = $celdas[$i][4];
                $clicencias->setcolegio($colegio);
                $clicencias->setcurso($curso);
                $clicencias->setarea($area);
                $clicencias->setlicencia($licencia);
                $clicencias->save();
                $i++;                
                }
	}else{
		//echo "no subió";
	}
}else{


$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
        
  	$colegio = $_POST['id_colegio'];
        $grado = $_POST['grado'];
        $area = $_POST['area'];
        $licencias = $_POST['licencias'];
	$clicencias->setcolegio($colegio);
        $clicencias->setcurso($grado);
        $clicencias->setarea($area);
        $clicencias->setlicencia($licencias);
        $clicencias->save();
        //echo "grado=".$grado;
	//$id = $clicencias->getMaxId();
  } else {
        
  	$colegio = $_POST['id_colegio'];
        $grado = $_POST['grado'];
        $area = $_POST['area'];
        $licencias = $_POST['licencias'];
	$clicencias->setcolegio($colegio);
        $clicencias->setcurso($grado);
        $clicencias->setarea($area);
        $clicencias->setlicencia($licencias);
        $clicencias->setid($id);
	$clicencias->save();
  }
}
}

// Consultamos la img actual del banner
$datos_lic = $clicencias->getByPk($id);
$cconsultas = new Dbconsultas_brujula();
$areas = $cconsultas->lista_areas_conocimiento();

?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      
      <form name="masivo" id="masivo" method="post" enctype="multipart/form-data">
        <div style="margin-top: 36px;">
            <label>Excel Masivo</label>
            <div>
                <input type="file" name="excel_licencias"  style="width: 325px; margin-left: 200px; margin-top: -25px;" />
                <input type="hidden" name="excel" value="1"/>
                    
            </div>
            </div>
            
            <div><a id="submitForm" onclick="$('#masivo').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
            
        </form> 
      
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando libros" ?></h3>
        
        
           

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>Colegio</label>
            <div>
                <input type="text" name="licencias"  style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_lic["colegio"]; ?>" onfocus="cambia_colegio($(this).val())" />
            </div>
            </div>
          
          <div style="margin-top: 36px;">
            <label>Cursos</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;" id="sel_grados">
                
            </div>
          </div>
          
          <div style="margin-top: 36px;">
              <label>&Aacute;rea</label>
              <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="area" id="area" >
                    <option value="0">Seleccione una opción</option>
                    <?php foreach ($areas as $item){
                        $sel="";
                        if ($datos_lic['colegio'] == $item["AeId"]){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item["AeId"]; ?>" <?php echo $sel;?> ><?php echo utf8_encode($item['AeNombre'])?></option>
                    <?php }?>
                </select>
            </div>
                
            </div>
          
          
                 
		  
	<div style="margin-top: 36px;">
            <label>C&oacute;digos Licencias</label>
            <div>
              <input type="text" name="licencias"  style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_lic["codigo"]; ?>" />

            </div>

          </div>
          </div>
          
          
		  

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=licencias&id=0">Agregar Nuevo libro</a>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">C&oacute;digo</span></th>
                                              <th><span class="th_wrapp">Colegio</span></th>
                                              <th><span class="th_wrapp">Curso</span></th>
                                              <th><span class="th_wrapp">&Aacute;rea</span></th>
                                              <th><span class="th_wrapp">Licencias</span></th>
                                              <th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($lista_licencias as $item) {
                            $dats_col = $cconsultas->detalle_colegio($item['colegio']);
                            $datos_cur = $cconsultas->detalle_grado($item['curso']);
                            $datos_area = $cconsultas->detalle_area($item['area']);
                            
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<?php echo utf8_encode($dats_col[0]['IeIdentificacion'])?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo utf8_encode($dats_col[0]['IeNombre'])?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo utf8_encode($datos_cur[0]['Grado'])?>
                  </td>
                  <td><?php echo utf8_encode($datos_area[0]['AeNombre'])?></td>
                  <td><?= $item["licencia"] ?></td>
                  <td class="center titulo" width="100px">
                    <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=licencias&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>
                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  
function cambia_colegio(colegio){
     $.post("sel_grados.php", {colegio:colegio}, function(msg_2){
			$("#sel_grados").html(msg_2);
		});
}

</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>