<?php
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );

$grado = 0;
if (isset($_POST['grado'])){
    $grado = $_POST['grado'];
}
$cgrado = new Dbconsultas_brujula();
$colegio = $_POST['colegio'];
$dats = $cgrado->detalle_colegiobycod($colegio);
$lista = $cgrado->lista_grados($dats[0]['IeId']);

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<input type="hidden" name="id_colegio" value="<?php echo $dats[0]['IeId']?>" >
<select name="grado" id="grado">
    <option value="0">Seleccione una opción</option>
    <?php foreach ($lista as $item){
        $sel="";
        if ($item['GnId'] == $grado){
            $sel="selected";
        }        
        ?>
    <option value="<?php echo $item['GnId']?>" <?php echo $sel?>><?php echo $item['Grado'] ?></option>
<?php }?>
</select>
<script>
$(document).ready(function () {
$('#grado').selectmenu({
					style: 'dropdown',
					transferClasses: true,
					width: null
				});
});				
</script>