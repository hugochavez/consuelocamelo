<?php 
$cpaqutescolegio = new Dbpaquetecolegio();
$colegio = $_GET['colegio'];
$datos_li['colegio'] = $colegio;
$datos_li['campos_esp'] = "pq.nombre as paquete, a.grado as grado, a.id as id"; 
$datos_li['join'] = " INNER JOIN paquete pq on pq.id = a.paquete";
$paquetes_lista = $cpaqutescolegio->getList($datos_li);
$cpaquete = new Dbpaquete();
$paquete_lista = $cpaquete->getList();
$cdescuento = new Dbdescuento();
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  function cambia_paquete(paquete){
      $.post("descuentos.php", {paquete:paquete}, function(msg_2){
            $("#div_descuentos").html(msg_2);
      });
  }
  
  $(function() { 
  var theTable = $('#tabla')
   
  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
  
  
</script>



<?php
if(isset($_GET["id_del"])){
  	$cpaqutescolegio->deleteById($_GET["id_del"]);  
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
        
  $id = $_POST["id"];
  if ($id == 0) {	
        
        $colegio = $_POST['colegio'];
        $paquete = $_POST['paquete'];
        $codigodane = $_POST['codigodane'];
        $grado = $_POST['grado'];
        $cpaqutescolegio->setcolegio($colegio);
        $cpaqutescolegio->setpaquete($paquete);
        $cpaqutescolegio->setgrado($grado);
        $cpaqutescolegio->save();
        $id = $cpaqutescolegio->getMaxId();        
  } else {
        $colegio = $_POST['colegio'];
        $paquete = $_POST['paquete'];
        $codigodane = $_POST['codigodane'];
        $grado = $_POST['grado'];
        
        $cpaqutescolegio->setcolegio($colegio);
        $cpaqutescolegio->setpaquete($paquete);
        $cpaqutescolegio->setgrado($grado);
        $cpaqutescolegio->setid($id);
        $cpaqutescolegio->save();
        
  }
  
  $li_descuentos = array();
if (isset($_SESSION['lista_descuentos'])){
    $li_descuentos = unserialize($_SESSION['lista_descuentos']);
}

for ($a=0;$a<count($li_descuentos);$a++){
    $cdescuento->setcolegio($colegio);
    $cdescuento->setpaquete($id);
    $cdescuento->setdetalle($li_descuentos[$a]['id_art']);
    $cdescuento->setvalor('valor');
    $cdescuento->save();
}
  
}


if ($id){
$datos_paque = $cpaqutescolegio->getByPk($id);
?>
<script>
    $(function() { 
  <?php if (isset($datos_paque['paquete'])){?>
          cambia_paquete(<?php echo $datos_paque['paquete']?>);
  <?php }?> 

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
</script>
<?php
}
else{
$datos_paque = array();    
}
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      Colegios
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
        <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando libros" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">
          <input type="hidden" value="<?= $colegio ?>" name="colegio" id="colegio">

          <div style="margin-top: 36px;">
            <label>Paquete</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="paquete" id="paquete"  onchange="cambia_paquete($(this).val())" >
                    <option value="0">Seleccione una opción</option>
                    <?php foreach ($paquete_lista as $item){
                         $sel="";
                        if ($datos_paque['paquete'] == $item["id"]){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item["id"]; ?>" <?php echo $sel?>><?php echo $item['nombre']?></option>
                    <?php }?>
                </select>
            </div>
                            
           
          </div>
          
          <div style="margin-top: 36px;">
            <label>Descuentos</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;" id="div_descuentos">
                
            </div>
          </div>
          
          <div style="margin-top: 36px;">
            <label>Grado</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="grado" id="grado"   >
                    <option value="0">Seleccione una opción</option>
                    <?php for ($a=1;$a<=11;$a++){
                         $sel="";
                        if ($datos_paque['grado'] == $a){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $a; ?>" <?php echo $sel?>><?php echo $a?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          
          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=paquetes&id=0&colegio=<?php echo $colegio?>">Agregar Nuevo Paquete</a>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Paquete</span></th>
                                              <th><span class="th_wrapp">Grado</span></th>
                                              <th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($paquetes_lista as $item) {?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                      <?php echo $item['paquete']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['grado']?>
                  </td>
                  <td>
                      
                      <a class="uibutton icon edit" href="index.php?seccion=paquetes&id=<?= $item["id"] ?>&colegio=<?php echo $colegio?>">Editar</a>
                    <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=colegios&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>&colegio=<?php echo $colegio?>">Eliminar</a></td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>