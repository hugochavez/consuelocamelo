<?php session_start();
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );

$id = 0;
if (isset($_POST['id'])){
    $id = $_POST['id'];
}
$index = 0;
if (isset($_POST['index'])){
    $index = $_POST['index'];
}

$li_descuentos = array();
if (isset($_SESSION['lista_descuentos'])){
    $li_descuentos = unserialize($_SESSION['lista_descuentos']);
}


$cdescuento = new Dbdescuento();
if ($id > 0){
    $cdescuento->deleteById($id);
}else{
    unset($li_descuentos[($index-1)]);
    $li_descuentos = array_values($li_descuentos);
    $_SESSION['lista_descuentos'] = serialize($li_descuentos);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>        