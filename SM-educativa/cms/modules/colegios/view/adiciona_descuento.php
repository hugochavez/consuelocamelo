<?php session_start();
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );
$carticulo = new Dbarticulo();
$li_descuentos = array();
if (isset($_SESSION['lista_descuentos'])){
    $li_descuentos = unserialize($_SESSION['lista_descuentos']);
}

$paquete = 0;
if (isset($_POST['paquete'])){
    $paquete = $_POST['paquete'];
}
$articulo = "";
if (isset($_POST['articulo'])){
    $articulo = $_POST['articulo'];
}
$valor = "";
if (isset($_POST['valor'])){
    $valor = $_POST['valor'];
}
$datos_articulo = $carticulo->getByPk($articulo); 

$num = count($li_descuentos);
$li_descuentos[$num]['id']=0;
$li_descuentos[$num]['paquete']=$paquete;
$li_descuentos[$num]['id_art']=$articulo;
$li_descuentos[$num]['nombre_art']=$datos_articulo['nombre'];
$li_descuentos[$num]['valor']=$valor;
$_SESSION['lista_descuentos'] = serialize($li_descuentos);?>