<?php ini_set('display_errors','On');

//Evita presentar contenidos sin el login debido
include "../../../security/secure.php";
include "../../../core/class/db.class.php";
include "../../../../business/function/plGeneral.fnc.php";
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include "../../class/PhpThumbFactory.class.php";
include "../../class/ClassFile.class.php";
$ccompra = new Dbcompra();

 
$data_compra['campos_esp'] = " DISTINCT art.organizacion,art.centrodistribucion,'' as tipopedido,col.codigosap,
    DATE(a.fecha) as fecha,art.codigo as material,pq.fecha1 as fecha1,pq.id as id_paquete,
    1 as cantidad,dp.precio as valor,descu.valor as descuento,al.grado,al.codigo as codealumno,a.tipo as forma,a.id as idcompra";
$data_compra['join'] = " INNER JOIN compradetalle cd on cd.compra = a.id INNER JOIN paquete pq on pq.id = cd.paquete 
    INNER JOIN detalle_paquete dp ON dp.paquete = pq.id INNER JOIN articulo art on art.id = dp.articulo 
    INNER JOIN alumno al on al.id = a.alumno INNER JOIN colegio col on col.id = al.colegio 
    LEFT JOIN descuento descu ON descu.paquete = pq.id AND col.id = descu.colegio
";
$data_compra['estado'] = 1;


//var_dump($data_compra);
$lista_compras = $ccompra->getList($data_compra);

?>
<?php header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=registros_SAP.xls");
header("Pragma: no-cache");
header("Expires: 0");?>
<table >
					<thead><tr>
                                                <th>Organizaci&oacute;n de vtas</th>
                                                <th>Canal distribución</th>
                                                <th>Tipo de pedido</th>
                                                <th>Cod Colegio</th>
                                                <th>fecha Pago</th>
                                                <th>material</th>
                                                <th>cantidad</th>
                                                <th>valor</th>
                                                <th>%dto</th>
                                                <th>grado</th>
                                                <th>Codigo Alumno</th>
                                                <th>Valor total</th>
                                                <th>Via pago</th>
                                                <th>condici&oacute;n pago</th>
                                                <th>versi&oacute;n</th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		   $fecha_ant="";
                   $id_paquete = 0;
                   $codigosap = "";
                   $codealumno="";
                   $grado = "";
                   $forma = "";
                   $fecha="";
                   foreach ($lista_compras as $item) {
                   if ($id_paquete != $item['idcompra']){
                       if ($id_paquete > 0){
                            if (substr($fecha_ant,0,4).substr($fecha_ant,5,2).substr($fecha_ant,8,2) < substr($fecha,0,4).substr($fecha,5,2).substr($fecha,8,2)){
                                       ?>
                                          <tr class="odd gradeX">
                    <td class="center" width="150px">
                      CB01
                  </td>
                  <td class="center" width="150px">
                      70
                  </td>
                  <td class="center" width="150px">
                      
                  </td>
                  <td class="center" width="150px">
                      830390
                  </td>
                  <td class="center" width="150px">
                      <?php echo substr($fecha,8,2).substr($fecha,5,2).substr($fecha,0,4)?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $codigosap?>
                  </td>
                  <td class="center" width="150px">
                      1
                  </td>
                  
                  <td class="center" width="150px">
                      15000
                  </td>
                  <td class="center" width="150px">
                    	0
                  </td>
                  <td class="center" width="150px">
                    	GRADO<?php echo $grado ?>
                  </td>
                  <td><?= $codealumno; ?></td>
                  
                  <td>$15000</td>
                  <td><?= $forma ?></td>
                  <td>XP00</td>
                  <td>A</td>
                </tr>  
                                       <?php     
                            }
                       }
                       $fecha_ant = $item['fecha1'];
                       $fecha = $item['fecha'];
                       $id_paquete = $item['idcompra'];
                       $codigosap = $item['codigosap'];
                       $codealumno=$item["codealumno"];
                       $grado = $item['grado'];
                       $forma = $item["forma"];
                   }        
					?>
                <tr class="odd gradeX">
                    <td class="center" width="150px">
                      <?php echo $item['organizacion']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['centrodistribucion']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['tipopedido']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['codigosap']?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo substr($item['fecha'],8,2).substr($item['fecha'],5,2).substr($item['fecha'],0,4)?>
                  </td>
                  <td class="center" width="150px">
                      <?php echo $item['material']?>
                  </td>
                  <td class="center" width="150px">
                      1
                  </td>
                  
                  <td class="center" width="150px">
                      <?php echo $item['valor']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['descuento']?>
                  </td>
                  <td class="center" width="150px">
                    	GRADO<?php echo $item['grado'] ?>
                  </td>
                  <td><?= $item["codealumno"] ?></td>
                  
                  <td>$<?php echo $item['valor']?></td>
                  <td><?= $item["forma"] ?></td>
                  <td>XP00</td>
                  <td>A</td>
                </tr>
             	<?php }?>

            </tbody>
          </table>