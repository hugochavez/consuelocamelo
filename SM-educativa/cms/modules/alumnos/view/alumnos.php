<?php require_once "../../class/phpExcelReader/Excel/reader.php";
$calumno = new Dbalumno();
$ccompra = new Dbcompra();
$ccompradetalle = new Dbcompradetalle();

$ccolegio = new Dbcolegio();
if (isset($_POST['tipo_c'])){
    $tipo_c = $_POST['tipo_c'];
    if ($tipo_c == 1){
        echo "Cargue";
    $retorno = ClassFile::UploadFile("excelalumnos", "../../../../exceles/excelalumnos", "alumno_".rand(0,10000), "alumno_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$data = new Spreadsheet_Excel_Reader();
                $data->setOutputEncoding('CP1251');
                $data->read("../../../../exceles/excelalumnos/".$retorno["NameFile"]);
                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $codigocol = $data->sheets[0]['cells'][$i][1];
                    $datos_col['codigosap'] = $codigocol;
                    $colegios = $ccolegio->getList($datos_col);
                    $calumno->setcolegio($colegios[0]['id']);
                    $calumno->setgrado($data->sheets[0]['cells'][$i][2]/1);
                    $calumno->setconsecutivo($data->sheets[0]['cells'][$i][3]/1);
                    $calumno->setcodigo($data->sheets[0]['cells'][$i][4]);
                    $nombre = utf8_encode($data->sheets[0]['cells'][$i][5]);
                    if ($nombre != ""){
                        $arr_nombre = explode(" ",$nombre);
                        if (count($arr_nombre) > 2){
                        $calumno->setapellidos($arr_nombre[0]." ".$arr_nombre[1]);
                            if(!isset($arr_nombre[3])){
                                $arr_nombre[3] = "";
                            }
                        $calumno->setnombres($arr_nombre[2]." ".$arr_nombre[3]);                    
                        }else{
                        $calumno->setapellidos($arr_nombre[0]);
                        $calumno->setnombres($arr_nombre[1]);     
                        }
                        $calumno->setcorreo($data->sheets[0]['cells'][$i][6]);
                    }
                    else{
                        $calumno->setapellidos("");
                        $calumno->setnombres("");
                    }
                    $dats_alumnover['codigo']=$data->sheets[0]['cells'][$i][4];
                    $lista_alums = $calumno->getList($dats_alumnover);
                    if (count($lista_alums) > 0){
                        $calumno->setid($lista_alums[0]['id']);
                    }
                    
                        $calumno->save();
                    
                }
	}else{
		
	}
    }elseif($tipo_c == 2){
        echo "Cargue 2";
        $retorno = ClassFile::UploadFile("excelrecaudos", "../../../../exceles/excelalumnos", "recaudo_".rand(0,10000), "recaudo_".rand(0,10000));
	if($retorno["Status"]=="Uploader"){
		$data = new Spreadsheet_Excel_Reader();
                $data->setOutputEncoding('CP1251');
                $data->read("../../../../exceles/excelalumnos/".$retorno["NameFile"]);
                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $fecha = $data->sheets[0]['cells'][$i][1];
                    $fecha = substr($fecha,4,4)."-".substr($fecha,2,2)."-".substr($fecha,0,2);
                    $codigo = (int)$data->sheets[0]['cells'][$i][3];
                    $dats_alumno['codigo']=$codigo;
                    $array_alumnos = $calumno->getList($dats_alumno);
                    
                    //var_dump($array_alumnos);
                    if (count($array_alumnos) > 0){
                        //$where = " WHERE alumno = ".$array_alumnos[0]['id']." AND tipo = 'U' ";
                        //$ccompra->delete($where);
                        $ccompra->setfecha($fecha);
                        $ccompra->setestado(1);
                        $ccompra->setalumno($array_alumnos[0]['id']);
                        $ccompra->settipo("U");
                        $ccompra->save();
                        //echo "REGISTRA";
                        $id_compra = $ccompra->getMaxId();
                        $cpaquete = new Dbpaquete();
                        $datos_pa['campos_esp'] = "a.*";
                        $datos_pa['join'] = "INNER JOIN paquetecolegio pc on pc.paquete = a.id";
                        $datos_pa['where'] = " AND pc.colegio = ".$array_alumnos[0]['colegio']." AND pc.grado = ".$array_alumnos[0]['grado']." AND fecha2 > CURDATE()";
                        
                        $lista_paquetes = $cpaquete->getList($datos_pa);
                        $ccompradetalle->setcompra($id_compra);
                        $ccompradetalle->setpaquete($lista_paquetes[0]['id']);
                        $valor_p = str_replace(".", "", $data->sheets[0]['cells'][$i][4]);
                        $ccompradetalle->setvalor($valor_p);
                        $ccompradetalle->save();
                    }
                    else{
                        echo $codigo."<br>";
                    }
                }
	}else{
		
	}
    }
} 
$data_alu['campos_esp'] = "col.nombre as colegio, a.codigo,a.nombres,a.apellidos,a.correo,a.grado,a.id";
$data_alu['join'] = " INNER JOIN colegio col on col.id = a.colegio ";
//$alumnos = $calumno->getList($data_alu);
$alumnos = array();
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  $(function() { 
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      ALUMNOS
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <form method="post" action="" name="formalumnos" id="formalumnos" enctype="multipart/form-data">
            <div style="margin-top: 36px;">
            <label>Archivo Alumnos</label>
            <div>
                <input type="hidden" name="tipo_c" value="1">
                <input type="file" name="excelalumnos" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>

            </div>

          </div>
             <div><a id="submitForm" onclick="$('#formalumnos').submit();" class="uibutton normal large">Guardar</a></div>
        </form>
      
      <form method="post" action="" name="formrecaudos" id="formrecaudos" enctype="multipart/form-data">
            <div style="margin-top: 36px;">
            <label>Archivo Recaudos</label>
            <div>
                <input type="hidden" name="tipo_c" value="2">
                <input type="file" name="excelrecaudos" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>

            </div>

          </div>
             <div><a id="submitForm" onclick="$('#formrecaudos').submit();" class="uibutton normal large">Guardar</a></div>
        </form>
      <form method="post" action="compras_SAP.php" name="formsap" id="formsap" enctype="multipart/form-data">
            <div><a id="submitForm" onclick="$('#formsap').submit();" class="uibutton normal large">Exportar para SAP</a></div>
        </form>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Código</span></th>
                                              <th><span class="th_wrapp">Apellidos</span></th>
                                              <th><span class="th_wrapp">Nombres</span></th>
                                              <th><span class="th_wrapp">Colegio</span></th>
                                              <th><span class="th_wrapp">Grado</span></th>
                                              <th><span class="th_wrapp">Correo</span></th>
                                              <th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($alumnos as $item) {
                           
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                      <?php echo $item['codigo']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['apellidos']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['nombres'] ?>
                  </td>
                  <td><?= $item["colegio"] ?></td>
                  
                  <td><?= $item["grado"] ?></td>
                  <td><?= $item["correo"] ?></td>
                  <td></td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>