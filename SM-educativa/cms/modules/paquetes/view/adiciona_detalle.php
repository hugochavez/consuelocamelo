<?php session_start();
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );
$carticulo = new Dbarticulo();
$li_detalles = array();
if (isset($_SESSION['lista_detalles'])){
    $li_detalles = unserialize($_SESSION['lista_detalles']);
}

$paquete = 0;
if (isset($_POST['paquete'])){
    $paquete = $_POST['paquete'];
}
$articulo = "";
if (isset($_POST['articulo'])){
    $articulo = $_POST['articulo'];
}
$valor = "";
if (isset($_POST['valor'])){
    $valor = $_POST['valor'];
}
$datos_articulo = $carticulo->getByPk($articulo); 
$num = count($li_detalles);
$li_detalles[$num]['id']=0;
$li_detalles[$num]['paquete']=$paquete;
$li_detalles[$num]['id_art']=$articulo;
$li_detalles[$num]['nombre_art']=$datos_articulo['nombre'];
$li_detalles[$num]['precio']=$valor;
$_SESSION['lista_detalles'] = serialize($li_detalles);?>