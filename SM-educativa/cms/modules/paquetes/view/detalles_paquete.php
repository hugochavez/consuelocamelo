<?php session_start();
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );

$paquete = 0;
if (isset($_POST['paquete'])){
    $paquete = $_POST['paquete'];
}
$li_detalles = array();
if (isset($_SESSION['lista_detalles'])){
    $li_detalles = unserialize($_SESSION['lista_detalles']);
}

$cpaquetesdetalle = new Dbdetalle_paquete();
$dats['paquete']=$paquete;
$dats['campos_esp'] = "a.id as id,a.precio as precio,art.id as id_art,art.nombre as nombre_art ";
$dats['join'] = "INNER JOIN articulo art on art.id = a.articulo";
$lista_detalles = $cpaquetesdetalle->getList($dats);

$carticulo = new Dbarticulo();
$lista_articulos = $carticulo->getList();
$id = 0;
if ($_GET['id']){
    $id = $_GET['id'];
}
$datos_detalle = $cpaquetesdetalle->getByPk($id);


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form  name="form_detalle" id="form_detalle">
    Art&iacute;culo
    <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
<select name="articulo" id="articulo"  >
                    <option value="0">Seleccione una opción</option>
                    <?php  foreach ($lista_articulos as $item){
                        
                        if ($datos_detalle['articulo'] == $item['id']){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item['id']; ?>" <?php echo $sel;?> ><?php echo $item['nombre']; ?></option>
                    <?php }?>
                </select>
    </div><br>
Valor<br>
<input type="text" name="valor" style="width: 325px; margin-left: 152px; margin-top: -25px;" value="<?php echo $datos_detalle['precio']?>"/><br>
<input type="hidden" name="id" id="id" value="<?php echo $id?>"/>
<input type="hidden" name="paquete" id="paquete" value="<?php echo $paquete?>"/>
<a id="submitForm" onclick="adicionar_detalle()" class="uibutton normal large">Guardar</a>
</form>

<script>
$(document).ready(function () {
$('#articulo').selectmenu({
					style: 'dropdown',
					transferClasses: true,
					width: null
				});
});				
</script>

<script>
    function elimina_detalle(id,index){
        $.post("elimina_detalle.php", {id:id,index:index}, function(msg_2){
            $.post("detalles_paquete.php", {paquete:<?php echo $paquete?>}, function(msg_2){
                $("#div_detalles").html(msg_2);
            });
        });            
    }
    
    
    
        
    function adicionar_detalle(){
        $.post("adiciona_detalle.php", $("#form_detalle").serialize(), function(msg_2){
            //$("#div_xml").html(msg_2);
            $.post("detalles_paquete.php", {paquete:<?php echo $paquete?>}, function(msg_2){
                $("#div_detalles").html(msg_2);
            });
	});
    }
    
</script>

    <?php 
    $lista_detalles = array_merge($lista_detalles,$li_detalles);
    $a=0;
    foreach ($lista_detalles as $item){
        if ($item['id'] == 0){
            $a++;
        }
        echo $item['nombre_art']?> ($<?php echo $item['precio']?>)|<a href="javascript:;" onclick="editar_detalle(<?php echo $item['id']?>,<?php echo $a?>)">Editar</a>|<a href="javascript:;" onclick="elimina_detalle(<?php echo $item['id']?>,<?php echo $a?>)">Eliminar</a><br>
        <?php }?>
        