<?php require_once "../../class/phpExcelReader/Excel/reader.php";
$tipo_c = 0;
$clicenciasarticulo = new Dblicencias_articulo();
$articulo  =0;
if (isset($_GET['articulo'])){
    $articulo  = $_GET['articulo'];
}
?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  function cambia_libro(texto){
      $.post("datos_libro.php", {isbn:texto}, function(msg_2){
            $("#existente").html(msg_2);
            });
  }
  
  function editar(libro,nombre){
      $("#div_libro").html(nombre+"<input type='hidden' id='libro' name='libro' value='"+libro+"'/>");
      cambia_libro(libro);
  }
  
  function cambia_tipo(tipo){
      if (tipo == '1'){
          $("#div_archivo").hide();
          $("#div_url").show();
      }else{
          $("#div_archivo").show();
          $("#div_url").hide();
      }
  }
  
  $(function() { 
  $("#div_archivo").hide();    
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<?php
if(isset($_GET["id_del"])){
  //if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$clicenciasarticulo->deleteById($_GET["id_del"]);     
  //}
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
        
  $id = $_POST["id"];
  if ($id == 0) {	
        
        $codigo=$_POST['codigo'];
        $articulo = $_POST['articulo'];
        
        $clicenciasarticulo->setarticulo($articulo);
        $clicenciasarticulo->setlicencia($codigo);
        $clicenciasarticulo->save();
  }
}

$data_lic['articulo'] = $articulo;
$lista_licencias = $clicenciasarticulo->getList($data_lic);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      ART&Iacute;CULOS
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $articulo ?>" name="articulo" id="articulo">
          <input type="hidden" value="0" name="id" id="id">

          
          <div style="margin-top: 36px;" id="div_url">
              <label>C&oacute;digo</label>
            <input type="text" name="codigo" style="width: 325px; margin-left: 180px; margin-top: -25px;"  value=""/>
                            
            
          </div>
          
          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p></form>
        
          
        <a class="uibutton normal" href="index.php?seccion=licencias&articulo=<?php echo $articulo ?>">Agregar Nueva Licencias</a>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              
                                              <th><span class="th_wrapp">C&oacute;digo</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($lista_licencias as $item) {
                            
                            
					?>
                <tr class="odd gradeX">
                  
                  
                  <td class="center" width="150px">
                    	<?php echo $item['licencia'];?>
                  </td>
                  <td class="center titulo" width="100px">
                    
                      
                    <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=licencias&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>&articulo=<?php echo $articulo?>">Eliminar</a>
                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>


<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>