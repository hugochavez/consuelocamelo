<!DOCTYPE>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js ie10"> <!--<![endif]-->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>SM</title>

<meta name="viewport" content="width=device-width; initial-scale=1.0">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<link rel='stylesheet' id='twentytwelve-style-css'  href='http://www.ediciones-sm.com.co/wp-content/themes/sm/style.css?ver=3.5.1' type='text/css' media='all' />
<!-- CHECK / RADIO STYLE -->
<link href="assets/js/ezmark/css/ezmark.css" rel="stylesheet" /> 
<!----> 

<!-- COLORBOX -->
<link href="assets/css/colorbox.css" rel="stylesheet" type="text/css" />
<!----> 

<link rel="stylesheet" href="assets/css/chosen.css">

<link href="assets/css/sm_pagos.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="assets/js/lib/jquery-1.8.3.js"></script>

</head>
<body>
	