<?php 
$cgrado= new Dbgrado();

?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php

$nivel = 0;
if (isset($_REQUEST['nivel'])){
	$nivel = $_REQUEST['nivel'];
}


if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$datos_ord = $cgrado->getByPk($_GET["id_del"]);
  	if ($nivel == 0){
        $datos_eli['where']=" AND nivel is null AND orden > ".$datos_ord['orden']; 
        }else{
        $datos_eli['where']=" AND nivel = ".$nivel." AND orden > ".$datos_ord['orden'];    
        }
        $cgrado->update_masi("orden = (orden-1)",$datos_eli['where']);
        $cgrado->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
	$nivel = $_POST['nivel'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
        if ($nivel == 0){
            $datos_pr['where'] = " AND nivel is null ";
        }else{
            $datos_pr['where'] = " AND nivel = ".$nivel." ";
        }
        
        $li_grados = $cgrado->getList($datos_pr);
        $cgrado->setorden($li_grados[0]['max_orden'] + 1);
	$cgrado->setnombre($nombre);
        if ($nivel == 0){
            $nivel = NULL;
        }
	$cgrado->setnivel($nivel);
        $cgrado->save();
	$id = $cgrado->getMaxId();
  } else {
  	$nombre = $_POST['nombre'];
  	$cgrado->setnombre($nombre);
        $cgrado->setid($id);
	$cgrado->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cgrado->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	if ($nivel == 0){
            $datos_ord2['where'] = " AND nivel is null ";
        }else{
            $datos_ord2['nivel'] = $nivel;
        }
        
	$grados_orden = $cgrado->getList($datos_ord2);
	$cgrado_2  = new Dbgrado();
	$cgrado_2->setid($grados_orden[0]['id']);
	$cgrado_2->setorden($grados_orden[0]['orden'] + 1);
	$cgrado_2->save();
	$cgrado_3  = new Dbgrado();
	$cgrado_3->setid($id_ord);
	$cgrado_3->setorden($datos_ord2['orden']);
	$cgrado_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cgrado->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	if ($nivel == 0){
            $datos_ord2['where'] = " AND nivel is null ";
        }else{
            $datos_ord2['nivel'] = $nivel;
        }
	$grados_orde = $cgrado->getList($datos_ord2);
	$cgrado_2  = new Dbgrado();
	$cgrado_2->setid($grados_orde[0]['id']);
	$cgrado_2->setorden($grados_orde[0]['orden'] - 1);
	$cgrado_2->save();
	$cgrado_3  = new Dbgrado();
	$cgrado_3->setid($id_ord);
	$cgrado_3->setorden($datos_ord2['orden']);
	$cgrado_3->save();
}


// Consultamos la img actual del banner
$datos = $cgrado->getByPk($id);
if ($nivel == 0){
    $datos_li['where'] = "AND nivel is null order by orden";
}else{
$datos_li['nivel'] = $nivel;
$datos_li['where'] = "order by orden";
}
$grados_list = $cgrado->getList($datos_li);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando grados" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">
		  <input type="hidden" value="<?= $nivel ?>" name="nivel" id="nivel">

          		  
		  <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos["nombre"]; ?>" />

            </div>

          </div>

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
			<a class="uibutton normal" href="index.php?seccion=niveles">Volver</a>		  
           <a class="uibutton normal" href="index.php?seccion=grados&nivel=<?php echo $nivel?>&id=0">Agregar Nueva grado</a>
		   
		   <table class="display" >
					<thead>
						
					  <tr>
						<th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php $grado = 0;
		    	foreach ($grados_list as $item) {
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<?php 
							if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=grados&op=up&id_ord=<?php echo $item['id']?>&nivel=<?php echo $nivel?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($grados_list)){
								?>
								<a href="index.php?seccion=grados&op=down&id_ord=<?php echo $item['id']?>&nivel=<?php echo $nivel?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>
                  </td>
                  <td><?= $item["nombre"] ?></td>
                  <td class="center titulo" width="100px">
				  	 <a class="uibutton icon edit" href="index.php?seccion=grados&id=<?= $item["id"] ?>&nivel=<?php echo $nivel?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=grados&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>&nivel=<?php echo $nivel?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  $(document).ready(function() {
  		$("#texto").cleditor();
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>