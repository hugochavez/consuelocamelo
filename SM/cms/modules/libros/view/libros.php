<?php 
$clibro= new Dblibro();
$ctipo = new Dbtipo();
$cserie = new Dbserie();
$carea = new Dbarea();
$cnivel = new Dbnivel();
$datos['campos_esp']="a.caratula as caratula,ti.nombre as tipo,se.nombre as serie,area.nombre as area,gr.nombre as grado,ni.nombre as nivel,a.nombre as libro,a.id as id";
$datos["join"]=" INNER JOIN tipo ti on ti.id = a.tipo
LEFT JOIN serie se on se.id = a.serie
LEFT JOIN area on area.id = a.area
LEFT JOIN grado gr on gr.id = a.grado
LEFT JOIN nivel ni on ni.id = a.nivel";
$libros = $clibro->getList($datos);
$tipos = $ctipo->getList();
$series = $cserie->getList();
$areas = $carea->getList();
$niveles = $cnivel->getList();

?>
<script src="../../../js/filtrar_tablas.js"></script>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
  
  $(function() { 
  var theTable = $('#tabla')

  $("#filtro_text").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })

  /*$('#filter-form').submit(function(){
    theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
    return false;
  }).focus(); //Give focus to input field*/
});
  
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$clibro->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
        $retorno = ClassFile::UploadimagenFile("imagen", "../../../../imagenes/caratulas", "caratula_".rand(0,10000), "caratula_".rand(0,10000), 940, 350);
	if($retorno["Status"]=="Uploader"){
		$clibro->setcaratula($retorno["NameFile"]);	
	}else{
		
	}
  	$nombre = $_POST['nombre'];
        $codigo = $_POST['codigo'];
        $tipo = $_POST['tipo'];
        $serie = $_POST['serie'];
        $area = $_POST['area'];
        $nivel = $_POST['nivel'];
        $grado = $_POST['grado'];
        $descripcion = $_POST['descripcion'];
        $autor = $_POST['autor'];
        $paginas = $_POST['paginas'];
        $encuadernacion = $_POST['encuadernacion'];
        $precio = $_POST['precio'];
        $recursos = $_POST['recursos'];
	$clibro->setnombre($nombre);
        $clibro->setcodigo($codigo);
        $clibro->settipo($tipo);
        $clibro->setserie($serie);
        $clibro->setarea($area);
        $clibro->setnivel($nivel);
        $clibro->setgrado($grado);
        $clibro->setdescripcion($descripcion);
        $clibro->setautor($autor);
        $clibro->setpaginas($paginas);
        $clibro->setencuadernacion($encuadernacion);
        $clibro->setprecio($precio);
        $clibro->setrecurso_asociado($recursos);
        
        $clibro->save();
        //echo "grado=".$grado;
	$id = $clibro->getMaxId();
  } else {
        $retorno = ClassFile::UploadimagenFile("imagen", "../../../../imagenes/caratulas", "caratula_".rand(0,10000), "caratula_".rand(0,10000), 940, 350);
	if($retorno["Status"]=="Uploader"){
		$clibro->setcaratula($retorno["NameFile"]);	
	}else{
		
	}
  	$nombre = $_POST['nombre'];
        $codigo = $_POST['codigo'];
        $tipo = $_POST['tipo'];
        $serie = $_POST['serie'];
        $area = $_POST['area'];
        $nivel = $_POST['nivel'];
        $grado = $_POST['grado'];
        $descripcion = $_POST['descripcion'];
        $autor = $_POST['autor'];
        $paginas = $_POST['paginas'];
        $encuadernacion = $_POST['encuadernacion'];
        $precio = $_POST['precio'];
        $recursos = $_POST['recursos'];
	$clibro->setnombre($nombre);
        $clibro->setcodigo($codigo);
        $clibro->settipo($tipo);
        $clibro->setserie($serie);
        $clibro->setarea($area);
        $clibro->setnivel($nivel);
        $clibro->setgrado($grado);
        $clibro->setdescripcion($descripcion);
        $clibro->setautor($autor);
        $clibro->setpaginas($paginas);
        $clibro->setencuadernacion($encuadernacion);
        $clibro->setprecio($precio);        
        $clibro->setrecurso_asociado($recursos);
        $clibro->setid($id);
	$clibro->save();
  }
}


// Consultamos la img actual del banner
$datos_li = $clibro->getByPk($id);
$cgrado = new Dbgrado();
$datos_gra = $cgrado->getByPk($datos_li['grado']);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando libros" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>Tipo</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="tipo" id="tipo" onchange="cambia_tipo($(this).val())" >
                    <option value="0">Seleccione una opción</option>
                    <?php foreach ($tipos as $item){
                        $sel="";
                        if ($datos_li['tipo'] == $item["id"]){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item["id"]; ?>" <?php echo $sel;?> ><?php echo $item['nombre']?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          
          <div style="margin-top: 36px;">
            <label>Serie</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;" id="sel_series">
                
            </div>
          </div>
          
          <div style="margin-top: 36px;">
              <label>&Aacute;rea</label>
              <div style="width: 325px; margin-left: 200px; margin-top: -25px;" id="sel_areas">
                
            </div>
          </div>
          
          <div style="margin-top: 36px;">
              <label>Nivel</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="nivel" id="nivel"  onchange="cambia_nivel($(this).val())" >
                    <option value="0">Seleccione una opción</option>
                    <?php foreach ($niveles as $item){
                         $sel="";
                        if ($datos_gra['nivel'] == $item["id"]){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item["id"]; ?>" <?php echo $sel?>><?php echo $item['nombre']?></option>
                    <?php }?>
                </select>
            </div>
          </div>
          
          <div style="margin-top: 36px;">
              <label>Grado</label>
            <div id="sel_grados" style="width: 325px; margin-left: 200px; margin-top: -25px;">
                
            </div>
          </div>
          
		  
	<div style="margin-top: 36px;">
            <label>C&oacute;digo</label>
            <div>
              <input type="text" name="codigo" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_li["codigo"]; ?>" />

            </div>

          </div>	  
          
          <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_li["nombre"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
              <label>Descripci&oacute;n</label>
            <div>
                <textarea name="descripcion" id="descripcion" style="width: 325px; margin-left: 200px; margin-top: -25px;"><?php echo $datos_li["descripcion"]; ?></textarea>

            </div>

          </div>
		  
		  <div style="margin-top: 36px;">
            <label>Caratula</label>
            <?php if ($datos_li['caratula'] != ''){?>
            <img src="../../../../imagenes/caratulas/<?php echo $datos_li['caratula']?>" height="300px" >
                <?php }?>
            <div>
                <input type="file" name="imagen" style="width: 325px; margin-left: 200px; margin-top: -25px;"/>

            </div>

          </div>
          
          <div style="margin-top: 36px;">
            <label>Autor</label>
            <div>
                <textarea  id="autor" name="autor"  style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <?php echo $datos_li["autor"]; ?>
                </textarea>

            </div>

          </div>
          
          <div style="margin-top: 36px;">
              <label>P&aacute;ginas</label>
            <div>
              <input type="text" name="paginas" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_li["paginas"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
              <label>Encuadernaci&oacute;n</label>
            <div>
              <textarea  id="encuadernacion" name="encuadernacion"  style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <?php echo $datos_li["encuadernacion"]; ?>
                </textarea>     </div>

          </div>
		  
	<div style="margin-top: 36px;">
              <label>Precio</label>
            <div>
              <input type="text" name="precio" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos_li["precio"]; ?>" />

            </div>

          </div>
          
          <div style="margin-top: 36px;">
              <label>Recursos Asociados</label>
            <div>
                <textarea name="recursos" id="recursos" style="width: 325px; margin-left: 200px; margin-top: -25px;"><?php echo $datos_li["recurso_asociado"]; ?></textarea>

            </div>

          </div>
		  

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=libros&id=0">Agregar Nuevo libro</a>
           <div class="span5 pull-right tar">
		<label>Buscar: <input type="text" aria-controls="example" id="filtro_text"></label>
	</div>
		   <table class="display" id="tabla" >
					<thead>
						
					  <tr>
                                              <th><span class="th_wrapp">Car&aacute;tula</span></th>
                                              <th><span class="th_wrapp">Archivo</span></th>
                                              <th><span class="th_wrapp">Tipo</span></th>
                                              <th><span class="th_wrapp">Serie</span></th>
                                              <th><span class="th_wrapp">&Aacute;rea</span></th>
                                              <th><span class="th_wrapp">Nivel</span></th>
                                              <th><span class="th_wrapp">Grado</span></th>
                                              <th><span class="th_wrapp">Libro</span></th>                                              
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php 
		    	foreach ($libros as $item) {
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<img src="../../../../imagenes/caratulas/<?php echo $item['caratula']?>" height="200px" >
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['caratula']?>
                  </td>
                  <td class="center" width="150px">
                    	<?php echo $item['tipo'] ?>
                  </td>
                  <td><?= $item["serie"] ?></td>
                  <td><?= $item["area"] ?></td>
                  <td><?= $item["nivel"] ?></td>
                  <td><?= $item["grado"] ?></td>
                  <td><?= $item["libro"] ?></td>
                  <td class="center titulo" width="100px">
                    <a class="uibutton icon edit" href="index.php?seccion=unidades&libro=<?php echo $item['id']?>">Unidades</a>
                      <a class="uibutton icon edit" href="index.php?seccion=libros&id=<?= $item["id"] ?>">Editar</a>
                    <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=libros&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>
                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  $(document).ready(function() {
  		$("#descripcion").cleditor();
                $("#recursos").cleditor();
                $("#autor").cleditor();
                $("#encuadernacion").cleditor();
                <?php if ($id > 0){
				if ($datos_li['area'] == NULL){
					$datos_li['area'] =0;
				}
				if ($datos_li['grado'] == NULL){
					$datos_li['grado'] =0;
				}
				if ($datos_li['serie'] == NULL){
					$datos_li['serie'] =0;
				}
				?>
                        <?php if ($datos_gra['nivel'] > 0){?>
                        $.post("sel_grados.php", {nivel:<?php echo $datos_gra['nivel']?>,grado:<?php echo $datos_li['grado']?>}, function(msg_2){
						<?php }
                        else{?>
                        $.post("sel_grados.php", {tipo:<?php echo $datos_li['tipo']?>,grado:<?php echo $datos_li['grado']?>}, function(msg_2){    
                        <?php }?>
                        $("#sel_grados").html(msg_2);
                        });
                        $.post("sel_areas.php", {tipo:<?php echo $datos_li['tipo']?>,area:<?php echo $datos_li['area']?>}, function(msg_2){
						$("#sel_areas").html(msg_2);
                        });
                        $.post("sel_series.php", {tipo:<?php echo $datos_li['tipo']?>,serie:<?php echo $datos_li['serie']?>}, function(msg_2){
						$("#sel_series").html(msg_2);
                        });
                    <?php }?>
	});
function cambia_nivel(nivel){
     $.post("sel_grados.php", {nivel:nivel}, function(msg_2){
			$("#sel_grados").html(msg_2);
		});
}

function cambia_tipo(tipo){
     $.post("sel_areas.php", {tipo:tipo}, function(msg_2){
			$("#sel_areas").html(msg_2);
		});
    $.post("sel_grados.php", {tipo:tipo}, function(msg_2){
			$("#sel_grados").html(msg_2);
		});            
    $.post("sel_series.php", {tipo:tipo}, function(msg_2){
			$("#sel_series").html(msg_2);
		});            
}

</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>