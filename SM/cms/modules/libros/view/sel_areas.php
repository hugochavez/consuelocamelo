<?php
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );
$tipo = $_POST['tipo'];
$area = $_POST['area'];
$carea = new Dbarea();
$datos['join']="INNER JOIN tipos_area ta on ta.area = a.id and ta.tipo = ".$tipo;
$lista = $carea->getList($datos);

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<select name="area" id="area">
    <option value="0">Seleccione una opción</option>
    <?php foreach ($lista as $item){
        $sel="";
        if ($item['id'] == $area){
            $sel="selected";
        }        
        ?>
    <option value="<?php echo $item['id']?>" <?php echo $sel?>><?php echo $item['nombre'] ?></option>
<?php }?>
</select>
<script>
$(document).ready(function () {
$('#area').selectmenu({
					style: 'dropdown',
					transferClasses: true,
					width: null
				});
});				
</script>