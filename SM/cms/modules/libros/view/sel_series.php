<?php
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );
$datos['tipo']=$_POST['tipo'];
$serie = $_POST['serie'];
$cserie = new Dbserie();
$lista = $cserie->getList($datos);

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<select name="serie" id="serie">
    <option value="0">Seleccione una opción</option>
    <?php foreach ($lista as $item){
        $sel="";
        if ($item['id'] == $serie){
            $sel="selected";
        }        
        ?>
    <option value="<?php echo $item['id']?>" <?php echo $sel?>><?php echo $item['nombre'] ?></option>
<?php }?>
</select>
<script>
$(document).ready(function () {
$('#serie').selectmenu({
					style: 'dropdown',
					transferClasses: true,
					width: null
				});
});				
</script>