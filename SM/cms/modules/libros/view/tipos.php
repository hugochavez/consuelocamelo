<?php 
$ctipo= new Dbtipo();
?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$datos_ord = $ctipo->getByPk($_GET["id_del"]);
  	$datos_eli['where']="AND orden > ".$datos_ord['orden'];
	$ctipo->update_masi("orden = (orden-1)",$datos_eli['where']);
        $ctipo->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen



if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
	$li_tipos = $ctipo->getList($datos_pr);
	$ctipo->setorden($li_tipos[0]['max_orden'] + 1);
	$ctipo->setnombre($nombre);
        $ctipo->save();
	$id = $ctipo->getMaxId();
        $num_areas = $_POST['num_areas'];
        for ($a=1;$a<=$num_areas;$a++){
            if ($_POST['ck_area_'.$a] == "on"){
               $area =  $_POST['idarea_'.$a];
               $tipo = $id;
               $ctipos_area = new Dbtipos_area();
               $ctipos_area->setarea($area);
               $ctipos_area->settipo($tipo);
               $ctipos_area->save();
            }            
        }
        $num_niveles = $_POST['num_niveles'];
        for ($a=1;$a<=$num_niveles;$a++){
            if ($_POST['ck_nivel_'.$a] == "on"){
               $nivel =  $_POST['idnivel_'.$a];
               $tipo = $id;
               $ctipos_nivel = new Dbtipos_nivel();
               $ctipos_nivel->setnivel($nivel);
               $ctipos_nivel->settipo($tipo);
               $ctipos_nivel->save();
            }            
        }
  } else {
  	$nombre = $_POST['nombre'];
  	$ctipo->setnombre($nombre);
        $ctipo->setid($id);
	$ctipo->save();
        $num_areas = $_POST['num_areas'];
        $ctipos_area = new Dbtipos_area();
        $where = "WHERE tipo = ".$id;
        $ctipos_area->delete($where);
        for ($a=1;$a<=$num_areas;$a++){
            if ($_POST['ck_area_'.$a] == "on"){
               $area =  $_POST['idarea_'.$a];
               $tipo = $id;
               $ctipos_area = new Dbtipos_area();
               $ctipos_area->setarea($area);
               $ctipos_area->settipo($tipo);
               $ctipos_area->save();
            }
            
        }
        $num_niveles = $_POST['num_niveles'];
        $ctipos_nivel = new Dbtipos_nivel();
        $where = "WHERE tipo = ".$id;
        $ctipos_nivel->delete($where);
        for ($a=1;$a<=$num_niveles;$a++){
            if ($_POST['ck_nivel_'.$a] == "on"){
               $nivel =  $_POST['idnivel_'.$a];
               $tipo = $id;
               $ctipos_nivel = new Dbtipos_nivel();
               $ctipos_nivel->setnivel($nivel);
               $ctipos_nivel->settipo($tipo);
               $ctipos_nivel->save();
            }            
        }
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $ctipo->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	$tipos_orden = $ctipo->getList($datos_ord2);
	$ctipo_2  = new Dbtipo();
	$ctipo_2->setid($tipos_orden[0]['id']);
	$ctipo_2->setorden($tipos_orden[0]['orden'] + 1);
	$ctipo_2->save();
	$ctipo_3  = new Dbtipo();
	$ctipo_3->setid($id_ord);
	$ctipo_3->setorden($datos_ord2['orden']);
	$ctipo_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $ctipo->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	$tipos_orde = $ctipo->getList($datos_ord2);
	$ctipo_2  = new Dbtipo();
	$ctipo_2->setid($tipos_orde[0]['id']);
	$ctipo_2->setorden($tipos_orde[0]['orden'] - 1);
	$ctipo_2->save();
	$ctipo_3  = new Dbtipo();
	$ctipo_3->setid($id_ord);
	$ctipo_3->setorden($datos_ord2['orden']);
	$ctipo_3->save();
}


// Consultamos la img actual del banner
$datos = $ctipo->getByPk($id);
$datos_li['where'] = "order by orden";
$tipos_list = $ctipo->getList($datos_li);

$carea = new Dbarea();
$datos_ar['campos_esp']="a.id,a.nombre,ta.id as id_ta";
$datos_ar['join'] = "left join tipos_area ta on ta.area = a.id and tipo = ".$id;
$datos_ar['where'] = "order by a.orden";
$lista_areas = $carea->getList($datos_ar);
$cnivel = new Dbnivel();
$datos_gr['campos_esp']="a.id,a.nombre,tn.id as id_tn";
$datos_gr['join'] = "left join tipos_nivel tn on tn.nivel = a.id and tipo = ".$id;
$datos_gr['where'] = "order by a.orden";
$lista_niveles = $cnivel->getList($datos_gr);

?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando tipos" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          		  
		  <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos["nombre"]; ?>" />

            </div>

          </div>
          
          	  <div style="margin-top: 36px;">
                      <label>&Aacute;reas o Ciclos</label>
            <div>
                <?php $ind = 0;
                
                foreach ($lista_areas as $it_area){
                $ind++;
                
                    ?>
                <input type="hidden" name="idarea_<?php echo $ind?>" id="idarea_<?php echo $ind?>" value="<?php echo $it_area['id']?>"/>
                <input type="checkbox" name="ck_area_<?php echo $ind?>" id="ck_area_<?php echo $ind?>" <?php if ($it_area['id_ta'] > 0){?> checked <?php }?> />
                <?php echo utf8_encode($it_area['nombre'])?>
                
                <?php }?>
                <input type="hidden" name="num_areas" id="num_areas" value="<?php echo count($lista_areas)?>" />
            
            </div>

          </div>
          
          <div style="margin-top: 36px;">
                      <label>Niveles</label>
            <div>
                <?php $ind = 0;
                foreach ($lista_niveles as $it_nivel){
                $ind++;
                
                    ?>
                
                <input type="hidden" name="idnivel_<?php echo $ind?>" id="idnivel_<?php echo $ind?>" value="<?php echo $it_nivel['id']?>"/>
                <input type="checkbox" name="ck_nivel_<?php echo $ind?>" id="ck_nivel_<?php echo $ind?>" <?php if ($it_nivel['id_tn'] > 0){?> checked <?php }?> />
                <?php echo $it_nivel['nombre']?>                
                <?php }?>
                <input type="hidden" name="num_niveles" id="num_niveles" value="<?php echo count($lista_niveles)?>" />
            </div>
          </div>

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=tipos&id=0">Agregar Nuevo Tipo</a>
		   <table class="display" >
					<thead>
						
					  <tr>
						<th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php $tipo = 0;
		    	foreach ($tipos_list as $item) {
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<?php 
							if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=tipos&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($tipos_list)){
								?>
								<a href="index.php?seccion=tipos&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>
                  </td>
                  <td><?= $item["nombre"] ?></td>
                  <td class="center titulo" width="100px">
		<a class="uibutton icon edit" href="../../banner/view/index.php?seccion=menu&tipo=<?= $item["id"] ?>">Banner</a>
                      <a class="uibutton icon edit" href="index.php?seccion=tipos&id=<?= $item["id"] ?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=tipos&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  $(document).ready(function() {
  		$("#texto").cleditor();
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>