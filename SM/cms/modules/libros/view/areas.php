<?php 
$carea= new Dbarea();

?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$datos_ord = $carea->getByPk($_GET["id_del"]);
  	$datos_eli['where']="AND orden > ".$datos_ord['orden'];
	$carea->update_masi("orden = (orden-1)",$datos_eli['where']);
        $carea->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
	$li_areas = $carea->getList($datos_pr);
	$carea->setorden($li_areas[0]['max_orden'] + 1);
	$carea->setnombre($nombre);
        $carea->save();
	$id = $carea->getMaxId();
  } else {
  	$nombre = $_POST['nombre'];
  	$carea->setnombre($nombre);
        $carea->setid($id);
	$carea->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $carea->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
	$areas_orden = $carea->getList($datos_ord2);
	$carea_2  = new Dbarea();
	$carea_2->setid($areas_orden[0]['id']);
	$carea_2->setorden($areas_orden[0]['orden'] + 1);
	$carea_2->save();
	$carea_3  = new Dbarea();
	$carea_3->setid($id_ord);
	$carea_3->setorden($datos_ord2['orden']);
	$carea_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $carea->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
	$areas_orde = $carea->getList($datos_ord2);
	$carea_2  = new Dbarea();
	$carea_2->setid($areas_orde[0]['id']);
	$carea_2->setorden($areas_orde[0]['orden'] - 1);
	$carea_2->save();
	$carea_3  = new Dbarea();
	$carea_3->setid($id_ord);
	$carea_3->setorden($datos_ord2['orden']);
	$carea_3->save();
}


// Consultamos la img actual del banner
$datos = $carea->getByPk($id);
$datos_li['where'] = "order by orden";
$areas_list = $carea->getList($datos_li);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando areas" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          		  
		  <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos["nombre"]; ?>" />

            </div>

          </div>

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=areas&id=0">Agregar Nueva &aacute;rea</a>
		   <table class="display" >
					<thead>
						
					  <tr>
						<th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>
					</thead>
					<tbody>
		   <?php $area = 0;
		    	foreach ($areas_list as $item) {
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<?php 
							if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=areas&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < count($areas_list)){
								?>
								<a href="index.php?seccion=areas&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>
                  </td>
                  <td><?= $item["nombre"] ?></td>
                  <td class="center titulo" width="100px">
				  	 <a class="uibutton icon edit" href="index.php?seccion=areas&id=<?= $item["id"] ?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=areas&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  $(document).ready(function() {
  		$("#texto").cleditor();
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>