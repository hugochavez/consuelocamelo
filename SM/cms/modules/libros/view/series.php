<?php 
$cserie= new Dbserie();
$ctipo = new Dbtipo();
$tipos = $ctipo->getList();
?>
<script  type="text/javascript">
    function confirmar() {
    var answer = confirm("¿Está seguro de borrar el servicio?")
    if (answer){
      return true;
    }
    else{
      return false;
    }
  }
</script>

<?php
if(isset($_GET["id_del"])){
  if($_GET["confirm"]==base64_encode(md5($_GET["id_del"]))){
  	$datos_ord = $cserie->getByPk($_GET["id_del"]);
  	
        $cserie->deleteById($_GET["id_del"]);           
  }
}
?>
<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
  	$nombre = $_POST['nombre'];
        $tipo = $_POST['tipo'];
        $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
	$datos_pr['join'] = " ";
        $datos_pr['where'] = " AND tipo = ".$tipo;
	$li_series = $cserie->getList($datos_pr);
	$cserie->setorden($li_series[0]['max_orden'] + 1);
	$cserie->setnombre($nombre);
        $cserie->settipo($tipo);
        $cserie->save();
	$id = $cserie->getMaxId();
  } else {
  	$nombre = $_POST['nombre'];
        $tipo = $_POST['tipo'];
        $tipo_ant = $_POST['tipo_ant'];
        $orden = $_POST['orden'];
        if ($tipo_ant != $tipo){
            $datos_pr['campos_esp'] = "MAX(orden) as max_orden";
            $datos_pr['join'] = " ";
            $datos_pr['where'] = " AND tipo = ".$tipo;
            $li_series = $cserie->getList($datos_pr);
            $cserie->setorden($li_series[0]['max_orden'] + 1);
            $datos_eli['where']="AND orden > ".$orden;
            $cserie->update_masi("orden = (orden-1)",$datos_eli['where']);
        }
        
  	$cserie->setnombre($nombre);
        $cserie->settipo($tipo);
        $cserie->setid($id);
	$cserie->save();
  }
}

if ($_GET['op'] == "up"){
	$id_ord = $_GET['id_ord'];
        $datos_ord = $cserie->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']-1;
        $datos_ord2['tipo'] = $datos_ord['tipo'];
	$series_orden = $cserie->getList($datos_ord2);
	$cserie_2  = new Dbserie();
	$cserie_2->setid($series_orden[0]['id']);
	$cserie_2->setorden($series_orden[0]['orden'] + 1);
	$cserie_2->save();
	$cserie_3  = new Dbserie();
	$cserie_3->setid($id_ord);
	$cserie_3->setorden($datos_ord2['orden']);
	$cserie_3->save();
}elseif($_GET['op'] == "down"){
	$id_ord = $_GET['id_ord'];
	$datos_ord = $cserie->getByPk($id_ord);
	$datos_ord2['orden'] = $datos_ord['orden']+1;
        $datos_ord2['tipo'] = $datos_ord['tipo'];
	$series_orde = $cserie->getList($datos_ord2);
	$cserie_2  = new Dbserie();
	$cserie_2->setid($series_orde[0]['id']);
	$cserie_2->setorden($series_orde[0]['orden'] - 1);
	$cserie_2->save();
	$cserie_3  = new Dbserie();
	$cserie_3->setid($id_ord);
	$cserie_3->setorden($datos_ord2['orden']);
	$cserie_3->save();
}


// Consultamos la img actual del banner
$datos = $cserie->getByPk($id);
$datos_li['campos_esp'] = "a.id as id,a.nombre as nombre,a.tipo as id_tipo,a.orden as orden,ti.nombre as tipo";
$datos_li['join'] = "INNER JOIN tipo ti on ti.id = a.tipo";
$datos_li['where'] = "order by a.tipo,a.orden";
$series_list = $cserie->getList($datos_li);
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      QUIENES
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando series" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">
          <input type="hidden" value="<?= $datos['tipo'] ?>" name="tipo_ant" id="tipo_ant">
          <input type="hidden" value="<?= $datos['orden'] ?>" name="orden" id="orden">

          <div style="margin-top: 36px;">
            <label>Tipo</label>
            <div style="width: 325px; margin-left: 200px; margin-top: -25px;">
                <select name="tipo" id="tipo"  >
                    <option value="0">Seleccione una opción</option>
                    <?php foreach ($tipos as $item){
                        $sel="";
                        if ($datos['tipo'] == $item["id"]){
                            $sel="selected";
                        }
                        ?>
                        <option value="<?php echo $item["id"]; ?>" <?php echo $sel;?> ><?php echo $item['nombre']?></option>
                    <?php }?>
                </select>
            </div>
          </div>		  
		  
          <div style="margin-top: 36px;">
            <label>Nombre</label>
            <div>
              <input type="text" name="nombre" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $datos["nombre"]; ?>" />

            </div>

          </div>

          <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>
          <p>&nbsp;</p>
          
           <a class="uibutton normal" href="index.php?seccion=series&id=0">Agregar Nueva serie</a>
		   <table class="display" >
					<tbody>
		   <?php 
                    $tipo = 0;
		    	foreach ($series_list as $item) {
                            if ($tipo != $item['id_tipo']){
                                ?>
                                          <tr>
                                              <th colspan="3"><span class="th_wrapp"><?php echo $item['tipo']?></span></th>
					  </tr>
                                          <tr>
						<th><span class="th_wrapp">Orden</span></th>
                                              <th><span class="th_wrapp">Nombre</span></th>
						<th><span class="th_wrapp">Acciones</span></th>
					  </tr>  
                                            <?php
                                $dats=array();            
                                $tipo = $item['id_tipo'];
                                $dats['campos_esp']="COUNT(id) as numero";
                                $dats['join']=" ";
                                $dats['tipo']=$tipo;
                                $total_li = $cserie->getList($dats);
                            }
					?>
                <tr class="odd gradeX">
                  <td class="center" width="150px">
                    	<?php 
							if($item['orden'] > 1){
								?>
								<a href="index.php?seccion=series&op=up&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seleteup.png" width="40px" /></a>
								<?php
							}
							?>
							<?php echo $item['orden'];
							
							if($item['orden'] < $total_li[0]['numero']){
								?>
								<a href="index.php?seccion=series&op=down&id_ord=<?php echo $item['id']?>">
								<img src="../../../images/forms/seletedown.png" width="40px" /></a>
								<?php
							}
						?>
                  </td>
                  <td><?= $item["nombre"] ?></td>
                  <td class="center titulo" width="100px">
				  	 <a class="uibutton icon edit" href="index.php?seccion=series&id=<?= $item["id"] ?>">Editar</a>
                     <a class="uibutton icon special edit " onclick="return confirmar();" href="index.php?seccion=series&id_del=<?= $item["id"] ?>&confirm=<?= base64_encode(md5($item["id"])) ?>">Eliminar</a>

                  </td>
                </tr>
             	<?php }?>

            </tbody>
          </table>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>



    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  $(document).ready(function() {
  		$("#texto").cleditor();
	});	
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Nuestros servicios editado correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Campo editado correctamente");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los campos ");\',400);</script>';
    }
  }
  
}
?>