<?php
include( '../../../../include/define.php' );
include( '../../../../include/config.php' );
include( '../../../../business/function/plGeneral.fnc.php' );
if (isset($_POST['nivel'])){
    $datos['nivel'] = $_POST['nivel'];
}elseif(isset($_POST['tipo'])){
    $datos['join'] = " INNER JOIN tipos_grado tg on tg.grado = a.id ";
    $datos['where'] = " AND tg.tipo = ".$_POST['tipo'];
}
$grado = $_POST['grado'];
$cgrado = new Dbgrado();
$lista = $cgrado->getList($datos);

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<select name="grado" id="grado">
    <option value="0">Seleccione una opción</option>
    <?php foreach ($lista as $item){
        $sel="";
        if ($item['id'] == $grado){
            $sel="selected";
        }        
        ?>
    <option value="<?php echo $item['id']?>" <?php echo $sel?>><?php echo $item['nombre'] ?></option>
<?php }?>
</select>
<script>
$(document).ready(function () {
$('#grado').selectmenu({
					style: 'dropdown',
					transferClasses: true,
					width: null
				});
});				
</script>