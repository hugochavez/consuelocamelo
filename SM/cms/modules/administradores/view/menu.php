<?php
$id = (int) $_GET["id"];
// Validamos si hizo post y desea subir una imagen
if (isset($_POST["id"])) {
  $id = $_POST["id"];

  if ($id == 0) {
    $id = $db->getLastId();
  } else {

    $db->doQuery("UPDATE generales SET 
                                        titulo ='" . GetData("titulo") . "', 
                                        texto='" . GetData("texto") . "'                                         
                                        WHERE id=" . $id, 4);

    $retorno = ClassFile::UploadFile("naturales", "../../../../imagenes/registro","reg_naturales_$id");
    $retorno1 = ClassFile::UploadFile("juridicas", "../../../../imagenes/registro","reg_juridicas_$id");
    if ($retorno["Status"] == "Uploader" || $retorno1["Status"] == "Uploader" ) {
      $db->doQuery("UPDATE generales SET
                                        url='" . $retorno["NameFile"] . "',
                                        seccion='" . $retorno1["NameFile"] . "'
                                        WHERE id=" . $id, 4);$val=1;
    } else {
      $val=1;
    }
  }
}

// Consultamos la img actual del banner
$db->doQuery("SELECT * FROM generales WHERE id=" . $id, 1);
$oficina = $db->results[0];
?>

<!-- full width -->
<div class="widget">
  <div class="header">
    <span>
      <span class="ico gray window"></span>
      REQUISITOS
    </span>
  </div>

  <div class="content">
    <div class="formEl_b">
      <!--Inicio del contenido del modulo-->
      <fieldset>
        <h3><?= ($id == 0) ? "" : "Editando REQUISITOS" ?></h3>

        <form method="post" action="" name="forminterno" id="forminterno" enctype="multipart/form-data">

          <input type="hidden" value="<?= $id ?>" name="id" id="id">

          <div style="margin-top: 36px;">
            <label>T&iacute;tulo</label>
            <div>
              <input type="text" name="titulo" style="width: 325px; margin-left: 200px; margin-top: -25px;" value="<?php echo $oficina["titulo"]; ?>" />

            </div>

          </div>

          <div style="margin-top: 36px;">
            <label>Contenido</label>
            <div>
              <textarea name="texto" style="width: 700px;margin: -25px 0px 2px 200px;height: 139px;"><?php echo $oficina["texto"]; ?></textarea>
            </div>
          </div>               

          <p>&nbsp;</p>
          <div id="naturales" name="naturales">
            <div style="margin-top: 36px;">
              <label>Solicitud personas Naturales</label>
              <div>
                <input  style="margin-top: -16px;margin-left: 200px;" type="file" name="naturales" id="img" />
              </div><br>
              <div style="margin-left: 600px; margin-top: -33px;">
              <label>Si quieres ver tu documento haz </label><a href="../../../../imagenes/registro/<?php echo $oficina["url"]; ?>" />Click aquí</a>
              </div>
                
            </div>
          </div>
          <div id="juridicas" name="juridicas">
            <div style="margin-top: 36px;">
              <label>Solicitud personas Juridicas</label>
              <div>
                <input  style="margin-top: -16px;margin-left: 200px;" type="file" name="juridicas" id="img" />
              </div><br>
              <div style="margin-left: 600px; margin-top: -33px;" >
              <label>Si quieres ver tu documento haz </label><a href="../../../../imagenes/registro/<?php echo $oficina["seccion"]; ?>" />Click aquí</a>
              </div>

            </div>
          </div>
          </div>
          <p>&nbsp;</p>

        </form>

      </fieldset>

      <p>&nbsp;</p>

      <div><a id="submitForm" onclick="$('#forminterno').submit();" class="uibutton normal large">Guardar</a></div>

    </div>
  </div>

  <!--Fin del Contenido del Modulo-->
</div>

<script>
  cambiar(<?= $oficina["id_tipos"] ?>);
</script>

<?php 
if(isset($val))
{
  $erno = $val;
  if(intval($erno))
  {
    if($erno == 1)
    {
      echo '<script>setTimeout(\'alert("Registro editado  correctamente");\',400);</script>';
    }
    if($erno == 2)
    {
      echo '<script>setTimeout(\'alert("Error al editar el registro");\',400);</script>';
    }
     if($erno == 3)
    {
     echo '<script>setTimeout(\'alert("Agrega todos los datos ");\',400);</script>';
    }
  }
  
}
?>