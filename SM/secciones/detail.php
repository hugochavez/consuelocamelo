<?php include("header.php");?>


<?php 

$tipo = $_GET['tipo'];
$area = $_GET['area'];
$nivel = $_GET['nivel'];
$grado = $_GET['grado'];
$serie = $_GET['serie'];
$busqueda = $_GET['busqueda'];
$where = "";
$texto = "";
$url_filtros = "";
if ($tipo >0){
    $where.=" AND a.tipo = ".$tipo;
    $ctipo = new Dbtipo();
    $dat_tipo = $ctipo->getByPk($tipo);
    $texto .= "".$dat_tipo['nombre']."|";
    $url_filtros .= "&tipo=".$tipo."";
}
if ($area > 0){
    $where.=" AND a.area= ".$area;   
    $carea = new Dbarea();
    $dat_area = $carea->getByPk($area);
    $texto .= "".$dat_area['nombre']."|";
    $url_filtros .= "&area=".$area."";
}
if ($nivel > 0){
    $where.=" AND a.nivel = ".$nivel;
    $cnivel = new Dbnivel();
    $dat_nivel = $cnivel->getByPk($nivel);
    $texto .= "".$dat_nivel['nombre']."|";
    $url_filtros .= "&nivel=".$nivel."";
}
if ($grado > 0){
    $where.=" AND a.grado = ".$grado;
    $cgrado = new Dbgrado();
    $dat_nivel = $cgrado->getByPk($grado);
    $texto .= "".$dat_nivel['nombre']."|";
    $url_filtros .= "&grado=".$grado."";
}
if ($serie > 0){
    $where.=" AND a.serie = ".$serie;
    $cserie = new Dbserie();
    $dat_serie = $cserie->getByPk($serie);
    $texto .= "".$dat_serie['nombre']."|";
    $url_filtros .= "&serie=".$serie."";
}
if ($busqueda != ''){
    $where.=" AND a.nombre LIKE '%".$busqueda."%'";
    $texto .= "".$busqueda."|";
    $url_filtros .= "&busqueda=".$busqueda."";
    
}

$pagina = 1;
if (isset($_GET['pagina'])){
    $pagina = $_GET['pagina'];
    $url_filtros .= "&pagina=".$pagina."";
}
$url_filtros = substr($url_filtros, 1);


//include("menu.php");
$id = $_GET['id_libro'];
$clibro = new Dblibro();
$datos = $clibro->getByPk($id);
$dats_unidades['libro']=$id;
$dats_unidades['where']=" order by orden";
$cunidades = new Dbunidades();
$lista_unidades = $cunidades->getList($dats_unidades);
$tipo = $datos['tipo']; 




?>
<?php include("menu_detail.php");?> 
<div class="detail-block">
	<h3><?php echo $datos['nombre']?></h3>
    
    <div class="slider-img">
        <img src="imagenes/caratulas/<?php echo $datos['caratula']?>" width="400px">
        <p>
            <?php echo $datos['recurso_asociado'];?>
        </p>
    </div>
    <div class="slider-info">
    	<table>
        	<tr>
                    <td style="vertical-align: top"><b>Autor:</b></td>
             	<td><?php echo (str_replace(',','<br>',($datos['autor'])))?></td>
            </tr>
            <tr>
            	<td><b>ISBN:</b></td>
             	<td><?php echo utf8_encode($datos['codigo'])?></td>
            </tr>
            <tr>
            	<td><b>P&aacute;ginas:</b></td>
             	<td><?php echo utf8_encode($datos['paginas'])?></td>
            </tr>
            <tr>
            	<td><b>Encuadernaci&oacute;n:</b></td>
             	<td><?php echo $datos['encuadernacion']?></td>
            </tr>
            <tr>
            	<td><b>Precio:</b></td>
                <td><?php echo number_format($datos['precio'])?></td>
            </tr>
        </table>
        <p>
             <?php echo $datos['descripcion'];?>
         </p>
         <?php if (count($lista_unidades) > 0){?>
         <p>Unidades<br>
         <?php foreach ($lista_unidades as $item){?>
             <?php echo utf8_encode($item['nombre'])?><br>
             <?php }?>
         </p>
         <?php }?>
    </div>
    <div class="clear"></div>
    <div class="slider-img">
    
    </div>
    <div class="slider-info">
         
         
		<!--<a class="detail-btn" href="javascript:history.back();">REGRESAR A PROYECTOS</a> -->
    </div>
    <div class="clear"></div>
     
</div>


<?php include("footer.php");?>
