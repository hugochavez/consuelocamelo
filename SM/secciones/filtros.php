<?php include("header.php");?>
<?php 
$ctipo = new Dbtipo();
$tipos = $ctipo->getList();
$cnivel = new Dbnivel();
$niveles = $cnivel->getList();
?>
<script>
    $(document).ready(function () {
        cambia_tipo(0);
    });
    function cambia_tipo(tipo){
        $.post("secciones/sel_areas.php", {tipo:tipo}, function(msg_2){
                $("#div_areas").html(msg_2);
        });
        $.post("secciones/sel_series.php", {tipo:tipo}, function(msg_2){
                $("#div_series").html(msg_2);
        });
        $.post("secciones/sel_grados.php", {tipo:tipo}, function(msg_2){
                $("#div_grados").html(msg_2);
        });
    }
    function cambia_nivel(nivel){
        $.post("secciones/sel_grados.php", {nivel:nivel}, function(msg_2){
                $("#div_grados").html(msg_2);
        });
    }
</script>
<div class="filter">
	<div class="filter-block">
        <h4>PROYECTOS EDUCATIVOS</h4>
        <form action="index.php" method="get" id="form1" name="form1">
            <select style="width:175px;" name="tipo" id="tipo" onchange="cambia_tipo($(this).val())">
                <option value="0">TIPO</option>
                <?php foreach ($tipos as $item){ ?>
                <option value="<?php echo $item['id']?>"><?php echo $item['nombre']?></option>
                <?php
                }?>
            </select>
            <select style="width:175px;"  onchange="cambia_nivel($(this).val())" id="nivel" name="nivel">
                <option value="0">Nivel</option>
                <?php foreach ($niveles as $item){ ?>
                <option value="<?php echo $item['id']?>"><?php echo $item['nombre']?></option>
                <?php
                }?>
            </select>
            <div id="div_areas"></div>
            <div id="div_grados"></div>
            <div id="div_series"></div>           
                
            <input type="text" placeholder="BUSCAR" nombre="busqueda" >
            <input type="hidden" value="index" id="seccion" name="seccion">
            <input type="submit"  />
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
</div>