<?php
include( '../include/define.php' );
include( '../include/config.php' );
include( '../business/function/plGeneral.fnc.php' );
header("Content-Type: text/html; charset=iso-8859-1");
$clibro = new Dblibro();
$tipo = $_POST['tipo'];
$area = $_POST['area'];
$nivel = $_POST['nivel'];
$grado = $_POST['grado'];
$serie = $_POST['serie'];
$busqueda = $_POST['busqueda'];
$where = "";
$texto = "";
$url_filtros = "";
if ($tipo >0){
    $where.=" AND a.tipo = ".$tipo;
    $ctipo = new Dbtipo();
    $dat_tipo = $ctipo->getByPk($tipo);
    $texto .= "".$dat_tipo['nombre']."|";
    $url_filtros .= "&tipo=".$tipo."";
}
if ($area > 0){
    $where.=" AND a.area= ".$area;   
    $carea = new Dbarea();
    $dat_area = $carea->getByPk($area);
    $texto .= "".$dat_area['nombre']."|";
    $url_filtros .= "&area=".$area."";
}
if ($nivel > 0){
    $where.=" AND a.nivel = ".$nivel;
    $cnivel = new Dbnivel();
    $dat_nivel = $cnivel->getByPk($nivel);
    $texto .= "".$dat_nivel['nombre']."|";
    $url_filtros .= "&nivel=".$nivel."";
}
if ($grado > 0){
    $where.=" AND a.grado = ".$grado;
    $cgrado = new Dbgrado();
    $dat_nivel = $cgrado->getByPk($grado);
    $texto .= "".$dat_nivel['nombre']."|";
    $url_filtros .= "&grado=".$grado."";
}
if ($serie > 0){
    $where.=" AND a.serie = ".$serie;
    $cserie = new Dbserie();
    $dat_serie = $cserie->getByPk($serie);
    $texto .= "".$dat_serie['nombre']."|";
    $url_filtros .= "&serie=".$serie."";
}
if ($busqueda != ''){
    $where.=" AND a.nombre LIKE '%".$busqueda."%'";
    $texto .= "".$busqueda."|";
    $url_filtros .= "&busqueda=".$busqueda."";
    
}

$texto = substr($texto,0,strlen($texto)-1);
$datos['where']=$where;
$por_pag = 8;
$num_dat = $clibro->getCount($datos);
$num_db = $num_dat[0]['count(*)'];
$num_pags = ceil($num_db/$por_pag);
$pagina = 1;
if (isset($_POST['pagina'])){
    $pagina = $_POST['pagina'];
    $url_filtros .= "&pagina=".$pagina."";
}
$url_filtros = substr($url_filtros, 1);
$inicio = ($pagina-1)*$por_pag;
$datos['campos'] = " ,ni.orden as niorden,gr.orden as grorden";
$datos['join'] =" LEFT JOIN nivel ni on ni.id = a.nivel LEFT JOIN grado gr on gr.id = a.grado  ";
$datos['where'].=" ORDER BY ni.orden,gr.orden  LIMIT ".$inicio.", ".$por_pag;
$lista_libros = $clibro->getList($datos);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h3><?php echo utf8_decode($texto);?></h3>

    <ul>
        <?php $ind=0;
        foreach ($lista_libros as $item){
            $ind++;
            ?>
    	<a href="index.php?seccion=detail&id_libro=<?php echo $item['id'] ?>&<?php echo $url_filtros ?>"><li <?php if (($ind % 4) == 0){
            ?> class="no-marg" <?php
        }?>>
        	<div class="result-img">
                    <img src="imagenes/caratulas/<?php echo $item['caratula']?>" width="240px" >
            </div>
                <h4><?php echo utf8_decode($item['nombre']);?></h4>
        </li></a>
        <?php }?>
        
        <div class="clear"></div>
    </ul>
<div class="pagination">
    <?php if ($pagina > 1){?>
		<a  href="javascript:;" onClick="paginacion(<?php echo $pagina-1?>);">Anterior</a>
                <?php }?>
			<?php for ($a=1;$a<=$num_pags;$a++){
                            if ((abs($a - $pagina) <= 5) || ($pagina < 5 && $a < 10) || (($num_pags - $pagina) < 5 && ($num_pags - $a) < 10)){
                                if ($a == $pagina){
                                   ?>
                                   <b><?php echo $a?></b>
                                   <?php
                                }else{
                                ?>
				<a href="javascript:;" onClick="paginacion(<?php echo $a?>);"><?php echo $a?></a>
			
                                <?php }
                            }
                        }
                        ?>
                                <?php if ($pagina < $num_pags){?>
			<a href="javascript:;" onClick="paginacion(<?php echo $pagina+1?>);">Siguiente</a>
		<?php }?>
</div>