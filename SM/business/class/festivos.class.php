<?php
/**
 * PhpThumb Base Class Definition File
 * 
 * This file contains the definition for the ThumbBase object
 * 
 * PHP Version 5 with GD 2.0+
 * PhpThumb : PHP Thumb Library <http://phpthumb.gxdlabs.com>
 * Copyright (c) 2009, Ian Selby/Gen X Design
 * 
 * Author(s): Ian Selby <ian@gen-x-design.com>
 * 
 * Licensed under the MIT License
 * Redistributions of files must retain the above copyright notice.
 * 
 * @author Ian Selby <ian@gen-x-design.com>
 * @copyright Copyright (c) 2009 Gen X Design
 * @link http://phpthumb.gxdlabs.com
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version 3.0
 * @package PhpThumb
 * @filesource
 */

/**
 * ThumbBase Class Definition
 * 
 * This is the base class that all implementations must extend.  It contains the 
 * core variables and functionality common to all implementations, as well as the functions that 
 * allow plugins to augment those classes.
 * 
 * @package PhpThumb
 * @subpackage Core
 */
abstract class festivos 
{
	/**
	 * All imported objects
	 * 
	 * An array of imported plugin objects
	 * 
	 * @var array
	 */
	
	protected $pascua;
	protected $festivos;
	
	/**
	 * Class constructor
	 * 
	 * @return ThumbBase
	 */
	public function __construct ()
	{
		
	}
	
	public function convertir_fecha($dias){
		return date("M-d",$dias*(60*60*24));
	}
	
	public function esfestivo($fecha){
		$festivos_arr = $this->festivos(substr($fecha,0,4));
		foreach ($festivos_arr as $itemfes){
			if ($itemfes['fecha'] == substr($fecha,5)){
				return "1";
			}
		}
		return "0";
	}
	
	public function festivos($anno){
		$this->pascua($anno);
		$pascua = $this->pascua;
		$arr_p = explode("-",$pascua);		
		$dias_p = mktime(0,0,0,$arr_p[1],$arr_p[0],$arr_p[2])/(60*60*24);
		$festivos_arr[1]['fecha']="01-01";
		$festivos_arr[1]['tipo']="1";
		$festivos_arr[2]['fecha']="01-06";
		$festivos_arr[2]['tipo']="2";
		$festivos_arr[3]['fecha']=convertir_fecha($dias_p-3);
		$festivos_arr[3]['tipo']="3";
		$festivos_arr[4]['fecha']=convertir_fecha($dias_p-2);
		$festivos_arr[4]['tipo']="3";
		$festivos_arr[5]['fecha']="05-01";
		$festivos_arr[5]['tipo']="1";
		$festivos_arr[6]['fecha']=convertir_fecha($dias_p+43);
		$festivos_arr[6]['tipo']="3";
		$festivos_arr[7]['fecha']=convertir_fecha($dias_p+64);
		$festivos_arr[7]['tipo']="3";
		$festivos_arr[8]['fecha']=convertir_fecha($dias_p+71);
		$festivos_arr[8]['tipo']="3";
		$festivos_arr[9]['fecha']="06-29";
		$festivos_arr[9]['tipo']="2";
		$festivos_arr[10]['fecha']="07-20";
		$festivos_arr[10]['tipo']="1";
		$festivos_arr[11]['fecha']="08-07";
		$festivos_arr[11]['tipo']="1";
		$festivos_arr[12]['fecha']="08-15";
		$festivos_arr[12]['tipo']="2";
		$festivos_arr[13]['fecha']="10-12";
		$festivos_arr[13]['tipo']="2";
		$festivos_arr[14]['fecha']="11-01";
		$festivos_arr[14]['tipo']="2";
		$festivos_arr[15]['fecha']="11-11";
		$festivos_arr[15]['tipo']="2";
		$festivos_arr[16]['fecha']="12-08";
		$festivos_arr[16]['tipo']="1";
		$festivos_arr[17]['fecha']="12-25";
		$festivos_arr[17]['tipo']="1";
		return $festivos_arr;
	}
	
	/**
	 * Imports plugins in $registry to the class
	 * 
	 * @param array $registry
	 */
	
	public function pascua ($anno){
# Constantes m�gicas
    $M = 24;
    $N = 5;
#C�lculo de residuos
    $a = $anno % 19;
    $b = $anno % 4;
    $c = $anno % 7;
    $d = (19*$a + $M) % 30;
    $e = (2*$b+4*$c+6*$d + $N) % 7;
# Decidir entre los 2 casos:
    if ( $d + $e < 10 ) {
        $dia = $d + $e + 22;
        $mes = 3; // marzo
        }
    else {
        $dia = $d + $e - 9;
        $mes = 4; //abril
        }
# Excepciones especiales (seg�n art�culo)
    if ( $dia == 26  and $mes == 4 ) { // 4 = abril
        $dia = 19;
        }
    if ( $dia == 25 and $mes == 4 and $d==28 and $e == 6 and $a >10 ) { // 4 = abril
        $dia = 18;
        }
    	$ret = $dia.'-'.$mes.'-'.$anno;
        return ($ret);
	}
	

}
