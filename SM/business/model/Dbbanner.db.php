<?php class Dbbanner extends DbDAO{
  public $id;
  protected $tipo;
  protected $titulo;
  protected $texto;
  protected $imagen;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function settitulo($titulo){
    $this->titulo = $titulo;
  }
  public function settexto($texto){
    $this->texto = $texto;
  }
  public function setimagen($imagen){
    $this->imagen = $imagen;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
