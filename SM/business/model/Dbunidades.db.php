<?php class Dbunidades extends DbDAO{
  public $id;
  protected $libro;
  protected $nombre;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setlibro($libro){
    $this->libro = $libro;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
