<?php class Dbcms_menu extends DbDAO{
  public $menu_id;
  protected $menu_title;
  protected $menu_url;
  protected $menu_icon;
  public function setmenu_id($menu_id){
    $this->menu_id = $menu_id;
  }
  public function setmenu_title($menu_title){
    $this->menu_title = $menu_title;
  }
  public function setmenu_url($menu_url){
    $this->menu_url = $menu_url;
  }
  public function setmenu_icon($menu_icon){
    $this->menu_icon = $menu_icon;
  }
}
