<?php class Dbgrado extends DbDAO{
  public $id;
  protected $nivel;
  protected $nombre;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setnivel($nivel){
    $this->nivel = $nivel;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
