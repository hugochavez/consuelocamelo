<?php class Dbmaterial extends DbDAO{
  public $id;
  protected $libro;
  protected $nombre;
  protected $url;
  protected $orden;
  public function setid($id){
    $this->id = $id;
  }
  public function setlibro($libro){
    $this->libro = $libro;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function seturl($url){
    $this->url = $url;
  }
  public function setorden($orden){
    $this->orden = $orden;
  }
}
