<?php class Dblibro extends DbDAO{
  public $id;
  protected $tipo;
  protected $nombre;
  protected $serie;
  protected $area;
  protected $nivel;
  protected $grado;
  protected $descripcion;
  protected $codigo;
  protected $caratula;
  protected $autor;
  protected $paginas;
  protected $encuadernacion;
  protected $precio;
  protected $recurso_asociado;
  public function setid($id){
    $this->id = $id;
  }
  public function settipo($tipo){
    $this->tipo = $tipo;
  }
  public function setnombre($nombre){
    $this->nombre = $nombre;
  }
  public function setserie($serie){
    $this->serie = $serie;
  }
  public function setarea($area){
    $this->area = $area;
  }
  public function setnivel($nivel){
    $this->nivel = $nivel;
  }
  public function setgrado($grado){
    $this->grado = $grado;
  }
  public function setdescripcion($descripcion){
    $this->descripcion = $descripcion;
  }
  public function setcodigo($codigo){
    $this->codigo = $codigo;
  }
  public function setcaratula($caratula){
    $this->caratula = $caratula;
  }
  public function setautor($autor){
    $this->autor = $autor;
  }
  public function setpaginas($paginas){
    $this->paginas = $paginas;
  }
  public function setencuadernacion($encuadernacion){
    $this->encuadernacion = $encuadernacion;
  }
  public function setprecio($precio){
    $this->precio = $precio;
  }
  public function setrecurso_asociado($recurso_asociado){
    $this->recurso_asociado = $recurso_asociado;
  }
}
