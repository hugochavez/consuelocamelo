<!DOCTYPE>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js ie10"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>SM CATÁLOGO</title>

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta name="Keywords" lang="es" content="palabras clave" />
<meta name="Description" lang="es" content="texto empresarial" />
<meta name="date" content="2012" />
<meta name="author" content="diseño web: imaginamos.com" />
<meta name="robots" content="All" />

<!-- SLIDER -->
<link rel="stylesheet" href="assets/css/basic-jquery-slider.css">
<!---->

<!-- SELECT Y MULTISELECT -->
<link rel="stylesheet" href="assets/css/chosen.css">
<!---->

<link href="assets/css/sm.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="assets/js/lib/jquery-1.8.3.js"></script>

</head>
<body>
	