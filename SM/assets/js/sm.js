$(document).ready(function () {
	
	/*  SELECTS  */
	if($('select').length) {
		$("select").chosen();
	}
	
	/*  PLACEHOLDER  */
	$(function() {
	 $('input, textarea').placeholder();
	});
	
	
        
        /* DISTRIBUCIÓN PRIMARY*/
	
	var cant_prim = $('.primary').attr('alt');
	var width_prim = 100 / cant_prim;
	width_prim = width_prim + '%'
	
	$(".primary li").each(function(){
		$(this).css({
			'width':width_prim	
		})
	})
	
	var cant_sec = $('.secondary').attr('alt');
	var width_sec = 100 / cant_sec;
	width_sec = width_sec + '%'
	
	$(".secondary li").each(function(){
		$(this).css({
			'width':width_sec	
		})
	})
	
	var cant_ter = $('.tertiary').attr('alt');
	var width_ter = 100 / cant_ter;
	width_ter = width_ter + '%'
	
	$(".tertiary li").each(function(){
		$(this).css({
			'width':width_ter	
		})
	})
        
        //cambia_tipo(0);
	$('.primary li a').click(function() {
            $('.primary li a').removeClass("active");
            $(this).addClass("active");            
        });       
         
        $('.secondary li a').click(function() {
            $('.secondary li a').removeClass("active");
            $(this).addClass("active");            
        });
        
        $('.tertiary li a').click(function() {
            $('.tertiary li a').removeClass("active");
            $(this).addClass("active");            
        });
        //buscar();
        
	
})


