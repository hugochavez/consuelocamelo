<?php include( 'include/define.php' );
include( 'include/config.php' );
include( 'business/function/plGeneral.fnc.php' );
$cbanner = new Dbbanner();
$tipo = $_POST['tipo'];
$dats['where'] = "order by orden";
if ($tipo >0){
    $dats['tipo'] = $tipo;
}
$lista_banner = $cbanner->getList($dats);
?>
  
<div class="slider-block" id="banner">
    <ul class="bjqs">
        <?php foreach ($lista_banner as $item_banner){?>
        <li>
            <div class="slider-info">
                 <h3><?php echo $item_banner['titulo']?></h3>
                 <p><?php echo $item_banner['texto']?></p>
            </div>
            <div class="slider-img">
                <img src="imagenes/banner/<?php echo $item_banner['imagen']?>" width="470px" height="275px">
            </div>
            <div class="clear"></div>
        </li>
        <?php }?>    
    </ul>
</div>
<script>
    $(document).ready(function () {
    /*  SLIDER */
	$('#banner').bjqs({
	  'animation' : 'fade'
	});
        });
</script>