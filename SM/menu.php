<?php $tipo=0;
if (isset($_GET['tipo'])){
$tipo = $_GET['tipo'];
}
$nivel=0;
if (isset($_GET['nivel'])){
$nivel = $_GET['nivel'];
}
$grado=0;
if (isset($_GET['grado'])){
$grado = $_GET['grado'];
}
$area=0;
if (isset($_GET['area'])){
$area = $_GET['area'];
}
$serie=0;
if (isset($_GET['serie'])){
$serie = $_GET['serie'];
}
$busqueda = "";
if (isset($_GET['busqueda'])){
$busqueda = $_GET['busqueda'];
}
?>
<?php $ctipo = new Dbtipo();
            $dats['where']="order by orden";
            $lista_tipos = $ctipo->getList($dats);            
            $ind = 0;?>
<div class="cat-nav">
	<ul class="primary" alt="<?php echo count($lista_tipos);?>">
            
            <?php
            foreach ($lista_tipos as $item){
            $ind ++;?>
    	<li><a onclick="cambia_tipo(<?php echo $item['id']?>)"  <?php if (($ind == 1 && $tipo==0) || ($tipo == $item['id'])){ ?> class="active" <?php }?> ><?php echo $item['nombre']?></a></li>
        <?php }?>
        <div class="clear"></div>
    </ul>
    
    <div id="niveles">
    
    </div>
	
	
    <div id="div_banner">
        
    </div>
    
    <div class="filter">
	<div class="filter-block">
            <div id="sel_series">
            </div>
        <div class="clear"></div>
    </div>
</div>
	
    <div id="grados">
    
    </div>
    <div id="areas">
    
    </div>
        
</div>
<input type="hidden" name="tipo" id="tipo" />
<input type="hidden" name="nivel" id="nivel" />
<input type="hidden" name="area" id="area" />
<input type="hidden" name="grado" id="grado" />
<input type="hidden" name="val_serie" id="val_serie" />

    
    
<script>

function paginacion(pag){
    $.post("secciones/lista_libros.php", {nivel:$("#nivel").val(),tipo:$("#tipo").val(),area:$("#area").val(),grado:$("#grado").val(),serie:$("#val_serie").val(),busqueda:$("#busqueda").val(),pagina:pag}, function(msg_2){
            $("#resultado_libros").html(msg_2);
    });
}

$(document).ready(function () {
        <?php if ($tipo == 0){?>
        cambia_tipo(<?php echo $lista_tipos[0]['id']?>);
        <?php }else{?>
        carga_get();    
        <?php }?>
});

function carga_get(){
    
    
    $.post("areas_menu.php", {tipo:<?php echo $tipo?>}, function(msg_2){
            $("#areas").html(msg_2);
    });
    $.post("banner_menu.php", {tipo:<?php echo $tipo?>}, function(msg_2){
            $("#div_banner").html(msg_2);
    });
    
    $.post("niveles_menu.php", {tipo:<?php echo $tipo?>}, function(msg_2){
            $("#niveles").html(msg_2);
    });
    
    $.post("areas_menu.php", {tipo:<?php echo $tipo?>,area:<?php echo $area?>}, function(msg_2){
            $("#areas").html(msg_2);
    });
    
    <?php if ($nivel > 0){?>
        $.post("grados_menu.php", {nivel:<?php echo $nivel?>,grado:<?php echo $grado?>}, function(msg_2){
            $("#grados").html(msg_2);
    });
        <?php }else{?>
         $.post("grados_menu.php", {tipo:<?php echo $tipo?>,grado:<?php echo $grado?>}, function(msg_2){
            $("#grados").html(msg_2);
    });   
       <?php }?>
    $.post("sel_series.php", {tipo:<?php echo $tipo?>,busqueda:'<?php echo $busqueda?>',serie:<?php echo $serie?>}, function(msg_2){
            $("#sel_series").html(msg_2);
            $("#tipo").val(<?php echo $tipo?>);
            $("#area").val(<?php echo $area?>);
            $("#grado").val(<?php echo $grado?>);
            $("#val_serie").val(<?php echo $serie?>);
            $("#nivel").val(<?php echo $nivel?>);
            $("#busqueda").val('<?php echo $busqueda?>');
            buscar();
    });
    
    
}
    
function cambia_nivel(nivel){
    $.post("grados_menu.php", {nivel:nivel}, function(msg_2){
            $("#grados").html(msg_2);
    });
    $("#grado").val(0);
    $("#nivel").val(nivel);
    buscar();
}
function cambia_tipo(tipo){
    $.post("areas_menu.php", {tipo:tipo}, function(msg_2){
            $("#areas").html(msg_2);
    });
    $.post("banner_menu.php", {tipo:tipo}, function(msg_2){
            $("#div_banner").html(msg_2);
    });
    
    $.post("niveles_menu.php", {tipo:tipo}, function(msg_2){
            $("#niveles").html(msg_2);
    });
    $.post("sel_series.php", {tipo:tipo}, function(msg_2){
            $("#sel_series").html(msg_2);
            cambia_nivel(0);
            $("#tipo").val(tipo);
            $("#nivel").val(0);
            $("#area").val(0);
            $("#grado").val(0);
            $("#val_serie").val($("#serie").val());
            buscar();
    });
    
}
function cambia_area(area){
    $("#area").val(area);
    buscar();
}
function cambia_grado(grado){
    $("#grado").val(grado);
    buscar();
}

function cambia_serie(serie){
    $("#val_serie").val(serie);
    buscar();
}

function buscar(){
    //$("#resultado_libros").html('<img src="assets/"');
    
    $.post("secciones/lista_libros.php", {nivel:$("#nivel").val(),tipo:$("#tipo").val(),area:$("#area").val(),grado:$("#grado").val(),serie:$("#val_serie").val(),busqueda:$("#busqueda").val()}, function(msg_2){
            $("#resultado_libros").html(msg_2);
    });
    
}

</script>